module HaskellFuerDenRestVonUns.Kapitel7.Aufgabe1 (Keller (..)) where

-- | Der vorgeschlagene Versuch verletzt die Monadengesetze, z.B.:
-- 1. Monadengesetz: 
-- return x >>= f
-- = neuerKeller x >>= f
-- = Keller ([], Nothing) >>= f
-- = Keller ([], Nothing)
-- was nicht gleich f x ist.
-- 2. Monadengesetz:
-- a) p = Keller ([], Nothing)
-- p >>= return
-- = Keller ([], Nothing) = p (noch in Ordnung)
-- b) p = Keller ([], Just x)
-- p >>= return
-- = Keller ([], Nothing) /= p (Widerspruch!)
-- b) p = Keller ((x:xs), Nothing)
-- p >>= return 
-- = undefiniert /= p (Widerspruch
-- c)
-- p = Keller ((x:xs), Just z)
-- p >>= return
-- = Keller (concat [fst(derKeller (return y)) | y <- (x:xs)], Just (head (x:xs)))
-- = Keller (concat [fst(derKeller (Keller ([], Nothing))) | y <- (x:xs)], Just x)
-- = Keller (concat [[] | y <- (x:xy)], Just x)
-- = Keller ([], Just x) /= p (Widerspruch!)
-- 
-- Offensichtlich erzeugen return und die Möglichkeit des invaliden Zustands
-- Keller ((x:xs), Nothing) Probleme.

-- | Kellerspeicher, diesmal sicher valide
data Keller a = Leer | Keller { derKeller :: ([a], a) } deriving (Show, Eq)

-- | Erzeugt einen Neuen Keller für das Element x
neuerKeller :: a -> Keller a
neuerKeller x = Keller ([], x)

-- | Eine Monadeninstanz für Keller
instance Monad Keller where
  return x = neuerKeller x
  Leer >>= _ = Leer
  Keller (xs, x) >>= f = 
    foldl1 kombiniere $ map f (x:xs)

-- | Kombiniert zwei Keller, indem der zweite Keller unter den ersten geschoben wird
kombiniere :: Keller a -> Keller a -> Keller a
kombiniere Leer k = k
kombiniere k Leer = k
kombiniere (Keller (xs, x)) (Keller (ys, y)) = Keller (xs ++ (y:ys), x)
      
-- | 1. Monadengesetz:
-- return x >>= f
-- = neuerKeller x >>= f
-- = Keller ([], x) >>= f
-- = foldl1 kombiniere $ map f [x]
-- = foldl1 kombiniere $ [f x]
-- = f x
-- 2. Monadengesetz:
-- k >>= return
-- a) Leer >>= return
-- = Leer
-- b) (Keller (xs, x)) >>= return
-- = foldl1 kombiniere $ map return (x:xs)
-- = foldl1 kombiniere [Keller ([], x), Keller ([], head xs), Keller ([], head (tail xs)), ... Keller ([], last xs)]
-- = foldl1 kombiniere [kombiniere (Keller ([], x)) (Keller ([], head xs)), Keller ([], head (tail xs)), ... Keller ([], last xs)]
-- = foldl1 kombiniere [Keller ([] ++ (head xs : []), x), Keller ([], head (tail xs)), ... Keller ([], last xs)]
-- = foldl1 kombiniere [Keller ([head xs], x), Keller ([], head (tail xs)), ... Keller ([], last xs)]
-- = foldl1 kombiniere [kombiniere (Keller ([head xs], x)) (Keller ([], head (tail xs))), ... Keller ([], last xs)]
-- = foldl1 kombiniere [Keller ([head xs] ++ (head (tail xs) : []), x), ... Keller ([], last xs)]
-- = foldl1 kombiniere [Keller ([head xs, head (tail xs)], x), ... Keller ([], last xs)]
-- = foldl1 kombiniere [Keller (init xs, x), Keller ([], last xs)]
-- = kombiniere (Keller (init xs, x)) (Keller ([], last xs))
-- = Keller (init xs ++ (last xs : []), x)
-- = Keller (xs, x)
-- 3. Monadengsetz:
--
-- Hilfssatz 3.1:
-- (kombiniere, Leer) bildet einen Monoiden über Keller
-- Beweis:
-- Neutrales Element:
-- kombiniere Leer k = kombiniere k Leer = k
-- Assoziativität: 
-- kombiniere Leer (kombiniere k k') = kombiniere k k' = kombiniere (kombiniere Leer k) k'
-- kombiniere k (komniniere Leer k') = kombiniere k k' = kombiniere (kombiniere k Leer) k'
-- kombiniere k (kombiniere k' Leer) = kombiniere k k' = kombiniere (kombiniere k k') Leer
-- kombiniere (Keller (xs, x)) (kombiniere (Keller (ys, y)) (Keller (zs, z)))
-- = kombiniere (Keller (xs, x)) (Keller (ys ++ (z:zs), y))
-- = Keller (xs ++ (y:(ys ++ (z:zs))), x)
-- = kombiniere (Keller (xs ++ (y:ys), x)) (Keller (zs, z))
-- = kombiniere (kombiniere (Keller (xs, x)) (Keller (ys, y))) (Keller (zs, z))
--
-- Hilfssatz 3.2:
-- Sei f assoziativ, so gilt:
-- f (foldl1 f (x:xs)) (foldl1 f (y:ys)) = foldl1 f ((x:xs) ++ (y:ys))
-- Beweis:
-- f (foldl1 f (x:xs)) (foldl1 f (y:ys))
-- = f (f (... (f (f x (head xs)) (head $ tail xs))) (last xs)) (f ( ... (f (f y (head ys)) (head $ tail ys))) (last ys))
-- = f ( ... (f (f (f ( ... (f (f x (head xs)) (head $ tail xs))) y) (head ys)) (head $ tail ys))) (last ys)
-- = foldl1 f ((x:xs) ++ (y:ys))
--
-- Beweis des 3. Monadengesetzes:
-- k >>= (\ x -> (f x >>= g)) = (k >>= (\ x -> f x)) >>= g
-- Beweis per Induktion in der Kellergröße
-- a) Leerer Keller
-- Leer >>= (\ x -> (f x >>= g)) = Leer
-- (Leer >>= (\ x -> f x)) >>= g = Leer >>= g = Leer
-- b) Keller mit einem Element
-- (Keller ([], x)) >>= (\ x -> (f x >>= g))
-- = foldl1 kombiniere $ map (\ x -> (f x >>= g)) [x]
-- = foldl1 kombiniere $ [f x >>= g]
-- = f x >>= g
-- = (return x >>= f) >>= g
-- = (Keller ([], x) >>= f) >>= g
-- = (Keller ([], x) >>= (\ x -> f x)) >>= g
-- c) Induktionsschritt: Keller mit n+1 Elementen
-- (Keller ((xs++[x']), x) >>= (\ x -> (f x >>= g)))
-- = foldl1 kombiniere $ map (\ x -> (f x >>= g)) (x:(xs ++ [x']))
-- = kombiniere (foldl1 kombiniere $ map (\ x -> (f x >>= g)) (x:xs)) ((\ x -> f x >>= g) x')
-- = kombiniere ((Keller (xs, x)) >>= (\ x -> f x >>= g)) (f x' >>= g)
-- = kombiniere (((Keller (xs, x)) >>= (\ x -> f x)) >>= g) (f x' >>= g)
-- c.1) (Keller (xs, x)) >>= (\ x -> f x) = Leer
-- => kombiniere (Leer >>= g) (f x' >>= g)
-- = f x' >>= g
-- = (kombiniere Leer (f x')) >>= g
-- = (kombiniere ((Keller (xs, x)) >>= (\ x -> f x)) (f x')) >>= g
-- = (foldl1 kombiniere $ map (\ x -> f x) (x:(xs++[x']))) >>= g
-- = (Keller (xs ++ [x'], x) >>= (\ x -> f x)) >>= g
-- c.2) f x' = Leer
-- => kombiniere (((Keller (xs, x)) >>= (\ x -> f x)) >>= g) (Leer >>= g)
-- = kombiniere (((Keller (xs, x)) >>= (\ x -> f x)) >>= g) Leer
-- = ((Keller (xs, x)) >>= (\ x -> f x)) >>= g
-- = (kombiniere ((Keller (xs, x)) >>= (\ x -> f x)) Leer) >>= g
-- = (kombiniere ((Keller (xs, x)) >>= (\ x -> f x)) (f x')) >>= g
-- = (foldl1 kombiniere $ map (\ x -> f x) (x:(xs ++ [x']))) >>= g
-- = ((Keller (xs ++ [x'], x)) >>= (\ x -> f x)) >>= g
-- c.3) (Keller (xs, x)) >>= (\ x -> f x) = Keller (ys, y), f x' = Keller (zs, z)
-- => kombiniere ((Keller (ys, y)) >>= g) ((Keller (zs, z)) >>= g)
-- = kombiniere (foldl1 kombiniere $ map g (y:ys)) (foldl1 kombiniere $ map g (z:zs))
-- = foldl1 kombiniere (map g (y:ys) ++ map g (z:zs))
-- = foldl1 kombiniere $ map g ((y:ys) ++ (z:zs))
-- = Keller (ys ++ (z:zs), y) >>= g
-- = (kombiniere (Keller (ys, y)) (Keller (zs, z))) >>= g
-- = (kombiniere ((Keller (xs, x)) >>= (\ x -> f x)) (f x')) >>= g
-- = (foldl1 kombiniere $ map (\ x -> f x) (Keller (xs ++ [x'], x))) >>= g
-- = ((Keller (xs ++ [x'], x)) >>= (\ x -> f x)) >>= g
