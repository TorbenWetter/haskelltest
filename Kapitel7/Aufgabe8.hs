module HaskellFuerDenRestVonUns.Kapitel7.Aufgabe8 (Zustand (..), Atom (..), mkAtom) where

-- | Atomare Identifikatoren
data Atom a = Atom a deriving (Show, Eq)

-- | Die Zustandsmonade
newtype Zustand z a = ZS { ausf :: z -> (z, a) }

instance Monad (Zustand z) where
  return x = ZS (\ s -> (s, x))
  (ZS p) >>= f = ZS (\ s -> let { (s', y) = p s; (ZS q) = f y } in q s')

-- | Gibt das aktuelle Atom zurück und zählt den Zustand für das nächste Atom 1 weiter.
mkAtom :: (Enum a) => Zustand (Atom a) (Atom a)
mkAtom = ZS (\ (Atom x) -> (Atom (succ x), Atom x) )
