{-# LANGUAGE TemplateHaskell, FlexibleInstances #-}

module HaskellFuerDenRestVonUns.Kapitel7.Test.Aufgabe5 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import qualified HaskellFuerDenRestVonUns.Kapitel7.Aufgabe5 as A5

-- | Zufällige Arithmetische Ausdrücke
instance Arbitrary (A5.Baum [Char]) where
  arbitrary = oneof [ A5.Knoten <$> (show . abs <$> (arbitrary :: Gen Int)) <*> return A5.Leer <*> return A5.Leer
                    , A5.Knoten <$> (show <$> (arbitrary :: Gen Op)) 
                                <*> suchThat arbitrary (/= A5.Leer) 
                                <*> suchThat arbitrary (/= A5.Leer)
                    ]

-- | Operationen
data Op = Plus | Minus | Mult | Div | Inval

-- | Operaionen in übersetzbarer Version
instance Show Op where
  show Plus = "+"
  show Minus = "-"
  show Mult = "*"
  show Div = "/"
  show Inval = "invalid"

-- | Zufällige Operationen
instance Arbitrary Op where
  arbitrary = oneof [ return Plus
                    , return Minus
                    , return Mult
                    , return Div
                    , return Inval
                    ]


-- | Testet, ob Operationen richtig interpretiert werden, indem zwei Teilbäume verknüpft werden
prop_op :: Op -> A5.Baum [Char] -> A5.Baum [Char] -> Bool
prop_op op b1 b2 = 
  let 
    res = do
      f <- A5.operation (show op)
      l <- A5.interpretiere b1
      r <- A5.interpretiere b2
      f l r
  in
    res == A5.interpretiere (A5.Knoten (show op) b1 b2)

runTests = $quickCheckAll

