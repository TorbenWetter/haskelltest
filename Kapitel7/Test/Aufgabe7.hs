{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel7.Test.Aufgabe7 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative
import Control.Monad

import qualified HaskellFuerDenRestVonUns.Kapitel7.Aufgabe7 as A7
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11 as A11

-- | Zufällige Mengen erzeugen
instance (Arbitrary a) => Arbitrary (A11.MeineMenge a) where
  arbitrary = A7.toMenge' . take 20 <$> arbitrary

-- | Testet, ob Mengen das erste Monadengesetz erfüllen
prop_monad1 :: (Eq a, Eq b) => a -> Fun a (A11.MeineMenge b) -> Bool
prop_monad1 x (Fun _ f) = (return x >>= f) == f x

-- | Testet, ob Mengen das zweite Monadengesetz erfüllen
prop_monad2 :: (Eq a) => A11.MeineMenge a -> Bool
prop_monad2 k = (k >>= return) == k

-- | Testet, ob Mengen das dritte Monadengsetz erfüllen
prop_monad3 :: (Eq a, Eq b, Eq c) => A11.MeineMenge a -> Fun a (A11.MeineMenge b) -> Fun b (A11.MeineMenge c) -> Bool
prop_monad3 k (Fun _ f) (Fun _ g) = ((k >>= (\ x -> f x)) >>= g) == (k >>= (\ x -> (f x >>= g)))

-- | Testet, ob Mengen das erste MonadPlus-Gesetz erfüllen
prop_mplusNeutral :: (Eq a) => A11.MeineMenge a -> Bool
prop_mplusNeutral m = (m `mplus` mzero == m) && (mzero `mplus` m == m)

-- | Testet, ob Mengen das zweite MonadPlus-Gesetz erfüllen
prop_mplusAssoc :: (Eq a) => A11.MeineMenge a -> A11.MeineMenge a -> A11.MeineMenge a -> Bool
prop_mplusAssoc m n o = (m `mplus` (n `mplus` o)) == ((m `mplus` n) `mplus` o) 


runTests = $quickCheckAll
