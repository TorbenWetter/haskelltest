{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel7.Test.Aufgabe1 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import qualified HaskellFuerDenRestVonUns.Kapitel7.Aufgabe1 as A1

instance (Arbitrary a) => Arbitrary (A1.Keller a) where
  arbitrary = oneof [ return A1.Leer
                    , A1.Keller . ((,) []) <$> arbitrary
                    , A1.Keller <$> ((,) <$> arbitrary <*> arbitrary)
                    ] 

-- | Testet, ob Keller das erste Monadengesetz erfüllen
prop_monad1 :: (Eq a, Eq b) => a -> Fun a (A1.Keller b) -> Bool
prop_monad1 x (Fun _ f) = (return x >>= f) == f x

-- | Testet, ob Keller das zweite Monadengesetz erfüllen
prop_monad2 :: (Eq a) => A1.Keller a -> Bool
prop_monad2 k = (k >>= return) == k

-- | Testet, ob Keller das dritte Monadengsetz erfüllen
prop_monad3 :: (Eq a, Eq b, Eq c) => A1.Keller a -> Fun a (A1.Keller b) -> Fun b (A1.Keller c) -> Bool
prop_monad3 k (Fun _ f) (Fun _ g) = ((k >>= (\ x -> f x)) >>= g) == (k >>= (\ x -> (f x >>= g)))

runTests = $quickCheckAll
