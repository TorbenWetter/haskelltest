{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel7.Test.Aufgabe8 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative
import Control.Monad
import Data.List (nub)

import HaskellFuerDenRestVonUns.Kapitel7.Aufgabe8 as A8


instance Arbitrary a => Arbitrary (A8.Atom a) where
  arbitrary = A8.Atom <$> arbitrary

-- | Testet, ob mkAtom (n `mod` 1000)-mal neue Atome liefert
prop_neueAtome :: (Eq a, Enum a) => Int -> A8.Atom a -> Bool
prop_neueAtome n start = 
  length (nub . snd $ ausf (mapM (\ _ -> mkAtom) [1..(n `mod` 1000)]) start) == (n `mod` 1000)

runTests = $quickCheckAll