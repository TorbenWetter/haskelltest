{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel7.Test.Aufgabe9 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import HaskellFuerDenRestVonUns.Kapitel6.Test.Aufgabe7 (Deriv (..), toParserForm, toString)

import HaskellFuerDenRestVonUns.Kapitel6.Aufgabe7 as A7
import HaskellFuerDenRestVonUns.Kapitel7.Aufgabe9 as A9

-- | Testet, ob der Monadische Automat das selbe
-- Ergebnis liefert, wenn er eins Liefert
prop_gleich :: Deriv -> Bool
prop_gleich d@(Deriv g (belongs, _)) =
  case (A7.automat (toParserForm g) (toString d), A9.parse (toParserForm g) (toString d)) of
    (Left _, Left _) -> not belongs
    (Left _, Right _) -> False
    (Right _, Left _) -> False
    (Right (res, _), Right res') -> (res == res') && belongs

runTests = $quickCheckAll