module HaskellFuerDenRestVonUns.Kapitel7.Aufgabe9 (parse) where

import Control.Monad

import HaskellFuerDenRestVonUns.Kapitel6.Aufgabe7

-- | Die Zustandstransformation (S. 191) über einer beliebigen Monade m
data ZustandsTransf m s a = ZT (s -> m (s, a))

-- | Monadeninstanz der Zustandstransformation
instance (Monad m) => Monad (ZustandsTransf m s) where
  return x = ZT (\ s -> return (s, x))
  (ZT p) >>= f = ZT (\ s0 -> do
                       (s1, x) <- p s0 
                       let (ZT q) = f x in q s1)

-- | Beweis:
-- 1. Monadengesetz
-- return x >>= f 
-- = ZT (\ s -> return (s, x)) >>= f
-- = ZT (\ s0 -> do
--         (s1, x) <- (\ s -> return (s, x)) s0
--         let (ZT q) = f x in q s1)
-- = ZT (\ s0 -> do let (ZT q) = f x in q s0)
-- = f x
-- 2. Monadengesetz
-- m >>= return
-- = (ZT p) >>= return
-- = ZT (\ s0 -> do
--         (s1, x) <- p s0
--         let (ZT q) = return x in q s1)
-- = ZT (\ s0 -> do
--         (s1, x) <- p s0
--         let (ZT q) = ZT (\ s -> return (s, x)) in q s0)
-- = ZT (\ s0 -> p s0 >>= (\ (s1, x) -> (\ s -> return (s, x)) s0))
-- = ZT (\ s0 -> p s0 >>= (\ (s1, x) -> return (s1, x)))
-- = ZT (\ s0 -> p s0 >>= return)
-- = ZT (\ s0 -> p s0)
-- = ZT p
-- = m
-- 3. Monadengesetz
-- ((ZT p) >>= (\ x -> f x)) >>= g
-- = ZT (\ s0 -> do
--         (s1, x) <- p s0
--         let (ZT q) = f x in q s1) >>= g
-- = ZT (\ s0 -> p s0 >>= (\ (s1, x) -> let (ZT q) = f x in q s1)) >>= g
-- = ZT (\ s2 -> do
--         (s3, x') <- (\ s0 -> p s0 >>= (\ (s1, x) -> let (ZT q) = f x in q s1)) s2
--         let (ZT q) = g x' in q s3)
-- = ZT (\ s2 -> do
--         (s3, x') <- p s2 >>= (\ (s1, x) -> let (ZT q) = f x in q s1)
--         let (ZT q) = g x' in q s3)
-- = ZT (\ s2 -> 
--         (p s2 >>= (\ (s1, x) -> let (ZT q) = f x in q s1)) >>= (\ (s3, x') -> let (ZT q) = g x' in q s3))
-- = ZT (\ s2 -> 
--         p s2 >>= (\ (s4, x'') -> (\ (s1, x) -> let (ZT q) = f x in q s1) (s4, x'') 
--                                  >>= (\ (s3, x') -> let (ZT q) = g x' in q s3)))
-- = ZT (\ s2 ->
--         (s4, x'') <- p s2
--         (\ (s1, x) -> let (ZT q) = f x in q s1) (s4, x'') 
--           >>= (\ (s3, x') -> let (ZT q) = g x' in q s3))
-- = ZT (\ s2 ->
--         (s4, x'') <- p s2
--         let (ZT q) = (\ x -> ZT (\ s1 -> let (ZT q) = f x in q s1) 
--                              >>= (\ (s3, x') -> let (ZT q) = g x' in q s3)) x in q s4)
-- = p s2 >>= (\ x -> ZT (\ s1 -> let (ZT q) = f x in q s1) 
--                    >>= (\ (s3, x') -> let (ZT q) = g x' in q s3)) x in q s4)
-- = p s2 >>= (\ x -> (f x) >>=  g)

-- | Wendet eine Zustandstransformation an
appZT :: ZustandsTransf m s a -> s -> m (s, a)
appZT (ZT q) s = q s

-- | MonadPlus-Instanz der Zustandstransformation
instance (MonadPlus m) => MonadPlus (ZustandsTransf m s) where
  mzero = ZT (\ _ -> mzero)
  (ZT p) `mplus` (ZT q) = ZT (\ s0 -> p s0 `mplus` q s0)

-- | Beweis:
-- 1. Neutrales Element:
-- mzero `mplus` (ZT q) 
-- = (ZT (\ s -> mzero)) `mplus` (ZT q)
-- = ZT (\ s0 -> mzero  `mplus` q s0)
-- = ZT (\ s0 -> q s0)
-- = ZT q
-- (ZT p) `mplus` mzero
-- = (ZT p) `mplus` (ZT (\ s -> mzero))
-- = ZT (\ s0 -> p s0  `mplus` mzero)
-- = ZT (\ s0 -> p s0)
-- = ZT p
-- 2. Assoziativität:
-- (ZT p) `mplus` ((ZT q) `mplus` (ZT r))
-- = (ZT p) `mplus` (ZT (\ s0 -> q s0 `mplus` r s0))
-- = ZT (\ s1 -> p s1 `mplus` (\ s0 -> q s0 `mplus` r s0) s1)
-- = ZT (\ s1 -> p s1 `mplus` (q s1 `mplus` r s1))
-- = ZT (\ s1 -> (p s1 `mplus` q s1) `mplus` r s1)
-- = ZT (\ s1 -> p s1 `mplus` q s1) `mplus` (ZT r)
-- = ((ZT p) `mplus` (ZT q)) `mplus` (ZT r)

                 



-- | Der aktuelle Parserzustand:
-- Fertig und ZuTun mit Keller vom Typ k sowie jeweils
-- den bisher erledigten Symbolen.
-- Andernfalls Fehler mit einem String
data Zustand k
  = Fertig { abgearbeitet :: [Atom] }
  | ZuTun  { abgearbeitet :: [Atom], keller :: k }
  | Fehler { info :: String }

-- | Eine Monadeninstanz für den aktuellen Parserzustand (in Anlehung an Maybe)
instance Monad Zustand where
  return k = ZuTun [] k
  (Fertig ok) >>= _ = (Fertig ok)
  (Fehler err) >>= _ = (Fehler err)
  (ZuTun ok k) >>= f =
    case f k of
      Fehler err   -> Fehler err
      Fertig ok'   -> Fertig (ok ++ ok')
      ZuTun ok' k' -> ZuTun (ok ++ ok') k'

-- Beweis:
-- 1. Monadengesetz
-- return k >>= f
-- = (ZuTun [] k) >>= f
-- = case f k of
--    Fehler err   -> Fehler err
--    Fertig ok'   -> Fertig ([] ++ ok')
--    ZuTun ok' k' -> ZuTun ([] ++ ok') k'
-- = f k
-- 2. Monadengesetz
-- 2.1) (Fertik ok) >>= return
-- = (Fertig ok)
-- 2.2) (Fehler err) >>= return
-- = (Fehler err)
-- 2.3) (ZuTun ok k) >>= return
-- = case return k of
--    Fehler err   -> Fehler err
--    Fertig ok'   -> Fertig (ok ++ ok')
--    ZuTun ok' k' -> ZuTun (ok ++ ok') k'
-- = case (ZuTun [] k) of
--    Fehler err   -> Fehler err
--    Fertig ok'   -> Fertig (ok ++ ok')
--    ZuTun ok' k' -> ZuTun (ok ++ ok') k'
-- = ZuTun (ok ++ []) k
-- = ZuTun ok k
-- 3. Monadengesetz
-- 3.1) (Fertig ok >>= (\ x -> f x)) >>= g
-- = Fertig ok >>= g
-- = Fertig ok
-- = Fertig ok >>= (\ x -> (f x) >>= g)
-- 3.2) (Fehler err >>= (\ x -> f x)) >>= g
-- = Fehler err >>= g
-- = Fehler err
-- = Fehler err >>= (\ x -> (f x) >>= g)
-- 3.3) (ZuTun ok k >>= (\ x -> f x)) >>= g
-- = (case f k of
--     Fehler err   -> Fehler err
--     Fertig ok'   -> Fertig (ok ++ ok')
--     ZuTun ok' k' -> ZuTun (ok ++ ok') k') >>= g
-- = case f k of
--    Fehler err   -> Fehler err >>= g
--    Fertig ok'   -> Fertig (ok ++ ok') >>= g
--    ZuTun ok' k' -> ZuTun (ok ++ ok') k' >>= g
-- = case f k >>= g of
--    Fehler err   -> Fehler err
--    Fertig ok'   -> Fertig (ok ++ ok')
--    ZuTun ok' k' -> ZuTun (ok ++ ok') k'
-- = ZuTun ok k >>= (\ k -> (f k) >>= g)

-- | Eine MonadPlus-Instanz für Parserzustände
instance MonadPlus Zustand where
  mzero = Fehler ""
  (Fehler _) `mplus` m = m
  m `mplus` (Fehler _) = m
  m `mplus` n = m >> n

-- | Beweis:
-- 1. Neutrales Element:
-- mzero `mplus` m = (Fehler "") `mplus` m = m
-- m `mplus` mzero = m `mplus` (Fehler "") = m
-- 2. Assoziativität:
-- 2.1) (Fehler err) `mplus` (n `mplus` o) 
-- = n `mplus` o 
-- = ((Fehler err)  `mplus` n) `mplus` o
-- 2.2) m `mplus` ((Fehler err) `mplus` o)
-- = m `mplus` o 
-- = (m `mplus` (Fehler err)) `mplus` o
-- 2.3) m `mplus` (n `mplus` (Fehler err))
-- = m `mplus` n
-- = (m `mplus` n) `mplus` (Fehler err)
-- 2.4) m `mplus` (n `mplus` o)
-- = m >> (n >> o)
-- = m >>= (\ _ -> n >>= (\ _ -> o))
-- = (m >>= (\ _ -> n)) >>= (\ _ -> o)
-- = (m >> n) >> o
-- = (m `mplus` n) `mplus` o


-- | Der aktuelle Keller mit der Zustandsnummer, 
-- dem abzuarbeitenden Atom und dem darunter liegenden Keller
data Keller 
  = Keller 
  { zustand :: Int
  , symbol :: Atom
  , rest :: Keller
  }
  | Boden { zustand :: Int }

-- | Nimmt den Zustand und das oberste Zeichen vom Keller eines Parsers
pop :: ZustandsTransf Zustand Keller (Int, Atom)
pop = ZT (\ k -> case k of
                   Keller z s r -> return (r, (z, s))
                   Boden _ -> Fehler "Keller unerwartet leer")


-- | Legt einen Zustand und ein Zeichen auf den Keller eines Parsers
push :: (Int, Atom) -> ZustandsTransf Zustand Keller ()
push (z, s) = ZT (\ k -> return (Keller z s k, ()))

-- | Liest den aktuellen Automatenzustand
peek :: ZustandsTransf Zustand Keller Int
peek = ZT (\ k -> return (k, zustand k))


-- | Bestimmt die auszuführende Aktion abhängig vom aktuellen Zustand und der Eingabe
nächsteAktion :: [((Int, Atom), Aktion)] -> Atom -> ZustandsTransf Zustand Keller Aktion
nächsteAktion tabelle eingabe = do
  z <- peek
  case lookup (z, eingabe) tabelle of
    Just a  -> return a
    Nothing -> ZT (\ _ -> Fehler "Ungültiges Aktionsziel")

-- | Führt eine Shift-Aktion zum übergebenen Zustand und der ersten Eingabe aus
shift :: Int -> [Atom] -> ZustandsTransf Zustand Keller [Atom]
shift _ [] = ZT (\ _ -> Fehler "Unerwartetes Eingabeende")
shift z (e:es) = push (z, e) >> return es

-- | Schlägt eine Produktionsregel anhand ihrer Nummer nach
findeRegel :: [(Atom, [Atom])] -> Int -> ZustandsTransf Zustand Keller (Atom, [Atom])
findeRegel tabelle nr
  | nr < 0 || nr >= length tabelle = ZT (\ _ -> Fehler $ "Ungültige Regelnummer " ++ (show nr))
  | otherwise = return (tabelle !! nr)

-- | Setzt den Folgezustand für einen nachgeschlagenen Zustand und ein Symbol
setzeFolgezustand :: [((Int, Atom), Int)] -> (Int, Atom) -> ZustandsTransf Zustand Keller ()
setzeFolgezustand goto (z, s) =
  case lookup (z, s) goto of
    Just z' -> push (z', s) >> return ()
    Nothing -> ZT (\ _ -> Fehler "Folgezustand nicht gefunden")

-- | Markiert ein Symbol als Abgearbeitet
alsAbgearbeitet :: Atom -> ZustandsTransf Zustand Keller ()
alsAbgearbeitet a = ZT (\ k -> ZuTun [a] (k, ()))

-- | Führt eine Reduce-Aktion zur übergebenen Produktionsregel aus
reduce :: Atom -> [((Int, Atom), Int)] -> (Atom, [Atom]) -> ZustandsTransf Zustand Keller ()
reduce startS goto (r, [])
  | r == startS = ZT (\ _ -> Fertig [])
  | otherwise = do
    z <- peek
    setzeFolgezustand goto (z, r)
reduce startS goto (r, rs) = do
  (_, s) <- pop
  guard (last rs == s)
  alsAbgearbeitet s
  reduce startS goto (r, init rs)


-- | Führt eine Automatentransition aus
transition :: 
  Atom 
  -- ^ Startsymbol
  -> [(Atom, [Atom])]
  -- ^ Produktions-Tabelle
  -> [((Int, Atom), Int)]
  -- ^ Goto-Tabelle
  -> [((Int, Atom), Aktion)]
  -- ^ Aktionstabelle
  -> [Atom]
  -- ^  Eingabe
  -> ZustandsTransf Zustand Keller [Atom]
transition _ _ _ _ [] = ZT (\ _ -> Fehler "Unerwartetes Eingabeende")
transition startS prod goto aktionen (e:es) = do
  akt <- nächsteAktion aktionen e
  case akt of
    (Shift z') -> shift z' (e:es)
    (Reduce i) -> do
             regel <- findeRegel prod i
             reduce startS goto regel
             return (e:es)

-- | Führt Automatentransitionen bis zum Ergebnis aus
automatM ::
  Atom 
  -- ^ Startsymbol
  -> [(Atom, [Atom])]
  -- ^ Produktions-Tabelle
  -> [((Int, Atom), Int)]
  -- ^ Goto-Tabelle
  -> [((Int, Atom), Aktion)]
  -- ^ Aktionstabelle
  -> [Atom]
  -- ^  Eingabe
  -> ZustandsTransf Zustand Keller [Atom]
automatM startS prod goto aktionen es =
  transition startS prod goto aktionen es >>= automatM startS prod goto aktionen

-- | Grammatik parsen und entweder einen Fehler, oder
-- die abgearbeiteten Symbole zurückgeben.
parse :: 
  [([Char], String)] 
  -> String 
  -> Either String [Atom]
parse prod eingabe =
  let
    goto = berechneGoto prod
    tabelle = berechneAktion prod
    aprod = atomify prod
    start = startSymb prod
    atomEingabe = myLex prod (eingabe ++ " $")
  in
    case appZT (automatM start aprod goto tabelle atomEingabe) (Boden 0) of
      Fehler err -> Left err
      Fertig ok  -> Right $ reverse ok
      ZuTun _ _  -> error "Programmierfehler!"