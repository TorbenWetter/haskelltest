module HaskellFuerDenRestVonUns.Kapitel7.Aufgabe7 (toMenge') where

import Control.Monad

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11

-- | Mege aus einer Liste erstellen, ohne die Duplikate zu entfernen
toMenge' :: [a] -> MeineMenge a
toMenge' [] = Leer
toMenge' (x:xs) = x :<: (toMenge' xs)


-- | Mengen als Monaden
instance Monad MeineMenge where
  return = singleton
  Leer >>= _ = Leer
  m >>= f = toMenge' (toListe m >>= (toListe . f))


-- | Beweis:
-- Hilfssatz:
-- toMenge' . toListe = id
-- toListe . toMenge' = id
-- Induktion in der Größe der Menge:
-- Leere Menge:
-- (toMenge' . toListe) Leer = toMenge' [] = Leer
-- (toListe . toMenge') [] = toListe Leer = []
-- Größe n+1:
-- (toMenge' . toListe) (x :<: xs) 
-- = toMenge' (x:(toListe xs))
-- = x :<: ((toMenge' . toListe) xs) (IH)
-- = x :<: xs
-- (toListe . toMenge') (x:xs)
-- = toListe (x :<: ((toListe . toMenge') xs))
-- = x : (toListe . toMenge' xs) (IH)
-- = x : xs
--
-- 1. Monadengesetz
-- return x >>= f
-- = singleton x >>= f
-- = toMenge' (toListe (x :<: Leer) >>= (toListe . f))
-- = toMenge' ([x] >>= (toListe . f))
-- = toMenge' (concat $ map (toListe . f) [x])
-- = toMenge' (toListe . f $ x)
-- = toMenge' . toListe . f $ x
-- = id . f $ x
-- = f x
-- 2. Monadengesetz
-- m >>= return = m
-- 2.1) m = Leer
-- Leer >>= return = Leer
-- 2.2) m = (x:<:xs)
-- m >>= return
-- = toMenge' (toListe m >>= (toListe . return))
-- = toMenge' (concat $ map (toListe . return) (toListe m))
-- = toMenge' (concat $ map (return) (toListe m)
-- = toMenge' ((toListe m) >>= return)
-- = toMenge' (toListe m)
-- = m
-- 3. Monadengesetz
-- m >>= (\ x -> (f x) >>= g) = (m >>= \ x -> f x) >>= g
-- 3.1) m = Leer
-- Leer >>= (\ x -> (f x) >>= g)
-- = Leer
-- = Leer >>= \ x -> f x
-- = (Leer >>= \ x -> f x) >>= g
-- 3.2) m = (x :<: xs)
-- m >>= (\ x -> (f x) >>= g)
-- = toMenge' (toListe m >>= (toListe . (\ x -> (f x) >>= g)))
-- = toMenge' (concat $ map (toListe . (\ x -> (f x) >>= g)) (toListe m))
-- = toMenge' (concat $ map (\ x -> toListe $ (f x) >>= g) (toListe m))
--
-- 3.2.1) (f x) >>= g = Leer
-- => toListe $ (f x) >>= g = []
-- 3.2.2) (f x) >>= g = toMenge' (toListe (f x) >>= (toListe . g))
-- => toListe $ (f x) >>= g
-- = toListe $ toMenge' (toListe (f x) >>= (toListe . g))
-- = toListe (f x) >>= (toListe . g)
--
-- => toMenge' (concat $ map (\ x -> toListe $ (f x) >>= g) (toListe m))
-- = toMenge' (concat $ map (\ x -> toListe (f x) >>= (toListe . g)) (toListe m))
-- = toMenge' ((toListe m) >>= (\ x -> (toListe . f $ x) >>= (toListe . g)))
-- = toMenge' (((toListe m) >>= (toListe . f)) >>= (toListe . g))
-- = toMenge' ((toListe . toMenge' $ ((toListe m) >>= (toListe . f))) >>= (toListe . g))
-- = toMenge' ((toListe (m >>= f)) >>= (toListe . g))
-- = (m >>= f) >>= g


-- | Mengen als MonadPlus
instance MonadPlus MeineMenge where
  mzero = Leer -- D0
  Leer `mplus` m = m -- D1
  m `mplus` Leer = m -- D2
  m `mplus` n = toMenge' ((toListe m) `mplus` (toListe n)) -- D3

-- | Beweis:
-- 1. Neutrales Element:
-- mzero `mplus` m (D0)
-- = Leer `mplus` m (D1)
-- = m 
-- m `mplus` mzero (D0)
-- = m `mplus` Leer (D2)
-- = m
-- 2. Assoziativität:
-- a) Leer `mplus` (n `mplus` o) (D1)
-- = n `mplus` o (D1)
-- = (Leer `mplus` n) `mplus` o
-- b) m `mplus` (Leer `mplus` o) (D1)
-- = m `mplus` o (D1)
-- = (m `mplus` Leer) `mplus` o
-- c) m `mplus` (n `mplus` Leer) (D1)
-- m `mplus` n (D1)
-- (m `mplus` n) `mplus` Leer
-- d) m `mplus` (n `mplus` o) (D3)
-- = toMenge' ((toListe m) `mplus` (toListe $ (toMenge' (toListe n) `mplus` (toListe o))))
-- = toMenge' ((toListe m) `mplus` ((toListe n) `mplus` (toListe o)))
-- = toMenge' (((toListe m) `mplus` (toListe n)) `mplus` (toListe o))
-- = toMenge' ((toListe . toMenge' ((toListe m) `mplus` (toListe n))) `mplus` (toListe o))
-- = toMenge' ((toListe (m `mplus` n)) `mplus` (toListe o))
-- = (m `mplus` n) `mplus` o

-- | Kleisli Komposition von Mengen:
-- f >=> g 
-- = (\ x -> f x >>= g) 
-- = (\ x -> toMenge' ((toListe (f x)) >>= (toListe . g)))
-- = (\ x -> toMenge' (concat . map (toListe . g) . (toListe . f))) (Aufgabe 6)


