module HaskellFuerDenRestVonUns.Kapitel7.Aufgabe5 (Baum (..), interpretiere, operation) where

data Baum a 
  = Knoten a (Baum a) (Baum a)
  | Leer
  deriving (Show, Eq)

-- | Interpretiert einen als Baum geparsten Arithmetischen Ausdruck
interpretiere :: Baum [Char] -> Maybe Int
interpretiere Leer = Nothing
interpretiere (Knoten nr Leer Leer) = Just (read nr)
interpretiere (Knoten op l r) = 
  do
    f <- operation op
    l <- interpretiere l
    r <- interpretiere r
    f l r

-- | Übersetzt einen String in eine Arithmetische Funktion
operation :: String -> Maybe (Int -> Int -> Maybe Int)
operation "+" = Just (\ x y -> return (x + y))
operation "-" = Just (\ x y -> return (x - y))
operation "*" = Just (\ x y -> return (x * y))
operation "/" = Just (\ x y -> if y == 0 then Nothing else (return (x `div` y)))
operation _   = Nothing