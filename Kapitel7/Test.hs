module Main where

import qualified HaskellFuerDenRestVonUns.Kapitel7.Test.Aufgabe1 as K7A1
import qualified HaskellFuerDenRestVonUns.Kapitel7.Test.Aufgabe5 as K7A5
import qualified HaskellFuerDenRestVonUns.Kapitel7.Test.Aufgabe7 as K7A7
import qualified HaskellFuerDenRestVonUns.Kapitel7.Test.Aufgabe8 as K7A8
import qualified HaskellFuerDenRestVonUns.Kapitel7.Test.Aufgabe9 as K7A9

main = do
  K7A1.runTests
  K7A5.runTests
  K7A7.runTests
  K7A8.runTests
  K7A9.runTests
