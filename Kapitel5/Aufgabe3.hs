module HaskellFuerDenRestVonUns.Kapitel5.Aufgabe3
  ( baumEinf
  , baumSuche
  , istNachfolger
  , leiterGraph
  , bestimmeLeiter
  , existiertLeiter
  , main
  ) where

import System.IO

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5 as A5
import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11 as A11

-- | Fügt in einen binären Suchbaum ein (siehe 4.3.2)
baumEinf :: (Ord a) => a -> Baum a -> Baum a
baumEinf x A5.Leer = Knoten x A5.Leer A5.Leer
baumEinf x ( Knoten w links rechts)
  | x == w = Knoten x links rechts
  | x < w = Knoten w (baumEinf x links) rechts
  | x > w = Knoten w links (baumEinf x rechts)

-- | Sucht in einem binären Suchbaum (siehe 4.3.2)
baumSuche :: (Ord a) => a -> Baum a -> Bool
baumSuche x A5.Leer = False
baumSuche x ( Knoten w links rechts)
  | x == w = True
  | x < w = baumSuche x links
  | x > w = baumSuche x rechts

-- | Bestimmt für ein Wort, ob ein anderes
-- Wort sein direkter Nachfolger durch verändern
-- eines Buchstabens ist.
istNachfolger :: String -> String -> Bool
istNachfolger wort wort'
  | length wort /= length wort' = False
  | otherwise =
    1 == (foldl zähleUngleich 0 (zip wort wort'))
  where
    zähleUngleich n (x, y)
      | x == y = n
      | x /= y = n+1

-- | Baut aus einer Liste von Wörtern den zugehörigen
-- Graphen mit Kanten, wenn sich Wörter in einem
-- Buchstaben unterscheiden. Im Graphen werden die
-- platznummern der Wörter abgelegt.
leiterGraph :: [String] -> GGraph (Int, String)
leiterGraph wörter =
  foldl (\ g (x, y) -> neueKante g x y)
        (GGraph A11.Leer A11.Leer)
        [((nr, w), (nr', w')) | (nr, w) <- mitNummern wörter
                              , (nr', w') <- mitNummern wörter
                              , w' `istNachfolger` w
        ]
  where
    mitNummern wörter = zip [1..] wörter

-- | Bestimmt die Leiter zwischen zwei Wörtern in einem
-- gegebenen Graph, falls diese existiert
bestimmeLeiter ::
  GGraph (Int, String)
  -- ^ Graph mit benachbarten Wörtern
  -> (Int, String)
  -- ^ Platznummer und Wort als Start
  -> (Int, String)
  -- ^ Platznummer und Wort als Ziel
  -> [(Int, String)]
bestimmeLeiter graph start ziel
  | not (start `element` (knoten graph)) = []
  | not (ziel `element` (knoten graph)) = []
  | otherwise =
    bestimmeLeiter' (toListe $ kanten graph) ziel A5.Leer [[start]]
  where
    bestimmeLeiter' kanten ziel gesehen ((w:l):ls)
      | w == ziel = (w:l)
      | baumSuche w gesehen =
        bestimmeLeiter' kanten ziel gesehen ls
      | otherwise =
        bestimmeLeiter' kanten ziel (baumEinf w gesehen) $
          ls ++ (mitLeiter (w:l) $ nachfolger kanten w)
    bestimmeLeiter' _ _ _ [] = []

    mitLeiter l nachfolger = map (:l) nachfolger
    nachfolger kanten wort =
      map (\ (_ :->: ziel) -> ziel) $
      filter (\ (wort' :->: ziel) -> wort' == wort) kanten

-- | Bestimmt, ob zwischen den Wörtern im gegebenen
-- Graph eine Leiter gebaut werden kann
existiertLeiter ::
  GGraph (Int, String)
    -> (Int, String)
    -> (Int, String)
    -> Bool
existiertLeiter graph wort wort' =
  bestimmeLeiter graph wort wort' /= []


main = do
  putStrLn "Eingabedatei?"
  dateiname <- getLine
  putStrLn "Leiter von nr:"
  startNr <- getLine
  putStrLn "nach nr:"
  zielNr <- getLine
  inhalt <- readFile dateiname
  let
    start = wörter !! (read startNr - 1)
    ziel = wörter !! (read zielNr - 1)
    wörter = words inhalt
    graph = leiterGraph wörter
  putStrLn "Die Leiter:"
  putStrLn . show $
       bestimmeLeiter graph (read startNr, start) (read zielNr, ziel)
