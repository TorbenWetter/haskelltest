{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel5.Test.Aufgabe4 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5 as A5
import HaskellFuerDenRestVonUns.Kapitel5.Aufgabe4 as A4

-- | Zufällige Suchbauminstanzen
instance (Arbitrary a, Bounded a, Ord a) => Arbitrary (A5.Baum a) where
  arbitrary = suchBaum (< maxBound)
    where
      suchBaum pred = oneof [ return A5.Leer
                            , (arbitrary `suchThat` (pred)) >>= 
                              (\ n -> A5.Knoten n <$> suchBaum (< n) 
                                                  <*> suchBaum (> n))
                            ]

-- | Testet, ob die Suchbaum-Eigenschaft beim Kombinieren erhalten
-- bleibt
prop_suchBaum :: A5.Baum Int -> A5.Baum Int -> Bool
prop_suchBaum b1 b2 = istSuchbaum (kombiniere b1 b2)
  where
    istSuchbaum (A5.Leer) = True
    istSuchbaum (A5.Knoten x l@(A5.Knoten y _ _) r@(A5.Knoten z _ _)) = x > y && x < z && istSuchbaum l && istSuchbaum r
    istSuchbaum (A5.Knoten x l@(A5.Knoten y _ _) (A5.Leer)) = x > y && istSuchbaum l
    istSuchbaum (A5.Knoten x (A5.Leer) r@(A5.Knoten z _ _)) = x < z && istSuchbaum r
    istSuchbaum (A5.Knoten x (A5.Leer) (A5.Leer)) = True

-- | Testet, ob alle Knoten der beiden Teilbäume im kombinierten Baum vorhanden sind
prop_alleKnoten :: A5.Baum Int -> A5.Baum Int -> Bool
prop_alleKnoten b1 b2 = 
  alleKnoten b1 (kombiniere b1 b2) && alleKnoten b2 (kombiniere b1 b2)
  where
    alleKnoten (A5.Leer) _ = True
    alleKnoten b@(A5.Knoten x l r) (A5.Knoten y l' r')
      | x == y = alleKnoten l l' && alleKnoten r r'
      | x < y = alleKnoten b l'
      | x > y = alleKnoten b r'
    alleKnoten (A5.Knoten _ _ _) (A5.Leer) = False


runTests = $quickCheckAll