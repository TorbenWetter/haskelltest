module HaskellFuerDenRestVonUns.Kapitel5.Aufgabe1
  ( Koordinate (..)
  , Status (..)
  , Spielfeld (..)
  , leeresSpielfeld
  , zufallsKoordinate
  , istErlaubt
  , belege
  , belegeSpielfeldeintrag
  , Schiff
  , platziere
  , beschiesse
  , versenke
  , Spieler (..)
  , getSpielfeld
  , setSpielfeld
  , getBeschossen
  , setBeschossen
  , istKI
  , verloren
  , Spiel (..)
  , kiSchuss
  , runde
  , schiffeVersenken
  ) where

import System.Random

-- | Spielfeldkoordinaten
type Koordinate = (Char, Int)

-- | Status einer Koordinate
data Status =
  Besetzt | Versenkt | Wasser
  deriving (Ord, Eq)

-- | Ein Spielfeld ordnet Koordinaten ihren Status zu
newtype Spielfeld =
  Spielfeld [(Koordinate, Status)]
  deriving (Ord, Eq)

-- | Erzeugt ein leeres Spielfeld
leeresSpielfeld =
  Spielfeld [((reihe, spalte), Wasser) | reihe <- ['A'..'J']
                                       , spalte <- [1..10]
            ]

-- | Erzeugt eine zufällige Koordinate und gibt sie
-- mit dem benutzen Zufallsgenerator zurück.
zufallsKoordinate :: (RandomGen g) => g -> (Koordinate, g)
zufallsKoordinate gen =
  let
    (r, gen') = random gen
    (s, gen'') = random gen'
    reihe = ['A'..'J'] !! (r `mod` 10)
    spalte = (s `mod` 10) + 1
  in
    ((reihe, spalte), gen'')



-- | Prüft, ob auf der Koordinate ein Schiff platziert werden kann
istErlaubt :: Spielfeld -> Koordinate -> Bool
istErlaubt (Spielfeld koords) (reihe, spalte) =
  case lookup (reihe, spalte) koords of
    Just Wasser ->
      and [frei koords (r, s) | r <- [pred reihe, reihe, succ reihe]
                              , s <- [pred spalte, spalte, succ spalte]
      ]
    _ -> False
  where
    frei koords koord =
      case lookup koord koords of
        Just Wasser -> True
        Nothing -> True
        _ -> False

-- | Belegt Koordinaten auf einem Spielfeld, wenn möglich
belege :: Spielfeld -> [Koordinate] -> Maybe Spielfeld
belege spielfeld@(Spielfeld koords) zuBelegen
  | all (istErlaubt spielfeld) zuBelegen =
    Just . Spielfeld $ map (belegeSpielfeldeintrag zuBelegen) koords
  | otherwise = Nothing

-- | Belegt einen Spielfeldeintrag, wenn seine Koordinate
-- in der Übergebenen Liste ist.
belegeSpielfeldeintrag :: [Koordinate] -> (Koordinate, Status) -> (Koordinate, Status)
belegeSpielfeldeintrag zuBelegen (koord, status)
      | koord `elem` zuBelegen = (koord, Besetzt)
      | otherwise = (koord, status)

-- | Ein Schiff wird durch die Zahl seiner belegten Kästchen beschrieben
type Schiff = Int

-- | Versucht die gegebene Liste von Schiffen zufällig auf dem Spielfeld
-- zu platzieren
platziere :: (RandomGen g)
             => g
             -- ^ Ein Zufallszahlengenerator
             -> Spielfeld
             -- ^ Das Spielfeld, auf dem platziert wird
             -> [Schiff]
             -- ^ Die zu platzierenden Schiffe
             -> (Spielfeld, g)
             -- ^ Das Spielfeld mit den Schiffen
             -- und der benutzte Zufallsgenerator.
platziere gen spielfeld [] = (spielfeld, gen)
platziere gen spielfeld (schiff:schiffe) =
  let
    (horizontal, gen') = random gen
    ((reihe, spalte), gen'') = zufallsKoordinate gen'
  in
    case belege spielfeld (schiffsKoordinaten schiff
                                              horizontal
                                              reihe
                                              spalte) of
      Nothing -> platziere gen'' spielfeld (schiff:schiffe)
      Just spielfeld' -> platziere gen'' spielfeld' schiffe
  where
    schiffsKoordinaten schiff horizontal reihe spalte
      | horizontal = take schiff [(r, spalte) | r <- [reihe..  ]]
      | otherwise  = take schiff [(reihe, s)  | s <- [spalte.. ]]

-- | Beschießt die gewählte Koordinate und gibt den Erfolg und das
-- neue Spielfeld zurück, falls die Koordinate gültig war.
beschiesse :: Spielfeld -> Koordinate -> Maybe (Bool, Spielfeld)
beschiesse spielfeld@(Spielfeld koords) ziel =
  case lookup ziel koords of
    Just Besetzt -> Just (True, versenke spielfeld ziel)
    Just _ -> Just (False, spielfeld)
    Nothing -> Nothing

-- | Setzt den Zustand der Zielkoordinate auf 'Versenkt'.
versenke :: Spielfeld -> Koordinate -> Spielfeld
versenke (Spielfeld koords) ziel =
  Spielfeld $ map (versenke' ziel) koords
  where
    versenke' ziel (koord, status)
      | ziel == koord = (koord, Versenkt)
      | otherwise = (koord, status)

-- | Datentyp für Spieler (ggF. mit Zufallsgenerator gen)
data Spieler gen
  = Mensch Spielfeld [Koordinate]
    -- ^ menschlicher Spieler mit beschossenen koordinaten
  | KI Spielfeld [Koordinate] gen
    -- ^ Computer mit Zufallsgenerator

-- | Extrahiert das Spielfeld eines Spielers
getSpielfeld :: Spieler gen -> Spielfeld
getSpielfeld (Mensch spielfeld _) = spielfeld
getSpielfeld (KI spielfeld _ _) = spielfeld

-- | Setzt das Spielfeld eines Spielers
setSpielfeld :: Spielfeld -> Spieler gen -> Spieler gen
setSpielfeld spielfeld (Mensch _ b) = Mensch spielfeld b
setSpielfeld spielfeld (KI _ b g) = KI spielfeld b g

-- | Extrahiert die beschossenen Koordinaten eines Spielers
getBeschossen :: Spieler gen -> [Koordinate]
getBeschossen (Mensch _ beschossen) = beschossen
getBeschossen (KI _ beschossen _) = beschossen

-- | Setzt die beschossenen Koordinaten eines Spielers
setBeschossen :: [Koordinate] -> Spieler gen -> Spieler gen
setBeschossen beschossen (Mensch s _) = Mensch s beschossen
setBeschossen beschossen (KI s _ g) = KI s beschossen g

-- | Bestimmt, ob ein Spieler ein Computer ist
istKI :: Spieler gen -> Bool
istKI (KI _ _ _) = True
istKI _ = False

-- | Prüft, ob ein Spieler verloren hat, weil sein
-- Spielfeld 30 terffer abbekommen hat.
verloren :: Spieler gen -> Bool
verloren spieler =
  let
    (Spielfeld koords) = getSpielfeld spieler
  in
    foldl zähleTreffer 0 koords == 30
  where
    zähleTreffer treffer (_, Versenkt) = treffer + 1
    zähleTreffer treffer _ = treffer

-- | Ein Spiel besteht aus zwei Spielern
data Spiel gen = Spiel (Spieler gen) (Spieler gen)

-- | Anzeigen der Spielfelder
instance Show (Spiel gen) where
  show (Spiel s1 s2) =
    show' (getBeschossen s2) s1 ++ "\n" ++
          show' (getBeschossen s1) s2
    where
      show' beschossen spieler =
        let
          (Spielfeld koords) = getSpielfeld spieler
          name = if istKI spieler
                 then ("Computer:\n")
                 else ("Mensch:\n")
        in
          name ++ "   |1|2|3|4|5|6|7|8|9|10|\n" ++
               foldl (druckeReihe beschossen koords) "" ['A'..'J']
      druckeReihe beschossen koords str reihe =
        str ++ [reihe] ++ ": " ++
            foldl (druckeSpalte beschossen koords reihe) "" [1..10] ++ " |\n"
      druckeSpalte beschossen koords reihe str spalte =
        case lookup (reihe, spalte) koords of
          Just Versenkt -> str ++ "|o"
          _ -> if (reihe, spalte) `elem` beschossen
               then str ++ "|x"
               else str ++ "| "


-- | Lässt den Computer eine zufällige unbeschossene
-- Koordinate erzeugen und aktualisiert seinen Zufallsgenerator.
kiSchuss :: (RandomGen gen) => Spiel gen -> (Koordinate, Spiel gen)
kiSchuss (Spiel (KI spielfeld beschossen gen) s) =
  let
    (koord, gen') = zufallsKoordinate gen
  in
    if koord `elem` beschossen
    then kiSchuss (Spiel (KI spielfeld beschossen gen') s)
    else (koord, (Spiel (KI spielfeld beschossen gen') s))
kiSchuss (Spiel s ki@(KI _ _ _)) =
  let
    (k, (Spiel ki' s')) = kiSchuss (Spiel ki s)
  in
    (k, (Spiel s' ki'))


-- | Spielt eine Spielrunde mit Beschuss der gegebenen
-- Koordinate. Vertauscht die Spielerpositionen, falls
-- nicht getroffen wurde.
runde :: Koordinate -> Spiel gen -> Spiel gen
runde koord (Spiel s1 s2) =
  case beschiesse (getSpielfeld s2) koord of
    Just (True, spielfeld) ->
      Spiel (setBeschossen (koord:getBeschossen s1) s1)
            (setSpielfeld spielfeld s2)
    otherwise ->
      Spiel s2 (setBeschossen (koord:getBeschossen s1) s1)

schiffeVersenken :: IO ()
schiffeVersenken = do
  gen <- newStdGen
  let
    (kiFeld, gen') =
      platziere gen leeresSpielfeld [5, 4, 4, 3, 3, 3, 2, 2, 2, 2]
    spiel = Spiel (Mensch leeresSpielfeld []) (KI kiFeld [] gen')
  schiffeVersenken' spiel
  where
    schiffeVersenken' spiel@(Spiel s1@(KI _ _ _) s2) = do
                     putStrLn (show spiel)
                     putStrLn "Computer am zug."
                     let
                       (koord, gen') = kiSchuss spiel
                     putStrLn $ "Treffer bei "
                                ++ show koord
                                ++ "? (True/False)"
                     treffer <- readLn
                     let
                       (Spielfeld koords) = getSpielfeld s2
                       koords' = map (belegeSpielfeldeintrag [koord]) koords
                       s2' = if treffer
                             then setSpielfeld (Spielfeld koords') s2
                             else s2
                       (Spiel s1' s2'') = runde koord (Spiel s1 s2')
                     if verloren s2''
                       then do putStrLn "Sie haben verloren"
                       else do schiffeVersenken' (Spiel s1' s2'')
    schiffeVersenken' spiel@(Spiel s1@(Mensch _ _) s2) = do
                     putStrLn (show spiel)
                     putStrLn "Sie sind am zug."
                     putStrLn "Koordinate? ('A-J', 1-10)"
                     koord <- readLn
                     let
                       (Spiel s1' s2') = runde koord spiel
                     if verloren s2'
                       then putStrLn "Sie haben gewonnen!"
                       else schiffeVersenken' (Spiel s1' s2')





