module HaskellFuerDenRestVonUns.Kapitel5.Aufgabe4
  ( kombiniere
  , main
  ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5

-- | Kombiniert zwei binäre Suchbäume
kombiniere :: (Ord a) => Baum a -> Baum a -> Baum a
kombiniere b Leer = b
kombiniere Leer b = b
kombiniere (Knoten x l r) b@(Knoten y l' r')
  | x == y = Knoten x (kombiniere l l') (kombiniere r r')
  | x > y = Knoten x (kombiniere l b) r
  | x < y = Knoten x l (kombiniere r b)

main = do
  bs <- readFile "baum1"
  let
    baum :: Baum String
    baum = foldl1 kombiniere (read bs)
  writeFile "baum2" (show baum)
