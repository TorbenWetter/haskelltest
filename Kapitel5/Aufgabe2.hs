module HaskellFuerDenRestVonUns.Kapitel5.Aufgabe2
  ( dateiNamen
  , dateiEndung
  , umbenennung
  , zeilen
  , main
  ) where

import System.IO

-- | Erzeugt eine endlose Liste mit nummerierten Dateinamen
-- entsprechend der gegebenen Namensbasis und Endung
dateiNamen :: String -> String -> [String]
dateiNamen namensbasis endung =
  map (\ nr -> namensbasis ++ "-" ++ show nr ++ endung) [1..]

-- | Findet die Dateiendung einer Datei, durch Suche
-- nach einem Punkt
dateiEndung :: String -> String
dateiEndung dateiname =
  dropWhile (/= '.') dateiname



-- | Erzeugt für eine Namensbasis und eine Liste von Dateinamen
-- eine Liste mit Umbenennungskommandos
umbenennung :: String -> [String] -> [String]
umbenennung _ [] = []
umbenennung namensbasis ds@(datei:dateien) =
  let
    endung = dateiEndung datei
    anzahl = 1 + (length $ takeWhile (mitEndung endung) dateien)
  in
  (zipWith zuBefehl ds . take (anzahl) $ dateiNamen namensbasis endung) ++
  umbenennung namensbasis (drop anzahl ds)
  where
    mitEndung endung datei = dateiEndung datei == endung
    zuBefehl alt neu = "mv " ++ alt ++ " " ++ neu ++ "\n"


-- | Zerlegt einen String in Zeilen
zeilen :: String -> [String]
zeilen "" = []
zeilen str =
  (takeWhile (/= '\n') str) : (zeilen (tail $ dropWhile (/= '\n') str))

main = do
  putStrLn "Datei mit Liste?"
  dateienDatei <- getLine
  putStrLn "Namensbasis?"
  namensbasis <- getLine
  putStrLn "Ausgabe Datei?"
  ausgabeDatei <- getLine
  dateien <- readFile dateienDatei
  writeFile ausgabeDatei (concat $ umbenennung namensbasis (zeilen dateien))
