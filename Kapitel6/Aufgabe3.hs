module HaskellFuerDenRestVonUns.Kapitel6.Aufgabe3 ( patFind' ) where

import qualified Data.Map as Map
import HaskellFuerDenRestVonUns.Kapitel6.Aufgabe1

patFind' :: (Ord a) => [a] -> Int -> [a] -> Bool
patFind' pat k [] = k == (finalZust pat)
patFind' pat s (x:xs)
  | nxt == final = True
  | otherwise = patFind pat nxt xs
  where
    final = finalZust pat
    errVal = final + 1
    nxt = case Map.lookup (x, s) (derAutomat pat) of
            Nothing -> errVal
            Just n -> n
