module HaskellFuerDenRestVonUns.Kapitel6.Aufgabe2 
  ( freqMap
  , ffilter
  , fquickSort
  , Node (..)
  , mkZNode
  , mkNode
  , mkpairZNode
  , isZNode
  , isNode
  , Baum (..)
  , mergeBaum
  , weight
  , weightInsert
  , simpleBaum
  , mergeBaumList
  , recordNode
  , baumDurchlauf
  , encoding
  , huffman
  ) where

import qualified Data.Map as Map

freqMap :: (Ord k, Num a) => [k] -> [(k, a)]
freqMap xs = Map.toList (lookFreq xs)
  where
    lookFreq (y:ys) = Map.insertWith (+) y 1 (lookFreq ys)
    lookFreq [] = Map.empty

ffilter :: (a -> b) -> (b -> Bool) -> [a] -> [a]
ffilter f p = filter (p.f) 

fquickSort :: (Ord a) => (a1 -> a) -> [a1] -> [a1]
fquickSort f [] = []
fquickSort f (x:xs) = theSmaller ++ theEqual ++ theLarger
  where
    t = f x
    theSmaller = fquickSort f (ffilter f (< t) xs)
    theEqual = ffilter f (== t) (x:xs)
    theLarger = fquickSort f (ffilter f (> t) xs)

data Node
  = ZNode {theChar:: Char, often :: Integer}
  | Node {often :: Integer}
  deriving (Show, Read)

mkZNode :: Char -> Integer -> Node
mkZNode x i = ZNode{theChar = x, often = i}

mkNode :: Integer -> Node
mkNode i = Node {often = i}

mkpairZNode :: (Char, Integer) -> Node
mkpairZNode p = mkZNode (fst p) (snd p)

isZNode :: Node -> Bool
isZNode(ZNode _ _) = True
isZNode _= False

isNode :: Node -> Bool
isNode(Node _) = True
isNode _= False

data Baum 
  = Baum {aNode :: Node, left :: Baum, right :: Baum}
  | Leer
  deriving (Show, Read)

mergeBaum :: Baum -> Baum -> Baum
mergeBaum b1 b2 = Baum {aNode = thisNode, left = b1, right = b2}
  where
    thisNode = mkNode $ (weight b1) + (weight b2)

weight :: Baum -> Integer 
weight = often . aNode

weightInsert :: Baum -> [Baum] -> [Baum]
weightInsert y (x:xs) = 
  if f y <= f x
  then y:x:xs
  else x:(weightInsert y xs)
  where
    f = weight
weightInsert y [] = [y]


simpleBaum :: (Char, Integer) -> Baum
simpleBaum p = Baum (mkpairZNode p) Leer Leer

mergeBaumList :: [Baum] -> [Baum]
mergeBaumList (x:[]) = [x]
mergeBaumList (x:x':xs) = mergeBaumList newBaumList
  where 
    newBaumList = weightInsert (mergeBaum x x') xs


recordNode :: t -> Node -> [(Char, t)]
recordNode x kn = if (isZNode kn) then [(theChar kn, x)] else []

baumDurchlauf :: [Char] -> Baum -> [(Char, [Char])]
baumDurchlauf pfad (Baum wurzel linkerUnterB rechterUnterB) =
  encWurz ++ nachLinks ++ nachRechts
  where
    encWurz = recordNode pfad wurzel
    nachLinks = baumDurchlauf (pfad ++ "0") linkerUnterB
    nachRechts = baumDurchlauf (pfad ++ "1") rechterUnterB
baumDurchlauf pfad Leer = []

encoding :: [Char] -> [(Char, [Char])]
encoding charList = baumDurchlauf [] dieserBaum
  where
    wieOft = fquickSort snd (freqMap charList)
    kleineB = map simpleBaum wieOft
    dieserBaum = head (mergeBaumList kleineB)

-- | Wandelt eine Zeichenkette in ihre
-- Huffman kodierte binäre Repräsentation um.
huffman :: String -> String
huffman text = 
  concat $ map (findCode (encoding text)) text
  where
    findCode ((chr, code):codes) x
      | x == chr = code
      | otherwise = findCode codes x
