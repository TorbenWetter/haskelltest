module HaskellFuerDenRestVonUns.Kapitel6.Aufgabe7 
  ( uInsert
  , (+++)
  , conccatt
  , knoten
  , defAdj
  , mkRel
  , upd
  , tHul
  , Atom (..)
  , symbol2Atom
  , symbol2Atom1
  , atom2Symbol
  , atom2Symbol1
  , nachsehen
  , wup
  , puw
  , atomify
  , alleSymbole
  , alleNTs
  , alleTs
  , startSymb
  , dollar
  , eps
  , genIteration
  , iteration
  , inhGleich
  , alleEpsProd
  , einfProd
  , initFirst
  , makeRel
  , first
  , firstLhs
  , teilListen
  , alleTeile
  , deal
  , neuFollow
  , berechneFollow
  , closure
  , dieC
  , schrittCl
  , berechneClos
  , punktSchub
  , zuSchl
  , gotoP
  , gotoN
  , canonColl
  , alleZust
  , komponentNr
  , zustandNr
  , hilfGoto
  , berechneGoto
  , Aktion (..)
  , dieAktion
  , rechteListe
  , berechneAktion
  , istSLR
  , myLex
  , myUnLex
  , automat
  ) where

import Data.List ((\\))

uInsert :: (Eq t) => t -> [t] -> [t]
uInsert x [] = [x]
uInsert x (y:ys)
  | x == y = (y:ys)
  | otherwise = y:(uInsert x ys)

infixr 5 +++
(+++) :: (Eq a) => [a] -> [a] -> [a]
(+++) xs ys = foldr uInsert xs ys
  where
    insert x [] = [x]
    insert x (y:ys)
      | x == y = (y:ys)
      | otherwise = y:(uInsert x ys)

conccatt :: (Eq a) => [[a]]  -> [a]
conccatt xss = foldr (+++) [] xss

knoten :: (Eq b) => [(b, b)] -> [b]
knoten gr = (map fst gr) +++ (map snd gr)

defAdj :: (Eq b) => [(b, b)] -> b -> [b]
defAdj gr a = map snd $ filter (\x -> fst x == a) gr

mkRel :: (Eq t, Eq t1) => [t] -> (t -> [t1]) -> [(t, t1)]
mkRel kno adj = foldr uInsert [] [(x, y) | x <- kno, y <- adj x]

upd :: (Eq a) => (a, a) -> (a -> [a]) -> a -> [a]
upd (a, b) adj = neuAdj
  where
    t = adj a
    s = adj b
    neuAdj x
      | x == a = t +++ (if b `elem` t then s else [])
      | x == b = s +++ (if a `elem` s then t else [])
      | otherwise = adj x

upd1 :: (Eq a) => (a -> [a]) -> [(a, a)] -> (a -> [a])
upd1 adj ps = foldr upd adj ps

tHul :: (Eq b) => [(b, b)] -> [(b, b)]
tHul gr = mkRel dieKnoten (upd1 dieAdj allePaare)
  where
    dieKnoten = knoten gr
    dieAdj = defAdj gr
    allePaare = [(x, y) | x <- dieKnoten,
                          y <- dieKnoten, x /= y]


newtype Atom
  = Atom Integer deriving Eq

instance Show Atom where
  show (Atom k) = "at" ++ (show k)

symbol2Atom :: [([Char], [Char])] -> [([Char], Atom)]
symbol2Atom prod = zip aList bList
  where
    w q = words ((fst q) ++ " " ++ (snd q))
    aList = foldr uInsert ["eps"] (concat $ map w prod)
    bList = [Atom k | k <- [0 ..]]


symbol2Atom1 :: [([Char], [Char])] -> [([Char], Atom)]
symbol2Atom1 prod = 
  symbol2Atom prod +++
    (zip sonderZeichen [Atom k | k <- [l + 100 ..]])
  where
    l = toInteger $ length prod
    sonderZeichen = ["$"]

atom2Symbol :: [([Char], [Char])] -> [(Atom, [Char])]
atom2Symbol prod = [(snd x, fst x) | x <- symbol2Atom prod]

atom2Symbol1 :: [([Char], [Char])] -> [(Atom, [Char])]
atom2Symbol1 prod = [(snd x, fst x) | x <- symbol2Atom1 prod]

nachsehen :: (Eq t) => [(t, a)] -> t -> a -> a
nachsehen li t def =
  case lookup t li of
    Just x -> x
    Nothing -> def

wup :: (Eq t) => [(t, Atom)] -> t -> Atom
wup li t = nachsehen li t (Atom 0)

puw :: [(Atom, [Char])] -> Atom -> [Char]
puw li t = nachsehen li t ""

atomify :: [([Char], String)] -> [(Atom, [Atom])]
atomify prod = zip g p
  where
    li = symbol2Atom prod
    g = map (\c -> (wup li) (fst c)) prod
    ww = [words $ snd x | x <- prod]
    p = [map (wup li) y | y <- ww]

-- | alle Symbole der Grammatik
alleSymbole :: [([Char], [Char])] -> [Atom]
alleSymbole prod = foldr uInsert [] $ map snd (symbol2Atom prod)\\[eps]

-- | alle nicht-terminalen Symbole
alleNTs :: [([Char], String)] -> [Atom]
alleNTs prod = foldr uInsert [] $ map fst (atomify prod)

-- | alle terminalen Symbole
alleTs :: [([Char], [Char])] -> [Atom]
alleTs prod = (alleSymbole prod)\\(alleNTs prod)

-- ausgezeichnete Symbole
startSymb :: [([Char], String)] -> Atom
startSymb prod = fst $ head $ atomify prod

dollar :: [([Char], String)] -> Atom
dollar prod = wup (symbol2Atom1 prod) "$"

eps :: Atom
eps = Atom 0

genIteration :: (t -> t -> Bool) -> t -> t -> (t -> t) -> t
genIteration p x y f = if (p x y)
                       then x
                       else (genIteration p y (f y) f)

iteration :: (Eq a) => [a] -> [a] -> ([a] -> [a]) -> [a]
iteration = genIteration inhGleich

inhGleich :: (Eq a) => [a] -> [a] -> Bool
inhGleich tup1 tup2 = all (`elem` tup1) tup2 && all (`elem` tup2) tup1

-- alle nicht-terminalen Symbole mit epsilon-Produktionen
alleEpsProd :: [([Char], String)] -> [Atom]
alleEpsProd prod = iteration [] ePS (ePSN prod)
  where
    ePS = [fst k | k <- atomify prod, snd k == [eps]]
    ePSN p a = a +++ [fst k | k <- atomify p,
                              all (`elem` a) (snd k)]


einfProd :: [([Char], [Char])] -> [(Atom, Atom)]
einfProd prod = [(n, t) | (n, t:_) <- atomify prod
                        , t `elem` (alleTs prod)]

initFirst :: [([Char], String)] -> [(Atom, Atom)]
initFirst prod = [(x, x) | x <- ts] ++ [(x, eps) | x <- as]
  where
    ts = alleTs prod
    as = alleEpsProd prod

makeRel :: [([Char], String)] -> [(Atom, Atom)]
makeRel prod = map (\p -> (fst p, head $ snd p)) (atomify prod)

first :: [([Char], String)] -> [(Atom, Atom)]
first prod = filter w $ tHul $ (makeRel prod ++ dieTerms)
  where
    dieTerms = initFirst prod
    w p = snd p `notElem` alleNTs prod


firstLhs :: [([Char], String)] -> [Atom] -> [Atom]
firstLhs prod lhs = lgg (first prod) lhs lhs
  where
    lgg fi lhs [] = [eps]
    lgg fi lhs (x:xs) = add +++ bet
      where
        bet = map snd (filter (\w -> fst w == x && snd w /= eps) fi)
        add = if (x, eps) `elem` fi
              then lgg fi lhs xs
              else []

teilListen :: [([Char], String)] -> (t, [Atom]) -> [(t, [Atom])]
teilListen prod (x, y) = [(x, r) | r <- teilListen]
  where
    richtig p = head p `elem` alleNTs prod
    endStuecke = [drop k y | k <- [0 .. length y - 1]]
    teilListen = filter richtig endStuecke

alleTeile :: [([Char], String)] -> [(Atom, [Atom])]
alleTeile prod = concat $ map (teilListen prod) (atomify prod)

deal :: 
  [([Char], String)] 
  -> [(Atom, Atom)] 
  -> (Atom, [Atom]) 
  -> [(Atom, Atom)]
deal prod fo (x, y:ys)
  | ys /= [] = fo +++ [(y, r) | r <- fLhs, r /= eps] +++ aux
  | otherwise = fo +++ foX
  where
    fLhs = firstLhs prod ys
    foX = [(y, snd r) | r <- filter (\p -> fst p == x) fo]
    aux = if eps `elem` fLhs
          then foX
          else []

neuFollow :: [([Char], String)] -> [(Atom, Atom)] -> [(Atom, Atom)]
neuFollow prod fo = foldl (deal prod) fo (alleTeile prod)

berechneFollow :: [([Char], String)] -> [(Atom, Atom)]
berechneFollow prod = 
  let 
    st = [(startSymb prod, dollar prod)]
  in 
    iteration [] st (neuFollow prod)

closure :: (Eq a, Num t, Eq t) 
  => [(a, [a])] 
  -> [(Int, t)] 
  -> [(Int, Int)] 
  -> [(Int, t)]
closure prod items [] = items
closure prod items ((prodN, r):li) = 
  items +++ neueItems
  ++ (closure prod items li)
  where
    (_, rhs) = dieC prod prodN
    neuU
      | length rhs <= r = []
      | otherwise = filter (\p -> fst p == dieC rhs r) prod
    indTup = [0 .. length prod - 1]
    kandidaten = [k | k <- indTup, dieC prod k `elem` neuU]
    neueItems = map (\t -> (t, 0)) kandidaten

dieC :: [a] -> Int -> a
dieC xs k = head $ drop k xs

schrittCl :: (Eq a) => [(a, [a])] -> [(Int, Int)] -> [(Int, Int)]
schrittCl prod items = closure prod items items

berechneClos :: (Eq a) => [(a, [a])] -> [(Int, Int)] -> [(Int, Int)]
berechneClos prod items = iteration [] items (schrittCl prod)


punktSchub :: (Eq a) => [(t, [a])] -> a -> (Int, Int) -> [(Int, Int)]
punktSchub prod x (l, r)
  | length y <= r = []
  | dieC y r /= x = []
  | otherwise = [(l, r+1)]
  where
    (a, y) = dieC prod l

zuSchl :: (Eq a) => [(t, [a])] -> a -> [(Int, Int)] -> [(Int, Int)]
zuSchl prod a xs = conccatt $ map (punktSchub prod a) xs

gotoP :: (Eq a) => [(a, [a])] -> [(Int, Int)] -> a -> [(Int, Int)]
gotoP prodA itm x = berechneClos prodA (zuSchl prodA x itm)

gotoN :: [([Char], [Char])] -> [(Int, Int)] -> [[(Int, Int)]]
gotoN prod itm = 
  let
    alleS = alleSymbole prod
    atfy = atomify prod
    zuFiltern = [gotoP atfy itm x | x <- alleS]
  in 
    filter (/= []) zuFiltern

canonColl :: [([Char], String)] -> [[(Int, Int)]]
canonColl prod = iteration [] startWert (neuN prod)
  where
    startWert = [berechneClos (atomify prod) [(0, 0)]]
    neuN prod i = i +++ concat [gotoN prod j | j <- i]

alleZust :: [([Char], String)] -> [Int]
alleZust prod = [0 .. length (canonColl prod) - 1]

komponentNr :: [([Char], String)] -> Int -> [(Int, Int)]
komponentNr prod k = dieC (canonColl prod) k

zustandNr :: [([Char], String)] -> [(Int, Int)] -> Maybe Int
zustandNr prod i = derIndex (canonColl prod) i
  where
    derIndex xs x =
      if (x `notElem` xs)
      then Nothing
      else Just $ fst $ head $ filter (\t -> snd t == x) (zip [0 ..] xs)

hilfGoto :: 
  [([Char], String)] 
  -> [(Int, Atom)]
  -> [((Int, Atom), Int)] 
  -> [((Int, Atom), Int)]
hilfGoto _ [] li = li
hilfGoto prod ((z, nt):as) akkListe = hilfGoto prod as neuAkkListe
  where
    w = gotoP (atomify prod) (komponentNr prod z) nt
    neuAkkListe
      | w == [] = akkListe
      | otherwise = case zustandNr prod w of
                      Just x ->
                        uInsert ((z, nt), x) akkListe
                      Nothing -> akkListe

berechneGoto :: [([Char], String)] -> [((Int, Atom), Int)]
berechneGoto prod = 
  let
    alleS = alleSymbole prod
    alleZ = alleZust prod
    paarListe = [(z, t) | z <- alleZ, t <- alleS]
  in
    hilfGoto prod paarListe []

data Aktion = Reduce Int | Shift Int deriving (Eq, Show)

dieAktion :: 
  (Eq t) 
  => [([Char], String)] 
  -> [((t, Atom), Int)] 
  -> t
  -> (Int, Int) 
  -> [((t, Atom), Aktion)]
dieAktion prod gotoListe zst (pr, r) = eineListe
  where
    (lhs, rhs) = dieC (atomify prod) pr -- die Produktion
    eineListe
      | r == length rhs = rechteListe prod zst pr
      | otherwise =
        let 
          sbl = dieC rhs r
        in
          case lookup (zst, sbl) gotoListe of
            Just j -> if sbl `elem` alleTs prod
                      then [((zst, sbl), Shift j)]
                      else []
            Nothing -> []

rechteListe :: 
  [([Char], String)] 
  -> t 
  -> Int
  -> [((t, Atom), Aktion)]
rechteListe prod zst pr = 
  let
    (lhs, rhs) = dieC (atomify prod) pr
    guck t = fst t == lhs
    folgt = berechneFollow prod
    dieAs = map snd $ filter guck folgt
  in
    [((zst, a), Reduce pr) | a <- dieAs]


berechneAktion :: [([Char], String)] -> [((Int, Atom), Aktion)]
berechneAktion prod = 
  let
    cc = canonColl prod
    gt = berechneGoto prod
    da = dieAktion prod gt
    imZustand t = conccatt $ map (da t) (dieC cc t)
  in
    conccatt $ map imZustand (alleZust prod)

istSLR :: [([Char], String)] -> Bool
istSLR prod = 
  let
    akt = berechneAktion prod
    w j = filter (\r -> fst r == j) akt
  in 
    all (\d -> length d == 1) [w j | j <- map fst akt]

-- | Aktionsergebnis einer Automatenaktion
data AktionsErgebnis 
  = Fehler [Either (Int, Atom) Int] [Atom]
  | Nächstes [Either (Int, Atom) Int] [Atom]
  | Fertig [Atom]
  deriving (Eq, Show)

-- | Eine einzelne Aktion eines Automaten
aktion :: 
  Atom
  -- ^ Das Startsymbol
  -> [(Atom, [Atom])]
  -- ^ Produktionsregeln
  -> [((Int, Atom), Int)]
  -- ^ Goto Tablle
  -> [((Int, Atom), Aktion)]
  -- ^ Aktionstabelle
  -> AktionsErgebnis
   -- ^ vorheriges Ergebnis
  -> [Atom]
  -- ^ Eingaben
  -> (AktionsErgebnis, [Atom])
aktion _ _ _ _ f@(Fehler _ _) e = (f, e)
aktion _ _ _ _ (Fertig ok) e = (Fertig ok, e)
aktion _ _ _ _ (Nächstes n ok) [] = (Fehler n ok, [])
aktion startS
       aprod 
       goto
       tabelle 
       (Nächstes (top:stack) ok) 
       (e : es) =
      case (lookup (getZustand top, e) tabelle) of
        Just (Shift z') -> (Nächstes ((Left (z', e)):top:stack) ok, es)
        Just (Reduce i) -> 
          reduce startS
                 goto 
                 (aprod !! i) 
                 (top:stack)
                 ok
                 (e:es)
        Nothing -> (Fehler (top:stack) ok, (e:es))
  where
    reduce _ _ (a, rs) [] ok e = (Fehler [] ok, e)
    reduce start goto (a, (r:rs)) ss@((Left (_, sa)):stack) ok e 
      | (last $ r:rs) /= sa = (Fehler ss ok, e)
      | (last $ r:rs) == sa = 
        reduce start goto (a, (init $ r:rs)) stack (sa:ok) e
    reduce startS goto (a, []) ss@(s:stack) ok e
      | a == startS = (Fertig ok, e)
      | otherwise = 
        case lookup (getZustand s, a) goto of
          Just s' -> (Nächstes ((Left (s', a)):ss) ok, e)
          Nothing -> (Fehler ss ok, e)
    getZustand (Left (x, _)) = x
    getZustand (Right x) = x

-- | Wandelt einen String in die zugehörigen terminalen
-- Grammatikatome
myLex :: [([Char], String)] -> String -> [Atom]
myLex prod s = map (lex' prod) $ words s
  where
    lex' prod "$" = dollar prod
    lex' prod w
      | (wup (symbol2Atom1 prod) w) `elem` (alleTs prod) = 
        (wup (symbol2Atom1 prod) w)
      | otherwise = eps

-- | Wandelt eine Liste von Grammatikatomen in einen
-- String
myUnLex :: [([Char], String)] -> [Atom] -> String
myUnLex prod as = 
  concat $ map (puw (atom2Symbol1 prod)) as

-- | Automat, der zur gegebenen Grammatik  den Eingabestring
-- parst und entweder den Zustand mit den abgearbeiteten Symbolen
-- zurückgibt, bei dem ein Fehler aufgetreten ist, oder die 
-- abgearbeiteten Grammatiksymbole mit dem verbleibenden String.
automat :: 
  [([Char], String)] 
  -> String 
  -> Either ([Either (Int, Atom) Int], [Atom]) ([Atom], String)
automat prod eingabe =
  let
    goto = berechneGoto prod
    tabelle = berechneAktion prod
    aprod = atomify prod
    start = startSymb prod
    atomEingabe = myLex prod (eingabe ++ " $")
  in
    automat' start 
             prod 
             aprod 
             goto 
             tabelle 
             (Nächstes [Right 0] [])
             (atomEingabe)
  where
    automat' s ps aps g at st ae =
      case aktion s aps g at st ae of
        f@(Fehler s e, _) -> Left (s, e)
        (Fertig ok, ae') -> Right (ok, myUnLex ps ae')
        (n@(Nächstes _ _), ae') -> automat' s ps aps g at n ae'