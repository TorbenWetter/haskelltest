module HaskellFuerDenRestVonUns.Kapitel6.Aufgabe6
  ( readMakro
  , makroDef
  , makroNamen
  ) where

import HaskellFuerDenRestVonUns.Kapitel6.Aufgabe2 (fquickSort)
import HaskellFuerDenRestVonUns.Kapitel6.Aufgabe5 (split)

-- | Datentyp für Makros
data Makro 
  = Makro
  { name :: String
  , parameter :: Int
  , defaultParameter :: Maybe String
  , text :: String
  } deriving (Show, Ord, Eq)

-- | Liest ein Makro aus einem Text ein und
-- gibt das gelesene Makro sowie
-- den verbleibenden Text zurück, wenn
-- alles klappt.
readMakro :: String -> Maybe (Makro, String)
readMakro str =
  let
    str' = readNewCommand str
  in
    case readNewCommand str of
      Nothing -> Nothing
      Just rest ->
        case readMakroName rest of
          Nothing -> Nothing
          Just (name, rest) -> 
            case readParamAnzahl rest of
              Nothing -> Nothing
              Just (anz, rest) -> 
                case readDefaultParameter rest of
                  Nothing -> Nothing
                  Just (defaultParam, rest) ->
                    case readMakroText anz rest of
                      Nothing -> Nothing
                      Just (text, rest) ->
                        Just (Makro name anz defaultParam text, rest)  
  where
    ncLength = length "\\newcommand{"
    readNewCommand str
      | (length str >= ncLength)
        && (take ncLength str == "\\newcommand{") =
        Just $ drop ncLength str
      | otherwise = Nothing

    readMakroName ('\\':str)
      | '}' `elem` str &&
        (all (not . (`elem` verboten)) $ takeWhile (/= '}') str) =
        Just (takeWhile (/= '}') str, tail $ dropWhile (/= '}') str)
      | otherwise = Nothing
    readMakroName _ = Nothing

    verboten = ['\\', '\t', ' ', '}']
    
    readParamAnzahl str@('{':_) = Just (0, str)
    readParamAnzahl ('[':c:']':str)
      | c `elem` ['0'..'9'] = Just (read [c], str)
      | otherwise = Nothing
    readParamAnzahl _ = Nothing

    readDefaultParameter str@('{':_) = Just (Nothing, str)
    readDefaultParameter ('[':str)
      | (']' `elem` str) =
        Just (Just $ takeWhile (/= ']') str, tail $ dropWhile (/= ']') str) 
      | otherwise = Nothing
    readDefaultParameter _ = Nothing

    readMakroText paramAnzahl ('{':str) =
      case foldl bisZurKlammer (1, "") str of
        (nr, text) | nr == 0 && paramsOk paramAnzahl text ->
                     Just (text, drop (length text) str)
        _ -> Nothing
    readMakroText _ _ = Nothing
    
    bisZurKlammer (nr, str) c
      | nr <= 0  = (nr, str)
      | c == '{' = (nr + 1, (str ++ "{"))
      | c == '}' = (nr - 1, (str ++ "}"))
      | otherwise = (nr, (str ++ [c]))
    
    paramsOk n ('#':k:str) = 
      k `elem` ['1'..'9'] && (read [k]) <= n && paramsOk n str
    paramsOk n (x:str) = paramsOk n str
    paramsOk _ [] = True

-- | Liest eine Liste von Makros aus einem String aus
readMakros :: String -> [Makro]
readMakros "" = []
readMakros str =
  case readMakro (strip str) of
    Just (m, rest) -> m:(readMakros rest)
    Nothing -> readMakros (dropWhile (/= '\\') $ tail str)
  where
    strip ('%':str) = 
      strip . tail $ dropWhile (/= '\n') str
    strip (c:str) = c:(strip str)
    strip "" = ""
  
-- | Gibt eine alphabetisch sortierte Liste aller
-- Makro namen aus
makroNamen :: [Makro] -> [String]
makroNamen ms = 
  fquickSort id . map (name) $ ms

-- | Gibt die Definition des Makros mit dem gesuchten
-- Namen aus, wenn es vorhanden ist.
makroDef :: String -> [Makro] -> Maybe Makro
makroDef n makros = 
  case filter ((n ==) . name) makros of
    [] -> Nothing
    (m:ms) -> Just m

-- | Liest die gewünschte Makrodefinition aus Makros.tex
makroDefDatei :: String -> IO (Maybe Makro)
makroDefDatei mn = do
  makroString <- readFile "Makros.tex"
  return $ makroDef mn (readMakros makroString)

-- | Liest die Namen aller Makros aus Makros.tex
makroNamenDatei :: IO [String]
makroNamenDatei = do
  makroString <- readFile "Makros.tex"
  return $ makroNamen (readMakros makroString)