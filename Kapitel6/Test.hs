module Main where

import qualified HaskellFuerDenRestVonUns.Kapitel6.Test.Aufgabe1 as K6A1
import qualified HaskellFuerDenRestVonUns.Kapitel6.Test.Aufgabe3 as K6A3
import qualified HaskellFuerDenRestVonUns.Kapitel6.Test.Aufgabe4 as K6A4
import qualified HaskellFuerDenRestVonUns.Kapitel6.Test.Aufgabe7 as K6A7

main = do
  K6A1.runTests
  K6A3.runTests
  K6A4.runTests
  K6A7.runTests
