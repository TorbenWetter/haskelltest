module HaskellFuerDenRestVonUns.Kapitel6.Aufgabe1 
  ( istSuffix
  , sigma
  , derAutomat
  , finalZust
  , patFind
  , patMatch
  , patMatchIrgendwo
  , patReplace
  , findInFile
  , replaceInFile
  ) where

import Data.List (nub)
import qualified Data.Map as Map

istSuffix :: (Eq a) => [a] -> [a] -> Bool
istSuffix xs ys = 
  let 
    k = (length ys) - (length xs)
  in xs == drop k ys

sigma :: (Eq a) => Int -> a -> [a] -> Int
sigma q a pat = dasMax [gell | gell <- [0 .. length pat], passtScho gell]
  where
    dasMax (x:xs) = foldr max x xs
    pqa = (take q pat) ++ [a]
    passtScho m = istSuffix (take m pat) pqa


derAutomat :: (Ord a) => [a] -> Map.Map (a, Int) Int
derAutomat pat = Map.fromList aListe
  where
    patt = pat
    lgth = length patt
    aListe = [((a, q), sigma q a patt) | a <- patt, q <- [0 .. lgth]]

finalZust pat = length pat

patFind :: (Ord a) => [a] -> Int -> [a] -> Bool
patFind pat k [] = k == (finalZust pat)
patFind pat s (x:xs)
  | nxt == final = True
  | otherwise = patFind pat nxt xs
  where
    final = finalZust pat
    errVal = final + 1
    nxt = Map.findWithDefault errVal (x, s) (derAutomat pat)

patMatch :: (Ord a) => [a] -> [a] -> Bool
patMatch pat text = patFind pat 0 text

-- | Prüft, ob das gegebene Muster irgendwo im Text vorkommt
patMatchIrgendwo :: (Ord a) => [a] -> [a] -> Bool
patMatchIrgendwo pat text =
  snd $ foldl (patFind' (finalZust pat) (nextState pat)) (0, False) text
  where
    patFind' final nextFun (state, res) input
      | res = (state, res)
      | nextFun state input == final =
        (0, True)
      | nextFun state input > 0 && nextFun state input < final =
        (nextFun state input, False)
      | otherwise  =
        (0, False)
    nextState pat state input = 
      Map.findWithDefault (finalZust pat + 1) (input, state) (derAutomat pat)


-- | Ersetzt jedes Vorkommen eines Musters in einem Text
-- durch die übergebene Zeichenkette
patReplace :: 
  (Ord a) 
  => [a] 
  -- ^ Das Muster
  -> [a] 
  -- ^ Der Ersatztext
  -> [a] 
  -- ^ Der Originaltext
  -> [a]
patReplace pat by orig = 
  toResult $ foldl (replace (finalZust pat) (nextState pat) by) 
                   ([], 0, []) 
                   orig
  where
    replace final nextFun by (read, state, res) input
      | nextFun state input == final =
        ([], 0, res ++ by)
      | nextFun state input > state && nextFun state input < final =
        (read ++ [input], nextFun state input, res)
      | nextFun state input == 0 || nextFun state input > final  =
        ([], 0, res ++ read ++ [input])
      | nextFun state input <= state =
        let back = (state - nextFun state input + 1) in
        (drop back read ++ [input], nextFun state input, res ++ (take back read))
      
    nextState pat state input = 
      Map.findWithDefault (finalZust pat + 1) (input, state) (derAutomat pat)
    toResult (inp, _, res) = res ++ inp

-- | Findet ein Muster in einer Datei
findInFile :: 
  String
  -- ^ Dateiname
  -> String
  -- ^ Muster
  -> IO Bool
findInFile datei muster = do
  inhalt <- readFile datei
  return $ patMatchIrgendwo muster inhalt

-- | Liefert den Inhalt einer Datei unter ersetzung eines Musters
replaceInFile ::
  String
  -- ^ Dateiname
  -> String
  -- ^ Muster
  -> String
  -- ^ Ersatz
  -> IO String
replaceInFile datei muster ersatz = do
  inhalt <- readFile datei
  return $ patReplace muster ersatz inhalt

