module HaskellFuerDenRestVonUns.Kapitel6.Aufgabe4 
  ( speichereBits
  , zuBaum
  , decode
  , decodeFile
  ) where

import HaskellFuerDenRestVonUns.Kapitel6.Aufgabe2

-- | Berechnet den Huffmanbaum einer Zeichenkette
zuBaum :: [Char] -> Baum
zuBaum charList = dieserBaum
  where
    wieOft = fquickSort snd (freqMap charList)
    kleineB = map simpleBaum wieOft
    dieserBaum = head (mergeBaumList kleineB)

-- | Speichert den Huffmanbaum zur Zeichenkette
-- in der Datei bBaum.txt
speichereBits :: String -> IO ()
speichereBits text = writeFile "bBaum.txt" (show $ zuBaum text)

-- | Decodiert einen Text anhand eines Huffmanbaums
decode :: Baum -> String -> String
decode baum text = decode' baum baum text
  where
    decode' root (Baum wurzel links rechts) (c:str)
         | isZNode wurzel = (theChar wurzel) : decode' root root (c:str)
         | c == '0' = decode' root links str
         | c == '1' = decode' root rechts str
    decode' root (Baum wurzel links rechts) []
         | isZNode wurzel = [(theChar wurzel)]

-- | Gibt den mit der Datei bBaum.txt entschlüsselten
-- Text der Datei dieBits.txt wieder.
decodeFile :: IO String
decodeFile = do
  schlüssel <- readFile "bBaum.txt"
  bits <- readFile "dieBits.txt"
  return $ decode (read schlüssel) bits
