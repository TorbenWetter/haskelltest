{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel6.Test.Aufgabe4 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Control.Applicative

import qualified HaskellFuerDenRestVonUns.Kapitel6.Aufgabe2 as A2
import qualified HaskellFuerDenRestVonUns.Kapitel6.Aufgabe4 as A4

-- | Testet, ob encodieren invers zu decodieren ist
prop_invers :: String -> Bool
prop_invers "" = True
prop_invers str = 
  A4.decode (A4.zuBaum str) (A2.huffman str) == str
  && A2.huffman (A4.decode (A4.zuBaum str) (A2.huffman str)) == A2.huffman str

runTests = $quickCheckAll