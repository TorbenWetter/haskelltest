{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel6.Test.Aufgabe3 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Control.Applicative

import qualified HaskellFuerDenRestVonUns.Kapitel6.Aufgabe1 as A1
import qualified HaskellFuerDenRestVonUns.Kapitel6.Aufgabe3 as A3
import HaskellFuerDenRestVonUns.Kapitel6.Test.Aufgabe1 (TextUndWort (..))

-- | Testet 'A1.patFind' gegen 'A3.patFind''
prop_patFind :: TextUndWort -> Bool
prop_patFind (TextUndWort t w) =
  A1.patFind w 0 t == A3.patFind' w 0 t && A1.patFind w 0 w == A3.patFind' w 0 w

runTests = $quickCheckAll