{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel6.Test.Aufgabe7 
  ( runTests
  , Var (..)
  , Term (..)
  , Grammar (..)
  , Deriv (..)
  , toParserForm
  , toString
  , ) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative
import Data.List (permutations, nub)
import Data.Tree
import Prelude hiding (foldl)
import Data.Foldable (foldl)
import Control.Arrow (first, second, (***))
import Control.Monad (liftM)

import qualified HaskellFuerDenRestVonUns.Kapitel6.Aufgabe7 as A7

-- | Grammatik Variablen
newtype Var = Var Char deriving (Eq)

-- | Variablen als Buchstaben anzeigen
instance Show Var where
  show (Var c) = [c]

-- | Zufällige Variablen erzeugen (außer 'S')
instance Arbitrary Var where
  arbitrary = elements . map Var $ ['A'..'R'] ++ ['T' .. 'Z']

-- | Grammatik Terminale
newtype Term = Term Char deriving (Eq)

-- | Terminale als Buchstaben anzeigen
instance Show Term where
  show (Term t) = [t]

-- | Zufällige Terminale erzeugen
instance Arbitrary Term where
  arbitrary = elements . map Term $ ['a' .. 'z']

-- | Eine kontextfreie Grammatik
newtype Grammar = Grammar [(Var, [Either Var Term])] deriving (Eq, Show)

-- | Eine Grammatik in die Eingabeform für Parser bringen
toParserForm :: Grammar -> [([Char], String)]
toParserForm (Grammar g) = 
  map (show *** toParserForm') g
  where
    toParserForm' [] = []
    toParserForm' (x:xs) = foldl (\ str x' -> str ++ " " ++ either show show x') (either show show x) xs

-- | S als Startsymbol in eine Grammatik einfügen
addS :: Grammar -> Grammar
addS (Grammar []) = Grammar [(Var 'S', [])]
addS (Grammar (p:ps)) = Grammar ((Var 'S', [Left (fst p)]):p:ps)

-- | Eine zufällige SLR-Grammatik generieren
instance Arbitrary Grammar where
  arbitrary = do
    lhs <- resize 7 $ listOf1 arbitrary
    terms <- resize 30 $ listOf1 arbitrary
    rhs <- vectorOf (length lhs) . resize 4 . listOf1 $ elements (map Left lhs ++ map Right terms)
    let ps = zip lhs rhs
    res <- elements $ map (addS . Grammar . onlyReachable . dropCycle) (permutations ps) 
    if ((> 1) . length . toParserForm) res && (A7.istSLR . toParserForm) res then return res else arbitrary

-- | Verwirft regeln der Form A -> A*
dropCycle :: [(Var, [Either Var Term])] -> [(Var, [Either Var Term])]
dropCycle = foldl (\ ps (v, rhs) -> if (nub rhs == [Left v]) then ps else (ps ++ [(v, rhs)])) []

-- | Gibt die Liste der von einem Symbol aus erreichbaren Variablen
-- mit ihrer Ableitungsdistanz zurück
reachable :: Var -> [(Var, [Either Var Term])] -> [(Var, Int)]
reachable v [] = []
reachable v ps = reachable' [] ps 1 v
  where
    reachable' rbl ps d v' =
      foldl (\ rbl' v'' -> if v'' `elem` map fst rbl' 
                           then rbl' 
                           else reachable' ((v'', d):rbl') ps (d + 1) v'') 
                            rbl 
                            (lookupAll v' ps >>= selectVar)

    lookupAll v ps = ps >>= (\ (v', rhs) -> if v == v' then rhs else [])
    selectVar (Left v) = [v]
    selectVar (Right _) = []

-- | Filtert nicht vom ersten Symbol aus erreichbare Variablen
onlyReachable :: [(Var, [Either Var Term])] -> [(Var, [Either Var Term])]
onlyReachable [] = []
onlyReachable (p:ps) = p : filter ((`elem` (map fst $ reachable (fst p) (p:ps))) . fst) ps

-- | Ableitungsbaum einer Grammatik
data Deriv = Deriv Grammar (Bool, Tree (Either Var Term)) deriving (Eq, Show)


-- | Zufällige Instanzen von Ableitungen
instance Arbitrary Deriv where
  arbitrary = do
    g@(Grammar ps) <- arbitrary
    Deriv g <$> (randomWalk 0 g . Left . fst . head $ ps)
    where
      randomWalk _ g (Right t) = 
        frequency [ (100, return (True, Node (Right t) []))
                  , (1, (\ t' -> (False, Node (Right t') [])) <$> (arbitrary `suchThat` (/= t)))
                  ]
      randomWalk depth g@(Grammar ps) (Left v) = 
        frequency [ (1, (\ ts -> (all fst ts, Node (Left v) (map snd ts))) <$> 
                      frequency (map (second (mapM (randomWalk (depth + 1) g))) $ weightDerivs g v))
                  , (max 0 (depth - 4), (\ f -> (False, Node (Left v) f)) . map (\ t -> Node (Right t) []) <$> 
                            (resize 4 $ listOf1 (arbitrary `suchThat` unused g)))
                  ]
      
      unused (Grammar ps) t = or $ liftM ((Right t `elem`) . snd) ps

-- | Berechnet zu einer rechten Produktionsseite ihre minimale 
-- Distanz in Ableitungsschritten zu einer Variable
withDistanceTo :: Grammar -> Var -> [Either Var Term] -> Maybe Int
withDistanceTo (Grammar ps) v = 
  foldl (minDist ps v) Nothing
  where
    minDist _  _ d        (Right _) = d
    minDist ps v Nothing  (Left v') = lookup v (reachable v' ps)
    minDist ps v (Just d) (Left v') = maybe (Just d) (Just . min d) (lookup v (reachable v' ps))
                      


-- | Berechnet zu einer Variable ihre Ableitungen umgekehrt Gewichtet mit der minimalen
-- Ableitungsdistanz zur Variablen selbst
weightDerivs :: Grammar -> Var -> [(Int, [Either Var Term])]
weightDerivs g@(Grammar ps) v =
  weight . map (\ (_, rhs) -> (withDistanceTo g v rhs, rhs)) $ filter ((== v) . fst) ps
  where
    weight rhs = map (first (maybe (1 + maxWeight rhs) ((+1) . (`div` maxWeight rhs)))) rhs
    maxWeight = foldl (\ w -> maybe w (max w) . fst) 1

-- | Gibt das Wort zu einer Ableitung zurück
toString :: Deriv -> String
toString (Deriv _ (_, t)) = 
  init $ foldl (\ str x -> either (const str) (\ t -> str ++ show t ++ " ") x) "" t

-- | Testet, ob ein Automat erkennt, ob ein Wort zur Sprache gehört oder nicht
prop_erkennt :: Deriv -> Bool
prop_erkennt d@(Deriv g (belongs, _)) =
  case A7.automat (toParserForm g) (toString d) of
    Left _ -> not belongs
    Right _ -> belongs

runTests = $quickCheckAll