{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel6.Test.Aufgabe1 (runTests, TextUndWort (..)) where

import Test.QuickCheck
import Test.QuickCheck.All
import Control.Applicative
import Data.List (intercalate, isInfixOf, isPrefixOf)

import qualified HaskellFuerDenRestVonUns.Kapitel6.Aufgabe1 as A1

-- | Ein Text und das zu suchende / ersetztende Wort
data TextUndWort = TextUndWort String String deriving (Show, Eq)

-- | Zufällige Texte und Wörter
instance Arbitrary TextUndWort where
  arbitrary = oneof [ TextUndWort <$> resize 30 (listOf1 buchstabe) <*> resize 30 (listOf1 buchstabe)
                    , (\ ts w -> TextUndWort (intercalate w ts) w) <$> (listOf . resize 30 . listOf1) buchstabe <*> resize 30 (listOf1 buchstabe)
                    ]
    where
      buchstabe = elements $ ['a'..'z'] ++ ['A' .. 'Z'] ++ [' ', '\t', '\n']

-- | Testet 'A1.patMatchIrgendwo' gegen 'isInfixOf'
prop_findet :: TextUndWort -> Bool
prop_findet (TextUndWort t w) =
  A1.patMatchIrgendwo w t == isInfixOf w t

-- | Testet 'A1.patReplace' gegen eine manuelle split Funktion + intercalate
prop_replace :: TextUndWort -> TextUndWort -> Bool
prop_replace (TextUndWort t w) (TextUndWort _ w') =
  A1.patReplace w w' t == intercalate w' (split w [] [] t)
  where
    split sep done current [] = done ++ [current]
    split sep done current xs
      | isPrefixOf sep xs = split sep (done ++ [current]) [] (drop (length sep) xs)
      | otherwise = split sep done (current ++ [head xs]) (tail xs)

runTests = $quickCheckAll