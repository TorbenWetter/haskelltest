module HaskellFuerDenRestVonUns.Kapitel6.Aufgabe5 
  ( Name (..)
  , readNames
  , Block (..)
  , split
  , readBlocks
  , Bibitem
  , readBibitems
  , BBL (..)
  , readBBL
  , SeitenZahl (..)
  , readSeitenZahl
  , RRRZeile
  , readRRR
  , mitSeitenzahlen
  , AUTZeile (..)
  , zuAut
  , AUT
  , main
  ) where

import System.IO
import Data.List (nub)
import HaskellFuerDenRestVonUns.Kapitel6.Aufgabe2

-- | Ein Autorenname
data Name 
  = Name
  { firstNames :: String
  , lastName :: String
  } deriving (Eq)

instance Ord Name where
  name1 <= name2
    | lastName name1 == lastName name2 = 
      firstNames name1 <= firstNames name2
    | otherwise = lastName name1 <= lastName name2

instance Show Name where
  show (Name first ('~':last)) =
    first ++ ('~':last)
  show (Name first last) =
    first ++ " " ++ last

-- | Liest einen String als Liste von Namen ein
readNames :: String -> [Name]
readNames str = 
  map readName . filter (/= ", ") . split ", " . normalizeAnd $ str
  where
    readName name =
      case dropWhile (/= '~') name of
        [] -> Name (concat . init $ words name) (last $ words name)
        last -> Name (normalizeSpace $ takeWhile (/= '~') name) 
                     (normalizeSpace last)
    normalizeSpace xs = unwords . words $ xs
    normalizeAnd str =
      case split "and " str of
        [str'] -> str'
        xs ->
          case (init . init $ xs) of
            [] -> last xs
            xs' | last (split ", " (last xs')) == ", " -> 
                  concat $ xs' ++ [last xs]
                | otherwise -> 
                  concat $ (init xs') ++ [(last xs' ++ ", "), last xs]
        
           
            

-- | Ein Bibitem Subblock
data Block
  = Block
  { content :: String
  } deriving (Eq, Ord)

-- | Spaltet eine Liste bei allen Vorkommen des
-- Seperators
split :: (Eq a) => [a] -> [a] -> [[a]]
split sep xs = (uncurry (:)) $ foldr (split' sep) ([], []) xs
  where
    split' sep x (buf, res)
      | length (x:buf) >= length sep &&
        (take (length sep) (x:buf)) == sep = 
          if length (x:buf) > length sep
          then ([], (take (length sep) (x:buf)):(drop (length sep) (x:buf)):res)
          else ([], (x:buf):res)
      | otherwise = ((x:buf), res)

-- | Liest eine Liste von Blöcken ein
readBlocks :: String -> [Block]
readBlocks str = readBlocks' $ split "\\newblock " str
  where
    readBlocks' ("\\newblock ":b:bs) = (Block b) : readBlocks' bs
    readBlocks' (_:bs) = readBlocks' bs
    readBlocks' [] = []

instance Show Block where
  show (Block s) = "\\newblock " ++ s

-- Ein Bibitem block
data Bibitem 
  = Bibitem
  { optional :: String
  , ref :: String
  , names :: [Name]
  , blocks :: [Block]
  } deriving (Eq)

instance Ord Bibitem where
  b1 <= b2
    | b1 == b2 = True
    | names b1 <= names b2 = True


instance Show Bibitem where
  show (Bibitem o r ns bs) =
    "\\bibitem" ++ (showOptional o) ++ "{" ++ r ++ "}\n" ++
    (showNames ns) ++ "\n" ++
    (showBlocks bs) ++ "\n"
    where
      showNames [] = ""
      showNames [name] = show name
      showNames names = 
        (foldl (\ res n -> res ++ ", " ++ show n) 
               (show $ head names) 
               (init $ tail names)) ++
        ", and " ++ show (last names)
      showBlocks [] = ""
      showBlocks (b:bs) = 
        foldl (\ res b -> res ++ (show b) ++ "\n") (show b) bs
      showOptional "" = ""
      showOptional o = "[" ++ o ++ "]"

-- | Liest eine Liste von Bibitems ein
readBibitems :: String -> [Bibitem]
readBibitems str = 
  readBibitems' $ split "\\bibitem" str
  where
    readBibitems' ("\\bibitem":(bi:bis)) = 
      (readBibitem bi):(readBibitems' bis)
    readBibitems' (_:bis) = readBibitems' bis
    readBibitems' [] = []
    readBibitem bi = 
      let
        (optPart, rest') = readOptional bi
        rest'' = tail rest'
        (refPart, rest) = (takeWhile (/= '}') rest'', tail $ dropWhile (/= '}') rest'')
        (namePart:blocks) = split "\\newblock" rest
      in
        Bibitem optPart 
                refPart 
                (readNames namePart) 
                (readBlocks $ concat blocks)
    readOptional ('[':bi) = 
      (takeWhile (/= ']') bi, tail $ dropWhile (/= ']') bi)
    readOptional bi = ("", bi)

-- | Daten einer bbl-Datei
data BBL
  = BBL
  { prelude :: String
  , items :: [Bibitem]
  } deriving (Ord, Eq)

instance Show BBL where
  show (BBL pre is) = 
    (foldl (\ res i -> res ++ (show i)) pre is) ++ "\\end{thebibliography}"

-- | Liest eine komplette BBL Datei ein
readBBL :: String -> BBL
readBBL str = 
  BBL (head $ split "\\bibitem" str) 
      (readBibitems . head $ split "\\end{thebibliography}" str)

-- | Seitenzahlen (römisch und arabisch)
data SeitenZahl
  = Römisch { zahl :: String }  
  | Arabisch { zahl :: String }
  deriving (Eq)

instance Show SeitenZahl where
  show sz = zahl sz

-- | Liest eine Seitenzahl ein
readSeitenZahl sz
  | all (`elem` "ivxlcdm") sz = Römisch sz
  | all (`elem` ['0'..'9']) sz  = Arabisch sz

instance Ord SeitenZahl where
  x <= y = convert x <= convert y
    where
      convert (Arabisch z) = read z
      convert (Römisch ('i':'v':xs)) = 4 + convert (Römisch xs)
      convert (Römisch ('i':'x':xs)) = 9 + convert (Römisch xs)
      convert (Römisch ('x':'l':xs)) = 40 + convert (Römisch xs)
      convert (Römisch ('x':'c':xs)) = 90 + convert (Römisch xs)
      convert (Römisch ('c':'d':xs)) = 400 + convert (Römisch xs)
      convert (Römisch ('c':'m':xs)) = 900 + convert (Römisch xs)
      convert (Römisch ('i':xs)) = 1 + convert (Römisch xs)
      convert (Römisch ('v':xs)) = 5 + convert (Römisch xs)
      convert (Römisch ('x':xs)) = 10 + convert (Römisch xs)
      convert (Römisch ('l':xs)) = 50 + convert (Römisch xs)
      convert (Römisch ('c':xs)) = 100 + convert (Römisch xs)
      convert (Römisch ('d':xs)) = 500 + convert (Römisch xs)
      convert (Römisch ('m':xs)) = 1000 + convert (Römisch xs)
      convert (Römisch "") = 0

-- | Zeile in einer .rrr-Datei
data RRRZeile
  = RRRZeile
  { seite :: SeitenZahl
  , refs :: [String]
  } deriving (Ord, Eq)

instance Show RRRZeile where
  show (RRRZeile s rs) = 
    foldl (\ res r -> res ++ "," ++ r) (show s) rs

-- | Liest RRRZeilen aus einem String ein
readRRR :: String -> [RRRZeile]
readRRR str = 
  map readRRRZeile . filter (/= "\n") $ split "\n" str
  where
    readRRRZeile str = 
      (\ (nr:rest) -> RRRZeile (readSeitenZahl nr) rest) . 
      filter (/= ",") $ split "," str


-- | Erweitert Bibitems um Seitenzahlen aus
-- einer rrr-Datei
mitSeitenzahlen :: BBL -> [RRRZeile] -> BBL
mitSeitenzahlen (BBL pre bis) rrrs = 
  BBL pre $ map (mitSeitenzahlen' rrrs) bis
  where
    mitSeitenzahlen' rrrs bi = 
      case seitenZahlenString (seitenZahlenFuer (ref bi) rrrs) of
        "" -> bi
        str -> bi { blocks = (blocks bi) ++ [Block str] }
    seitenZahlenFuer ref rrrs = 
          fquickSort id .
          nub . 
          map (seite) . 
          filter ((ref `elem`) . refs) $ rrrs
    seitenZahlenString [] = ""
    seitenZahlenString [sz] = "Zitiert auf der Seite " ++ (show sz) ++ "."
    seitenZahlenString (sz:szs) = 
      (foldl (\ res nr -> res ++ ", " ++ show nr)
             ("Zitiert auf den Seiten " ++ show sz) 
             szs) ++ "."

-- | Zeile für eine aut-Datei
data AUTZeile
  = AUTZeile
  { name :: String
  , zitiertAuf :: [SeitenZahl]
  } deriving (Ord, Eq)

instance Show AUTZeile where
  show (AUTZeile n zA) = 
    "\\derVerfasser{" ++ n ++ "}{" ++ showZA zA ++ "}"
    where
      showZA [] = ""
      showZA (zA:zAs) = 
        foldl (\ res z -> res ++ ", " ++  (show z)) (show $ zA) (zAs)

data AUT 
  = AUT [AUTZeile]
  deriving (Ord, Eq)

instance Show AUT where
  show (AUT auts) = 
    concat $ map ((++ "\n") . show) auts
    

-- | Erzeugt aut-Zeilen aus rrr-Zeilen und einer bbl-Datei
zuAut :: [RRRZeile] -> BBL -> AUT
zuAut rrrs (BBL pre bibs) =
  AUT $ foldr (\ (RRRZeile seite rs) auts -> 
                 foldl (addZitat seite) auts $ zuNamen rs bibs) 
              (alleAuts bibs) 
              rrrs
  where
    alleAuts bibs = 
      nub . concat $ 
          map (map (\ n -> AUTZeile (showAUTName n) []) . names) bibs
    showAUTName (Name f l) = l ++ ", " ++ f
    zuNamen refs bibs =
      concat $ map names $ filter ((`elem` refs) . ref) bibs
    addZitat seite auts name = 
      map (\ (AUTZeile name' seiten) ->
             if (showAUTName name) == name' 
             then (AUTZeile name' (fquickSort id . nub $ seite:seiten))
             else (AUTZeile name' seiten)
          ) auts

-- | Erzeugt die aut-Datei und erneuert die bbl-Datei
-- zu DasBuch.rrr
main = do
  rrrContent <- readFile "DasBuch.rrr"
  bblHandle <- openFile "DasBuch.bbl" ReadMode
  bblContent <- hGetContents bblHandle
  let
    bbl = readBBL bblContent
    rrr = readRRR rrrContent
    bbl' = mitSeitenzahlen bbl rrr
    aut = zuAut rrr bbl
  writeFile "DasBuch.aut" (show aut)
  hClose bblHandle
  writeFile "DasBuch.bbl" (show bbl)