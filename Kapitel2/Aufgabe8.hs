module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe8 ( ersetze ) where


-- | Ersetzt jedes Vorkommen von 'a' durch 'Y'.
ersetze :: String -> String
ersetze xs = map aZuY xs
  where
    aZuY 'a' = 'Y'
    aZuY x = x
