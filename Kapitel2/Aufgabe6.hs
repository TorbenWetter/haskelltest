module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe6 ( match ) where

-- | Bildet ein Präfix und eine Liste auf das Präfix und die Liste ohne Präfix ab.
match :: (Eq a) => [a] -> [a] -> ([a], [a])
match ps ss
  | ps == take (length ps) ss = (ps, drop (length ps) ss)
  | otherwise = ([], ss)
