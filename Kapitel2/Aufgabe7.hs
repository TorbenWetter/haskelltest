module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe7 ( span, break ) where

import Prelude hiding (span,  break)

-- | Zerlegt eine Liste in das Längste Präfix, für das die übergebene
-- Bedingungsfunktion wahr ist und den Rest der Liste.
span :: (a -> Bool)
     -- ^ Bedingungsfunktion
     -> [a]
     -- ^ Zu zerlegende Liste
     -> ([a], [a])
span f [] = ([], [])
span f (x:xs)
  | f x = (\ (as, bs) -> (x:as, bs)) $ span f xs
  | otherwise = ([], (x:xs))

-- | Verhält sich wie 'span', nur mit logisch invertierter Bedingungsfunktion.
break :: (a -> Bool)
      -- ^ Bedingungsfunktion
      -> [a]
      -- ^ Zu zerlegende Liste
      -> ([a], [a])
break f = span (not . f)