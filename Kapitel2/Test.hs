module Main where

import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe1 as K2A1
import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe2 as K2A2
import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe3 as K2A3
import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe4 as K2A4
import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe5 as K2A5
import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe6 as K2A6
import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe7 as K2A7
import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe8 as K2A8
import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe9 as K2A9
import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe10 as K2A10
import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe11 as K2A11
import qualified HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe12 as K2A12

main = do
  K2A1.runTests
  K2A2.runTests
  K2A3.runTests
  K2A4.runTests
  K2A5.runTests
  K2A6.runTests
  K2A7.runTests
  K2A8.runTests
  K2A9.runTests
  K2A10.runTests
  K2A11.runTests
  K2A12.runTests