module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe11 ( perfekteZahlen ) where

-- | Berechnet die Liste aller Teiler einer Zahl
teiler :: Int -> [Int]
teiler n = [t | t <- [1 .. n `div` 2], n `mod` t == 0]

-- | Berechnet die Liste aller perfekten Zahlen in einem gegebenen Intervall
perfekteZahlen :: Int -> Int -> [Int]
perfekteZahlen start stop
  | start >= stop = []
  | otherwise = [p | p <- [start .. stop], p == sum (teiler p)]
