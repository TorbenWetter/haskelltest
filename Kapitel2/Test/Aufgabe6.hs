{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe6 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified Data.List as List
import Data.Char

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe6 as A6

-- | Testet, ob die Invariante der Aufgabenstellung auf Zufallslisten für 'match' erfüllt ist.
prop_match :: (Eq a) => [a] -> [a] -> Bool
prop_match ps ss
  | take (length ps) ss == ps = (ps, drop (length ps) ss) == A6.match ps ss
  | otherwise = ([], ss) == A6.match ps ss

runTests = $quickCheckAll