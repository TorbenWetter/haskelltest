{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe3 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import Data.List

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe3 as A3

-- | Testet die Funktion 'takeWhileRek' gegen die Haskell Referenzimplementierung 'takeWhile'.
-- Verwendet die zugehörigkeit zu einer zufälligen Liste als Prädikat.
prop_takeWhileRek :: (Eq a) => [a] -> [a] -> Bool
prop_takeWhileRek xs ys = (A3.takeWhileRek (`elem` ys) xs) == (takeWhile (`elem` ys) xs)

-- | Testet die Funktion 'takeWhileFoldr' gegen die Haskell Referenzimplementierung 'takeWhile'.
-- Verwendet die zugehörigkeit zu einer zufälligen Liste als Prädikat.
prop_takeWhileFoldr :: (Eq a) => [a] -> [a] -> Bool
prop_takeWhileFoldr xs ys = (A3.takeWhileFoldr (`elem` ys) xs) == (takeWhile (`elem` ys) xs)


runTests = $quickCheckAll

