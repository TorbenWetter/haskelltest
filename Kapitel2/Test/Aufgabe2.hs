{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe2 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import Data.List

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe2 as A2

-- | Testet die Funktion 'ohneDuplikate' gegen die Haskell Referenzimplementierung 'nub'
prop_ohneDuplikate :: (Eq a) => [a] -> Bool
prop_ohneDuplikate xs = (A2.ohneDuplikate xs) == (nub xs)

runTests = $quickCheckAll

