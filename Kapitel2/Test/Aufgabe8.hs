{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe8 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe8 as A8

-- | Testet, ob alle 'a' in einer zufälligen Zeichenkette richtig durch Y ersetzt werden.
prop_ersetze :: String -> Bool
prop_ersetze xs = 
  foldl (\ res (x, y) -> res && not (x == 'a' && y /= 'Y')) True $ zip xs (A8.ersetze xs)

runTests = $quickCheckAll