{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe10 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe10 as A10

-- | Vergleicht das Ergebnis der listenbasierten Lösung mit der geschlossenen.
prop_kleinGross :: Int -> Int -> Bool
prop_kleinGross k j = 
  let 
    anzahl = (k `mod` 1000) + 1
    bisher = (j `mod` anzahl) + 1
    anzahl' = (realToFrac $ anzahl) :: Double
    posNeu = round $ 2.0 * (anzahl' - 2.0^(floor $ logBase 2 anzahl'))
  in
    A10.kleinGross anzahl bisher == (map (`mod` anzahl) [bisher .. bisher + (anzahl - 1)] !! posNeu)

runTests = $quickCheckAll