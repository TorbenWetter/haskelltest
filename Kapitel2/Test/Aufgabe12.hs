{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe12 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import  Data.List (sort)

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe12 as A12

data Stein = R | W | B deriving (Eq, Show)

-- | Der Buchstabe zu einem Stein
toChar :: Stein -> Char
toChar R = 'r'
toChar W = 'w'
toChar B = 'b'

instance Arbitrary Stein where
  arbitrary = elements [R, W, B]

instance Ord Stein where
  R <= W = True
  R <= B = True
  W <= B = True
  x <= y = x == y

-- | Prüft, ob die zufällig generierte Steinchen passend sortiert werden
prop_perfekteZahlen :: [Stein] -> Bool
prop_perfekteZahlen steinchen =
  (A12.flagge $ map toChar steinchen) == (map toChar $ sort steinchen)

runTests = $quickCheckAll