{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe4 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified Data.List as List
import Data.Char

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe4 as A4

-- | Testet die Funktion 'words' aus Aufgabe 4 gegen die Haskell Referenzimplementierung 'words'.
-- Beschränkt sich auf standard Whitespacezeichen und lässt Unicode außen vor.
prop_words :: String -> Bool
prop_words xs = 
  let
    xs' = filter (\ c -> (not $ isSpace c) || (c `elem` [' ', '\t', '\n', '\r'])) xs
  in
    (A4.words xs') == (words xs')

-- | Testet die Funktion 'unwords' aus Aufgabe 4 gegen die Haskell Referenzimplementierung 'unwords'.
prop_unwords :: [String] -> Bool
prop_unwords xss = (A4.unwords xss) == (List.unwords xss)


runTests = $quickCheckAll