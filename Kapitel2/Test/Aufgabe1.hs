{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe1 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import Data.List

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe1 as A1


-- | Vergleicht das Ergebnis der permutations Funktion mit der Haskell-Referenzimplementierung.
-- Verwendert nur n = 0..10, um den Arbeitsspeicherverbrauch zu begrenzen.
prop_permutations :: Bool
prop_permutations = 
  foldl (\ res n -> res && (seqEq (A1.permutations n) (permutations [1..n]))) True [0..6]
  where
    seqEq xs ys = null $ (xs \\ ys) ++ (ys \\ xs)

runTests = $quickCheckAll