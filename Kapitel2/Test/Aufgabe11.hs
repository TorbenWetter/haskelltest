{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe11 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe11 as A11

-- | Prüft, ob die ersten 4 perfekten Zahlen korrekt berechnet werden.
prop_perfekteZahlen :: Bool
prop_perfekteZahlen = A11.perfekteZahlen 1 10000 == [6, 28, 496, 8128]

runTests = $quickCheckAll