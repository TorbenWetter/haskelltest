{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe9 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe9 as A9

-- | Testet, ob alle 'g' in einer zufälligen Zeichenkette richtig durch "Fontane hatte es nicht leicht" ersetzt werden.
prop_ersetze :: String -> Bool
prop_ersetze xs = 
  fst $ foldl (gReplacedBy "Fontane hatte es nicht leicht") (True, A9.ersetze xs) xs
  where
    gReplacedBy by (res, repl) x
               | x == 'g' =
                 (res && (take (length by) repl) == by, drop (length by) repl)
               | otherwise = 
                 (res, tail repl)

runTests = $quickCheckAll