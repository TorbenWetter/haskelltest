{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe7 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe7 as A7

-- | Testet die Methode 'span' gegen ihre Referenzimplementierung aus Prelude.
-- Verwendet die zugehörigkeit zu einer zufälligen Liste als Prädikat.
prop_span :: (Eq a) => [a] -> [a] -> Bool
prop_span ps xs = (span (`elem` ps) xs) == (A7.span (`elem` ps) xs)

-- | Testet die Methode 'break' gegen ihre Referenzimplementierung aus Prelude.
-- Verwendet die zugehörigkeit zu einer zufälligen Liste als Prädikat.
prop_break :: (Eq a) => [a] -> [a] -> Bool
prop_break ps xs = (break (`elem` ps) xs) == (A7.break (`elem` ps) xs)


runTests = $quickCheckAll