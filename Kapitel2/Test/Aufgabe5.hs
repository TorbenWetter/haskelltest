{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel2.Test.Aufgabe5 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified Data.List as List
import Data.Char

import qualified HaskellFuerDenRestVonUns.Kapitel2.Aufgabe5 as A5

-- | Testet, ob 'umbruch' alle Wörter in ihrer Reihenfolge ungetrennt intakt lässt.
prop_wortGrenzen :: Int -> String -> Bool 
prop_wortGrenzen k xs = words xs == words (A5.umbruch k xs)

-- | Testet, ob die maximale Zeilenlänge von 'umbruch' nicht überschritten wird.
prop_zeilenLänge k xs = foldl (\ res line -> res && lineOk k line) True . lines $ A5.umbruch k xs
  where
    lineOk k line = (length line <= k) || (length (words line) == 1)

runTests = $quickCheckAll