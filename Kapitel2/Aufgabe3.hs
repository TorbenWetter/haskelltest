module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe3 ( takeWhileRek, takeWhileFoldr ) where

-- | Extrahiert das längste Prefix aus einer Liste, für das die übergebene Bedingungsfunktion True zurück gibt.
takeWhileRek :: (a -> Bool) 
             -- ^ Bedingungsfunktion
             -> [a]
             -- ^ Liste, deren Präfix berechnet werden soll
             -> [a]
takeWhileRek f [] = []
takeWhileRek f (x:xs)
               | f x = x : (takeWhileRek f xs)
               | otherwise = []

takeWhileFoldr :: (a -> Bool) -> [a] -> [a]
takeWhileFoldr f xs = foldr (hinzu f) [] xs
  where
    hinzu f y ys
            | f y = y:ys
            | otherwise = []