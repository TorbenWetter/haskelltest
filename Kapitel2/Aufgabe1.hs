module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe1 ( permutations ) where

-- | Berechnet alle Permutationen der Zahlen 1 bis n
permutations :: Int -> [[Int]]
permutations 0 = [[]]
permutations 1 = [[1]]
permutations n = concatMap (insertEverywhere n) (permutations (n - 1))
  where
    insertEverywhere n xs = map (\ k -> (take k xs) ++ [n] ++ (drop k xs)) [0..length xs]

