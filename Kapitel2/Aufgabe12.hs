module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe12 ( flagge ) where

-- | Vertauscht zwei Werte in einer Liste.
tausche :: Int
        -- ^ Index des ersten Austauschelements
        -> Int
        -- ^ Index des zweiten Austauschelements
        -> [a]
        -- ^ Die Liste
        -> [a]
tausche i j xs
  | i == j = xs
  | otherwise = (take i xs) ++ ((xs !! j) : (take (j - i - 1) (drop (i + 1) xs))) ++ ((xs !! i) : (drop (j + 1) xs))


-- | Ordnet die übergebene Liste aus Steinchen entsprechend der niederländischen Nationalflagge ('r', 'w', 'b')
-- an.
flagge :: [Char] -> [Char]
flagge steinchen = flagge' steinchen 0 0 (length steinchen - 1)
  where
    flagge' xs lo mid hi
      | mid > hi = xs
      | xs !! mid == 'r' = flagge' (tausche lo mid xs) (lo + 1) (mid + 1) hi
      | xs !! mid == 'w' = flagge' xs lo (mid + 1) hi
      | xs !! mid == 'b' = flagge' (tausche mid hi xs) lo mid (hi - 1)
