module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe5 ( umbruch ) where

-- | Trennt eine Zeichenkette unter Beachtung von Wortgrenzen mit Zeilenumbrüchen,
-- sodass die entstehenden Zeilen nicht länger als die maximale vorgegebene Länge sind.
umbruch :: Int
        -- ^ Die maximale Zeilenlänge
        -> String
        -- ^ Die zu trennende Zeichenkette
        -> String
umbruch k xs = 
  case words xs of
    [] -> ""
    ws -> snd . foldl1 (hängeAn k) . map mitLänge $ ws
  where
    mitLänge w = (length w, w)
    hängeAn k (aktuelleLänge, text) (wortLänge, wort)
             | aktuelleLänge + wortLänge < k = 
               (aktuelleLänge + 1 + wortLänge, text ++ (' ' : wort))
             | otherwise = 
               (wortLänge, text ++ ('\n' : wort))
