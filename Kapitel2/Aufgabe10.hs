module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe10 ( kleinGross ) where

-- | Übernimmt alle Elemente an geraden Indices.
jederZweite :: [a] -> [a]
jederZweite [] = []
jederZweite (x:[]) = [x]
jederZweite (x:y:xs) = x : jederZweite xs

-- | Setzt den Wahlmodus KleinGross um.
kleinGross ::Int
           -- ^ Die Gruppengröße
           -> Int
           -- ^ Die bisherige Vorsitzende
           -> Int
kleinGross k bisher = head . wähle $ stehendeGruppe k bisher
  where
    stehendeGruppe k bisher = map (`mod` k) [bisher .. bisher + (k - 1)]
    wähle [letzter] = [letzter]
    wähle gruppe
      | (length gruppe `mod` 2) == 0 = wähle $ jederZweite gruppe
      | otherwise = wähle . tail $ jederZweite gruppe
    
