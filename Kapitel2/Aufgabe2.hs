module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe2 ( ohneDuplikate ) where

-- | Filtert alle duplikate aus einer Liste
ohneDuplikate :: (Eq a) => [a] -> [a]
ohneDuplikate [] = []
ohneDuplikate (x:xs) = x:(filter (/= x) (ohneDuplikate xs))
