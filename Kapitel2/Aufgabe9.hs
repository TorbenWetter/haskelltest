module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe9 ( ersetze ) where


-- | Ersetzt jedes Vorkommen von 'g' durch "Fontane hatte es nicht leicht".
ersetze :: String -> String
ersetze xs = concat $ map gZuFontane xs
  where
    gZuFontane 'g' = "Fontane hatte es nicht leicht"
    gZuFontane x = [x]
