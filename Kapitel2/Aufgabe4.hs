module HaskellFuerDenRestVonUns.Kapitel2.Aufgabe4 ( words, unwords ) where

import Prelude hiding (words, unwords)

-- | Zerlegt eine Zeichenkette an allen Stellen, an denen Leerzeichen, 
-- Tabulatoren oder Zeilenvorschübe auftauchen.
words :: String -> [String]
words xs = snd $ foldr nehmenOderVerwerfen (False, []) xs
  where
    leer c = c `elem` [' ', '\t', '\n', '\r']
    nehmenOderVerwerfen y (genommen, yss)
                          | leer y = (False, yss)
                          | genommen = (True, (y : head yss):(tail yss))
                          | otherwise = (True, [y]:yss)

-- | Konstruiert aus einer Liste von Zeichenketten eine einzige durch Konkatenation.
unwords :: [String] -> String
unwords [] = ""
unwords (xs:xss) = foldl (\ yss ys -> yss ++ (' ' : ys)) xs xss