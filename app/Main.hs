module Main where

import Lib

main :: IO ()

-- Aufgabe 1.1
curry f x y = f (x, y)
uncurry f (x, y) = f x y

-- Aufgabe 1.2.1
infix 6 #
(#) x y
  | y == 0 = x
  | True = (#) (succ x) (pred y)

infix 6 %
(%) x y
  | y == 0 = x
  | x == 0 = 0
  | True = (%) (pred x) (pred y)

-- Aufgabe 1.2.2
mult x y result
  | x == 0 = result
  | True = mult (pred x) y (result # y)
infix 7 ##
(##) x y = mult x y 0

divi x y result
  | x == 0 = result
  | True = divi (x % y) y (succ result)
infix 7 %%
(%%) x y = divi x y 0

-- Ausgabe zum Testen
main = putStrLn (show (18 %% 6))
