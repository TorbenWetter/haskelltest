{-# LANGUAGE TemplateHaskell #-}
module Kapitel1.Test.Aufgabe2 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All

import Kapitel1.Aufgabe2

-- | Testet, ob # das selbe Ergebnis liefert wie +
prop_Plus :: Int -> Int -> Bool
prop_Plus x y = 
  let
    x' = (abs x) `mod` 10000 -- Verhindert Rekursionsüberlauf bei zu großen Zahlen
    y' = (abs y) `mod` 10000 
  in 
    x' + y' == x' # y'

-- | Testet, ob % das selbe Ergebnis liefert wie -
prop_Minus :: Int -> Int -> Bool
prop_Minus x y = 
  let
    x' = (abs x) `mod` 10000 -- Verhindert Rekursionsüberlauf bei zu großen Zahlen
    y' = (abs y) `mod` 10000 
  in
    max 0 (x' - y') == x' % y'

-- | Testet, ob ## das selbe Ergebnis liefert wie *
prop_Mult :: Int -> Int -> Bool
prop_Mult x y =
  let
    x' = (abs x) `mod` 100
    y' = (abs y) `mod` 100
  in
    x' * y' == x' ## y'

-- | Testet, ob %% das selbe Ergebnis liefert wie div
prop_Div :: Int -> Int -> Bool
prop_Div x y =
  let
    x' = (abs x) `mod` 100
    y' = ((abs y) `mod` 100) + 1
  in
    x' `div` y' == x' %% y'

-- | Testet, ob alle Operationen linksassoziativ sind
prop_Assoc :: Int -> Int -> Int -> Bool
prop_Assoc x y z = 
  let
    x' = (abs x) `mod` 10
    y' = (abs y) `mod` 100
    z' = (abs z) `mod` 100
  in
    (x' # y') # z' == x' # y' # z'
    && (x' % y') % z' == x' % y' % z'
    && (x' ## y') ## z' == x' ## y' ## z'
    && (x' %% (y' + 1)) %% (z' + 1) == x' %% (y' + 1) %% (z' + 1)

-- | Testet, ob ## und %% höhere Priorität als # und % haben
prop_Prio :: Int -> Int -> Int -> Bool
prop_Prio x y z =
  let
    x' = (abs x) `mod` 10
    y' = (abs y) `mod` 100
    z' = (abs z) `mod` 100
  in
    (x' ## y') # z' == x' ## y' # z'
    && (x' ## y') % z' == x' ## y' % z'
    && (x' %% (y' + 1)) # z' == x' %% (y' + 1) # z'
    && (x' %% (y' + 1)) % z' == x' %% (y' + 1) % z'

runTests = $quickCheckAll