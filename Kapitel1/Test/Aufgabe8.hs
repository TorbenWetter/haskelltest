{-# LANGUAGE TemplateHaskell #-}
module Kapitel1.Test.Aufgabe8 ( runTests ) where

import Test.QuickCheck
import Test.QuickCheck.All

import Kapitel1.Aufgabe8

-- | Testet die Geradengleichung auf Linearität.
prop_Linear :: (Double, Double) -> (Double, Double) -> Double -> Double -> Bool
prop_Linear (x1, y1) (x2, y2) x c =
  let
    (x2', y2') = if x1 == x2 && y1 == y2 then (x1 + 1, y1 + 1) else (x2, y2)
    b = y1 - (y2' - y1) * x1 / (x2' - x1)
  in 
    0.0001 > (abs $ (c * gerade (x1, y1) (x2', y2') x - (c - 1) * b) -  gerade (x1, y1) (x2', y2') (c * x))

runTests = $quickCheckAll