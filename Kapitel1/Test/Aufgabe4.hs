{-# LANGUAGE TemplateHaskell #-}

module Kapitel1.Test.Aufgabe4 
  ( runTests
  , Wochentag
  ) where

import Test.QuickCheck
import Test.QuickCheck.All

import Kapitel1.Aufgabe4

data Wochentag = 
  Montag | Dienstag | Mittwoch | Donnerstag | Freitag | Samstag | Sonntag | Keiner
  deriving (Show)

instance Arbitrary Wochentag where
  arbitrary = elements [Montag, Dienstag, Mittwoch, Donnerstag, Freitag, Samstag, Sonntag, Keiner]

-- | Testet, ob das Loeschen eines neu erstellten Datums zu einem ungültigen Datum führt.
prop_datumLoeschen :: Wochentag -> Int -> Int -> Int -> Bool
prop_datumLoeschen wochenTag tag monat jahr = 
  let 
    datum = erstelleDatum (show wochenTag) tag monat jahr 
  in
    (not $ testeDatum datum) || (not . testeDatum $ loescheDatum datum)

-- | Testet, ob das Loeschen einer neu erstellten Uhrzeit zu einer ungültigen Uhrzeit führt.
prop_uhrzeitLoeschen :: Int -> Int -> Bool
prop_uhrzeitLoeschen stunde minute =
  let
    uhrzeit = erstelleUhrzeit stunde minute
  in
    (not $ testeUhrzeit uhrzeit) || (not . testeUhrzeit $ loescheUhrzeit uhrzeit)

-- | Testet, ob das Loeschen einer Dauer zu einer ungültigen Dauer führt.
prop_dauerLoeschen :: Int -> Bool
prop_dauerLoeschen dauer =
  (not $ testeDauer dauer) || (not . testeDauer $ loescheDauer dauer)

-- | Testet, ob das Loeschen einer Beschreibung zu einer ungültigen Beschreibung führt.
prop_beschreibungLoeschen :: String -> Bool
prop_beschreibungLoeschen beschreibung =
  (not $ testeBeschreibung beschreibung) || (not . testeBeschreibung $ loescheBeschreibung beschreibung)

-- | Testet, ob das Löschen eines neu erstellten Termins zu einem ungültigen Termin führt.
prop_terminLoeschen :: Wochentag -> Int -> Int -> Int -> Int -> Int -> Int -> String -> Bool
prop_terminLoeschen wochenTag tag monat jahr stunde minute dauer beschreibung =
  let
    termin = vereinbareTermin (show wochenTag) tag monat jahr stunde minute dauer beschreibung
  in
    (not $ testeTermin termin) || (not . testeTermin $ loescheTermin termin)

runTests = $(quickCheckAll)