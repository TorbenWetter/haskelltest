{-# LANGUAGE TemplateHaskell #-}

module Kapitel1.Test.Aufgabe1 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified Kapitel1.Aufgabe1 as A1

-- | Testet, ob curryfizierte Funktionen nicht curryfizierten gleichen
prop_curryEq :: 
  (Eq c, Arbitrary a, Arbitrary b, Arbitrary c) 
  => Fun (a, b) c -> (a, b) -> Bool
prop_curryEq (Fun _ f) (x, y) = f (x, y) == (A1.curry f) x y


-- | Testet, ob das Entcurryfizieren invers zum Curryfizieren ist
prop_uncurryEq ::
  (Eq c, Arbitrary a, Arbitrary b, Arbitrary c)
  => Fun (a, b) c -> (a, b) -> Bool
prop_uncurryEq (Fun _ f) (x, y) = (A1.uncurry (A1.curry f)) (x, y) == f (x, y)

runTests = $quickCheckAll