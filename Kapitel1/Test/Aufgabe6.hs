{-# LANGUAGE TemplateHaskell #-}

module Kapitel1.Test.Aufgabe6 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All

import Control.Applicative

import Kapitel1.Aufgabe6

-- | Alle Kennzeichen in richtiger Reihenfolge und endloses Suffix aus ZZZ 999
kennzeichen = 
  ((\ a b c x y z -> (a, b, c, x, y, z)) <$> chars <*> chars <*> chars <*> ints <*> ints <*> ints) ++
  (cycle [('Z', 'Z', 'Z', 9, 9, 9)])
  where
    chars = ['A' .. 'Z']
    ints = [0 .. 9]


-- | Testet, ob folgendesKennzeichen das richtige Kennzeichen zurückgibt.
prop_Follow :: Int -> Bool
prop_Follow kz = 
  let kz' = kz `mod` ((26^3) * 1000) in
  folgendesKennzeichen (kennzeichen !! kz') == kennzeichen !! (kz' + 1)

runTests = $quickCheckAll