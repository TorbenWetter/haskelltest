{-# LANGUAGE TemplateHaskell #-}
module Kapitel1.Test.Aufgabe9 ( runTests ) where

import Test.QuickCheck
import Test.QuickCheck.All

import Data.List

import Kapitel1.Aufgabe9

(a, b) <-> (c, d) = (a - c, b - d)
(a, b) <**> (c, d) = a * c + b * d
rad (a, b) = sqrt $ a * a + b * b
ang x y = abs . acos $ (x <**> y) / ((rad x) * (rad y))

-- | Testet die Methode kollinear durch vergleich mit dem Skalarprodukt.
prop_Kollinear :: (Double, Double) -> (Double, Double) -> (Double, Double) -> Bool
prop_Kollinear p1 p2 p3
  | rad (p1 <-> p2) > 0 && rad (p1 <-> p3) > 0 = 
    (abs (ang (p1 <-> p2) (p1 <-> p3)) < 0.0001) == kollinear p1 p2 p3
  | otherwise = kollinear p1 p2 p3

type Dreieck = ((Double, Double), (Double, Double), (Double, Double))

angs (a, b, c)
  | kollinear a b c = permutations [0, 0, 0]
  | otherwise = map angs' $ permutations [a, b, c]
  where
    angs' xs = map (\ (a, b, c) -> ang (a <-> b) (a <-> c)) $ zip3 xs (tail $ cycle xs) (tail . tail $ cycle xs)

-- | Testet die Methode ähnlich, durch Vergleich aller Winkelpermutationen
prop_Ähnlich :: Dreieck -> Dreieck -> Bool
prop_Ähnlich x y =
  ähnlich x y == (not . null $ intersect (angs x) (angs y))


wsw [a, b, c] = (ang (c <-> a) (b <-> a), (b <-> a), ang (a <-> b) (c <-> b))

wsws (a, b, c)
  | kollinear a b c = map (\ [x, y, z] -> (x, (y, y), z)) $ permutations [0, 0, 0]
  | otherwise = map wsw $ permutations [a, b, c]

-- | Testet die Methode kongruent, durch Vergleich aller Kombinationen von Winkel-Seite-Winkel Tripeln 
prop_Kongruent :: Dreieck -> Dreieck -> Bool
prop_Kongruent x y =
  kongruent x y == (not . null $ intersect (wsws x) (wsws y))

runTests = $quickCheckAll
