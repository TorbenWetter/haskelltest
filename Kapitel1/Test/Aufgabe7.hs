{-# LANGUAGE TemplateHaskell #-}
module Kapitel1.Test.Aufgabe7 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All

import Data.Complex

import Kapitel1.Aufgabe7

-- | Testet, ob # das selbe Ergebnis liefert wie +
prop_Plus :: (Double, Double) -> (Double, Double) -> Bool
prop_Plus (a, b) (c, d) = 
  (uncurry (:+) $ (a, b) # (c, d)) == (a :+ b) + (c :+ d)

-- | Testet, ob % das selbe Ergebnis liefert wie -
prop_Minus :: (Double, Double) -> (Double, Double) -> Bool
prop_Minus (a, b) (c, d) = 
  (uncurry (:+) $ (a, b) % (c, d)) == (a :+ b) - (c :+ d)

-- | Testet, ob ## das selbe Ergebnis liefert wie *
prop_Mult :: (Double, Double) -> (Double, Double) -> Bool
prop_Mult (a, b) (c, d) =
  (uncurry (:+) $ (a, b) ## (c, d)) == (a :+ b) * (c :+ d)

-- | Testet, ob %% das selbe Ergebnis liefert wie /
prop_Div :: (Double, Double) -> (Double, Double) -> Bool
prop_Div (a, b) (c, d) =
  (uncurry (:+) $ (a, b) %% (c + 1, d)) == (a :+ b) / ((c + 1) :+ d)

-- | Testet, ob alle Operationen linksassoziativ sind
prop_Assoc :: (Double, Double) -> (Double, Double) -> (Double, Double) -> Bool
prop_Assoc x y z = 
    (x # y) # z == x # y # z
    && (x % y) % z == x % y % z
    && (x ## y) ## z == x ## y ## z
    && (x %% (y # (1, 0))) %% (z # (1, 0)) == x %% (y # (1, 0)) %% (z # (1, 0))

-- | Testet, ob ## und %% höhere Priorität als # und % haben
prop_Prio :: (Double, Double) -> (Double, Double) -> (Double, Double) -> Bool
prop_Prio x y z =
    (x ## y) # z == x ## y # z
    && (x ## y) % z == x ## y % z
    && (x %% (y # (1, 0))) # z == x %% (y # (1, 0)) # z
    && (x %% (y # (1, 0))) % z == x %% (y # (1, 0)) % z

-- | Testet, ob Realteil und Imaginätteil zusammen wieder die selbe Zahl ergeben.
prop_RealImag :: (Num a, Eq a) => (a, a) -> Bool
prop_RealImag (a, b) = 
  (real (a, b), imag (a, b)) == (a, b)

-- | Testet die Polarkoordinatendarstellung gegen die Haskell-Referenzimplementierung.
prop_Polarkoord :: (Double, Double) -> Bool
prop_Polarkoord (a, b) = 
  let 
    (r, phi) = polar ((a + 1) :+ b)
    phi' = if phi < 0 then 2 * pi + phi else phi
    (rp, phip) = zuPolar ((a + 1), b)
  in
    abs (r - rp) < 0.0001 && abs (phi' - phip) < 0.0001

-- | Testet die Umwandlung zu Kartesischen Koordinaten gegen die Haskell-Referenzimplementierung.
prop_Cartesian :: (Double, Double) -> Bool
prop_Cartesian (r, phi) =
  let
    phi' = if phi <= -pi then 0 else if phi > pi then pi else phi
    phi'' = if phi' < 0 then 2 * pi + phi' else phi'
  in
    (magnitude $ (uncurry (:+) $ zuKartesisch (r, phi'')) -  mkPolar r phi') < 0.0001

runTests = $quickCheckAll