{-# LANGUAGE TemplateHaskell #-}

module Kapitel1.Test.Aufgabe5 ( runTests ) where

import Test.QuickCheck
import Test.QuickCheck.All

import Kapitel1.Aufgabe5
import Kapitel1.Test.Aufgabe4 ( Wochentag )

-- | Testet, ob neu erzeugte klassifizierte Termine richtig gelöscht werden.
prop_KlassifiziertenTerminLoeschen :: Wochentag -> Int -> Int -> Int -> Int -> Int -> Int -> String -> Bool
prop_KlassifiziertenTerminLoeschen wochentag tag monat jahr stunde minute dauer beschreibung =
  let
    termin = vereinbareKlassifiziertenTermin (show wochentag) tag monat jahr stunde minute dauer beschreibung
  in
    (not $ testeKlassifiziertenTermin termin) || (not . testeKlassifiziertenTermin $ loescheKlassifiziertenTermin termin)

runTests = $(quickCheckAll)