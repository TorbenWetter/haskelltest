{-# LANGUAGE TemplateHaskell #-}

module Kapitel1.Test.Aufgabe3 ( runTests ) where

import Test.QuickCheck
import Test.QuickCheck.All

import Kapitel1.Aufgabe3

-- | Testet, ob die Anzahl an Pence in einem Geldbetrag 12 * 20 * Pfund + 20 * Shilling + Pence ist.
prop_PenceInGeldbetrag :: Int -> Int -> Int -> Bool
prop_PenceInGeldbetrag pf sh pe = 
  (pence $ mkGeldbetrag pf sh pe) == pf * 12 * 20 + sh * 20 + pe

-- | Testet, ob die Anzahl an ganzen Shilling in einem Geldbetrag 
-- (Pfund * 12 * 20 + Shilling * 20 + Pence) `quot` 20  ist
prop_ShillingInGeldbetrag :: Int -> Int -> Int -> Bool
prop_ShillingInGeldbetrag pf sh pe =
  (shilling $ mkGeldbetrag pf sh pe) == (pf * 12 * 20 + sh * 20 + pe) `quot` 20

-- | Testet, ob die Anzahl an ganzen Pfund in einem Geldbetrag 
-- (Pfund * 12 * 20 + Shilling * 20 + Pence) `quot` (12 * 20) ist
prop_PfundInGeldbetrag :: Int -> Int -> Int -> Bool
prop_PfundInGeldbetrag pf sh pe =
  (pfund $ mkGeldbetrag pf sh pe) == (pf * 12 * 20 + sh * 20 + pe) `quot` (12 * 20)

-- | Testet, ob die Addition von Geldbeträgen der Addition der in ihenen vorkommenden Pence entspricht.
prop_Addiere :: Int -> Int -> Int -> Int -> Int -> Int -> Bool
prop_Addiere pf sh pe pf' sh' pe' =
  (pence $ addiere (mkGeldbetrag pf sh pe) (mkGeldbetrag pf' sh' pe')) 
  == (pf + pf') * 12 * 20 + (sh + sh') * 20 + (pe + pe')

-- | Testet, ob Zinssätze auf Geldebträgen genauso ausgerechnet werden wie auf Pence
prop_Zins pf sh pe p =
  (pence $ zins (mkGeldbetrag pf sh pe) p) == ((pf * 12 * 20 + sh * 20 + pe) * p) `div` 100

runTests = $quickCheckAll