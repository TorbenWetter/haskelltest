module Kapitel1.Aufgabe5
  ( testeKlassifiziertenTermin
  , vereinbareKlassifiziertenTermin
  , loescheKlassifiziertenTermin
  ) where

import Kapitel1.Aufgabe4

-- | Prüft, ob in Wochentag ein Arbeitstag ist
istArbeitstag :: String -> Bool
istArbeitstag "Sonntag" = False
istArbeitstag "Montag" = True
istArbeitstag "Dienstag" = True
istArbeitstag "Mittwoch" = True
istArbeitstag "Donnerstag" = True
istArbeitstag "Freitag" = True
istArbeitstag "Samstag" = False
istArbeitstag tag = False

-- | Prüft, ob in Wochentag ein Wochenendtag ist
istWochenendTag :: String -> Bool
istWochenendTag "Sonntag" = True
istWochenendTag "Montag" = False
istWochenendTag "Dienstag" = False
istWochenendTag "Mittwoch" = False
istWochenendTag "Donnerstag" = False
istWochenendTag "Freitag" = False
istWochenendTag "Samstag" = True
istWochenendTag tag = False

-- | Ordnet einem Monat einen Typen (Lang, Kurz, Mittel) zu.
-- Ungültige Monate bekommen den Typ "Keiner".
monatsTyp :: Int -> String
monatsTyp 1 = "Lang"
monatsTyp 2 = "Kurz"
monatsTyp 3 = "Lang"
monatsTyp 4 = "Mittel"
monatsTyp 5 = "Lang"
monatsTyp 6 = "Mittel"
monatsTyp 7 = "Lang"
monatsTyp 8 = "Lang"
monatsTyp 9 = "Mittel"
monatsTyp 10 = "Lang"
monatsTyp 11 = "Mittel"
monatsTyp 12 = "Lang"
monatsTyp anderer = "Keiner"

-- | Testet, ob ein Termin richtig klassifiziert wurde und ob er 
-- entsprechend 'Kapitel1.Aufgabe4.testeTermin' gültig ist.
testeKlassifiziertenTermin :: (String, String, ((String, Int, Int, Int), (Int, Int), Int, String)) -> Bool
testeKlassifiziertenTermin (terminTyp, monatsArt, ((wochentag, tag, monat, jahr), uhrzeit, dauer, beschreibung))
  = ( istArbeitstag wochentag && terminTyp == "Arbeitstermin" 
     || istWochenendTag wochentag && terminTyp == "Freizeittermin"
    ) 
    && (monatsArt == monatsTyp monat)
    && testeTermin ((wochentag, tag, monat, jahr), uhrzeit, dauer, beschreibung)

-- | Vereinbart einen klassifizierten Termin.
-- Termine werden nach Arbeitsterminen und Freizeitterminen entsprechend ihres Wochentages,
-- sowie anhand der Länge ihres Monats klassifiziert.
vereinbareKlassifiziertenTermin :: String
                                -- ^ Der Wochentag
                                -> Int
                                -- ^ Der Tag
                                -> Int
                                -- ^ Der Monat
                                -> Int
                                -- ^ Das Jahr
                                -> Int
                                -- ^ Die Stunde                               
                                -> Int
                                -- ^ Die Minute
                                -> Int
                                -- ^ Die Dauer in Minuten
                                -> String
                                -- ^ Die Beschreibung
                                -> (String, String, ((String, Int, Int, Int), (Int, Int), Int, String))
vereinbareKlassifiziertenTermin wochentag tag monat jahr stunde minute dauer beschreibung
  | istArbeitstag wochentag = 
    ("Arbeitstermin", monatsTyp monat, vereinbareTermin wochentag tag monat jahr stunde minute dauer beschreibung)
  | istWochenendTag wochentag =
    ("Freizeittermin", monatsTyp monat, vereinbareTermin wochentag tag monat jahr stunde minute dauer beschreibung)
  | otherwise = 
    ("GELÖSCHT", "Keiner", loescheTermin $ vereinbareTermin wochentag tag monat jahr stunde minute dauer beschreibung)

-- | Loescht einen klassifizierten Termin.
-- Verwendet 'Kapitel1.Aufgabe4.loescheTermin' und löscht zusätzlich die Klassifikation.
loescheKlassifiziertenTermin :: (String, String, ((String, Int, Int, Int), (Int, Int), Int, String))
                             -> (String, String, ((String, Int, Int, Int), (Int, Int), Int, String))
loescheKlassifiziertenTermin (terminTyp, monatsArt, termin) = ("GELÖSCHT", "Keiner", loescheTermin termin)