module Kapitel1.Aufgabe3 
  ( mkGeldbetrag
  , pfund
  , shilling
  , pence
  , addiere
  , zins
  ) where

-- Macht aus Pfund, Shilling und Pence einen Geldbetrag
mkGeldbetrag ::
  Int 
  -- ^ Pfund
  -> Int 
  -- ^ Shilling
  -> Int
  -- ^ Pence
  -> (Int, Int, Int)
mkGeldbetrag pf sh pe =
  let
    erg = pf * 12 * 20 + sh * 20 + pe
    pe' = erg `rem` 20
    sh' = (erg `quot` 20) `rem` 12
    pf' = erg `quot` (20 * 12)
  in    
   (pf', sh', pe')

-- | Rundet einen Geldbetrag auf ganze Pfund.
pfund :: 
  (Int, Int, Int) 
  -- ^ Der Geldbetrag (Pfund, Shilling, Pence)
  -> Int
pfund (pf, sh, pe) =
  let
    (pf', _, _) = mkGeldbetrag pf sh pe
  in
    pf'

-- | Rundet einen Geldbetrag auf ganze Shilling.
shilling :: 
  (Int, Int, Int) 
  -- ^ Der Geldbetrag (Pfund, Shilling, Pence)
  -> Int
shilling (pf, sh, pe) =
  let
    (pf', sh', _) = mkGeldbetrag pf sh pe
  in
    pf' * 12 + sh'
    

-- | Wandelt einen Geldbetrag in Pence um.
pence ::
  (Int, Int, Int)
  -- ^ Der Geldbetrag (Pfund, Shilling, Pence)
  -> Int
pence (pf, sh, pe) = pf * 12 * 20 + sh * 20 + pe

-- | Addiert zwei Geldbeträge.
-- Stellt das Ergebnis in Pfund, Shilling, Pence dar.
addiere :: 
  (Int, Int, Int) 
  -- ^ Erster Summand als Geldbetrag
  -> (Int, Int, Int)
  -- ^ Zweiter Summand als Geldbetrag
  -> (Int, Int, Int)
addiere (pf, sh, pe) (pf', sh', pe') =
  mkGeldbetrag (pf + pf') (sh + sh') (pe + pe')

-- | Berechnet Zinsen zu einem Geldbetrag.
-- Stellt das Ergebnis in Pfund, Shilling, Pence dar.
zins :: 
  (Int, Int, Int)
  -- ^ Der Geldbetrag
  -> Int
  -- ^ Der Zinssatz in Prozent
  -> (Int, Int, Int)
zins gb p =
  mkGeldbetrag 0 0 ((pence gb * p) `div` 100)
