module Kapitel1.Aufgabe2 ((#), (%), (##), (%%)) where

-- | Addiert zwei nichtnegative ganze Zahlen
(#) :: Int -> Int -> Int
x # 0 = x
x # y = (succ x) # (pred y)

infixl 6 #

-- | Subtrahiert zwei nichtnegative ganze Zahlen.
-- Gibt 0 zurück, wenn das Ergebnis der Subtraktion negativ wäre.
(%) :: Int -> Int -> Int
x % 0 = x
0 % x = 0
x % y = (pred x) % (pred y)

infixl 6 %

-- | Multipliziert zwei nichtnegative ganze Zahlen.
(##) :: Int -> Int -> Int
x ## 0 = 0
x ## y = x # x ## (y % 1)

infixl 7 ##

-- | Dividiert zwei nichtnegative ganze Zahlen.
-- Rundet ab, wenn das Ergebnis keine ganze Zahl wäre.
(%%) :: Int -> Int -> Int
x %% y 
 | x % y == 0 && y % x == 0 = 1
 | x % y == 0 = 0
 | otherwise = 1 # ((x % y) %% y)

infixl 7 %%