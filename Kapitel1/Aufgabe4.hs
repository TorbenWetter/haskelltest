module Kapitel1.Aufgabe4 
  ( testeDatum
  , erstelleDatum
  , loescheDatum
  , testeUhrzeit
  , erstelleUhrzeit
  , loescheUhrzeit
  , testeDauer
  , loescheDauer
  , testeBeschreibung
  , loescheBeschreibung
  , testeTermin
  , vereinbareTermin
  , loescheTermin
  ) where

-- | Testet, ob das übergebene Datum gültig ist.
-- Hierzu werden Tage und Monate auf Zugehörigkeit zu einem Bereich 
-- zwischen 1 und 31 bzw 1 und 12 geprüft. Wochentage müssen 
-- müssen "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag" oder "Sonntag" sein.
testeDatum :: (String, Int, Int, Int) -> Bool
testeDatum (wochentag, tag, monat, jahr) = (tag > 0 && tag < 31) && (monat > 0 && monat < 13)

-- | Erstellt ein datum zum angegebenen Wochentag, Tag, Monat und Jahr.
erstelleDatum :: String
              -- ^ Der Wochentag
              ->  Int
              -- ^ Der Tag
              -> Int
              -- ^ Der Monat
              -> Int 
              -- ^ Das Jahr
              -> (String, Int, Int, Int)
erstelleDatum wochenTag tag monat jahr = (wochenTag, tag, monat, jahr)

-- | Loescht ein Datum, indem es auf einen ungültigen Wert gesetzt wird.
loescheDatum :: (String, Int, Int, Int) -> (String, Int, Int, Int)
loescheDatum d = ("GELÖSCHT", 0, 0, 0)

-- | Testet eine übergebene Uhrzeit auf gültigkeit.
-- Minuten und Stunden werden auf die Zugehörigkeit zu einem Intervall
-- zwischen 0 und 59 bzw. zwischen 0 und 23 geprüft.
testeUhrzeit :: (Int, Int) -> Bool
testeUhrzeit (stunde, minute) = (stunde >= 0 && stunde < 24) && (minute >= 0 && minute < 60)

-- | Erstellt eine Uhrzeit.
erstelleUhrzeit :: Int
                -- ^ Die Stunde
                -> Int
                -- ^ Die Minute
                -> (Int, Int)
erstelleUhrzeit = (,)

-- | Loescht eine Uhrzeit, indem sie auf einen ungültigen Wert gesetzt wird.
loescheUhrzeit :: (Int, Int) -> (Int, Int)
loescheUhrzeit z = (-1, -1)

-- | Testet, ob eine Dauer eine positive Anzahl von Minuten hat.
testeDauer :: Int -> Bool
testeDauer d = d > 0

-- | Löscht eine Dauer durch Setzen auf einen ungültigen Wert.
loescheDauer :: Int -> Int
loescheDauer d = -1

-- Testet, ob eine Beschreibung gültig ist.
-- Ungültige Beschreibungen bestehen aus der Zeichenkette "GELÖSCHT"
testeBeschreibung :: String -> Bool
testeBeschreibung "GELÖSCHT" = False
testeBeschreibung beschreibung = True

-- Löscht eine Beschreibung durch setzen auf den Wert "GELÖSCHT"
loescheBeschreibung :: String -> String
loescheBeschreibung beschreibung = "GELÖSCHT"

-- Testet, ob ein Termin gültig ist. 
-- Hierzu werden die Methoden 'testeDatum', 
-- 'testeUhrzeit', 'testeDauer' und 'testeBeschreibung' verwendet.
testeTermin :: ((String, Int, Int, Int), (Int, Int), Int, String) -> Bool
testeTermin (datum, uhrzeit, dauer, beschreibung) =
  testeDatum datum && testeUhrzeit uhrzeit && testeDauer dauer && testeBeschreibung beschreibung


-- | Vereinbart einen neuen Termin.
vereinbareTermin :: String
                 -- ^ Der Wochentag
                 -> Int
                 -- ^ Der Tag
                 -> Int
                 -- ^ Der Monat
                 -> Int
                 -- ^ Das Jahr
                 -> Int
                 -- ^ Die Stunde
                 -> Int
                 -- ^ Die Minute
                 -> Int
                 -- ^ Die Dauer in Minuten
                 -> String
                 -- ^ Die Beschreibung
                 -> ((String, Int, Int, Int), (Int, Int), Int, String)
vereinbareTermin wochenTag tag monat jahr stunde minute dauer beschreibung =
  (erstelleDatum wochenTag tag monat jahr, erstelleUhrzeit stunde minute, dauer, beschreibung)

-- | Löscht einen Termin.
-- Verwendet die Methoden 'loescheDatum', 'loescheUhrzeit',
-- 'loescheDauer' und 'loescheBeschreibung', um die einzelnen
-- Werte des Termins ungültig zu machen.
loescheTermin :: ((String, Int, Int, Int), (Int, Int), Int, String) -> ((String, Int, Int, Int), (Int, Int), Int, String)
loescheTermin (datum, uhrzeit, dauer, beschreibung) =
  (loescheDatum datum, loescheUhrzeit uhrzeit, loescheDauer dauer, loescheBeschreibung beschreibung)



