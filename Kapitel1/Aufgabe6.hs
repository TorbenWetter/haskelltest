module Kapitel1.Aufgabe6 ( folgendesKennzeichen ) where


-- | Addiert zwei Ziffern und gibt das Ergebnis und den Übertrag im 10er-System zurück
addiereZiffern :: Int -> Int -> (Int, Int)
addiereZiffern x y = ((x + y) `mod` 10, (x + y) `div` 10)

-- | Schaltet einen Großbuchstaben (A-Z) zyklisch um n Buchstaben weiter und gibt, 
-- wenn bei Z angekommen, den Übertrag zurück.
addiereBuchstaben :: Char -> Int -> (Char, Int)
addiereBuchstaben x 0 = (x, 0)
addiereBuchstaben 'Z' n = let (c, n') = addiereBuchstaben 'A' (pred n) in (c, succ n')
addiereBuchstaben x n = addiereBuchstaben (succ x) (pred n)


-- | Gibt das folgende Kennzeichen im Bereich ('A', 'A', 'A', 0, 0, 0) bis ('Z', 'Z', 'Z', 9, 9, 9) zurück.
folgendesKennzeichen :: (Char, Char, Char, Int, Int, Int) -> (Char, Char, Char, Int, Int, Int)
folgendesKennzeichen ('Z', 'Z', 'Z', 9, 9, 9) = ('Z', 'Z', 'Z', 9, 9, 9)
folgendesKennzeichen (a, b, c, x, y, z) =
  let
    (z', u0) = addiereZiffern z 1
    (y', u1) = addiereZiffern y u0
    (x', u2) = addiereZiffern x u1
    (c', u3) = addiereBuchstaben c u2
    (b', u4) = addiereBuchstaben b u3
    (a', u5) = addiereBuchstaben a u4
  in
    if u5 == 0
      then (a', b', c', x', y', z')
      else folgendesKennzeichen (a', b', c', x', y', z')

