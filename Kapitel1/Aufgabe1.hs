module Kapitel1.Aufgabe1 (curry, uncurry) where

import Prelude hiding (curry, uncurry)

-- | Curryfiziert eine Funktion mit zwei Argumenten.
curry ::
  ((a, b) -> c)
  -- ^ Die zu curryfizierende Funktion
  -> a -> b -> c
  -- ^ Die curryfizierte Ergebnisfunktion
curry f x y = f (x, y)

-- | Macht das curryfizieren einer Funktion rückgängig.
uncurry ::
  (a -> b -> c)
  -- ^ Die curryfizierte Funktion
  -> (a, b) -> c
  -- ^ Die entcurryfizierte Ergebnisfunktion
uncurry f (x, y) = f x y


