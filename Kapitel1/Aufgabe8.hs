module Kapitel1.Aufgabe8 ( gerade ) where

-- | Geradengleichung durch die Punkte (x1, y1), (x2, y2).
gerade :: (Num a, Fractional a) 
          => (a, a)
          -- ^ Erster Punkt (x1, y1)
          -> (a, a)
          -- ^ Zweiter Punkt (x2, y2)
          -> a -> a
          -- ^ Resultierende Gleichung
gerade (x1, y1) (x2, y2) x = y1 + ((y2 - y1) / (x2 - x1)) * (x - x1)