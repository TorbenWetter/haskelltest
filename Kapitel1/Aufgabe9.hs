module Kapitel1.Aufgabe9 ( kollinear, ähnlich, kongruent ) where

-- | Bestimmt, ob die drei übergebenen Punkte kollinear sind.
kollinear :: (Num a, Eq a, Fractional a, Ord a) 
             => (a, a)
             -- ^ Der erste Punkt
             -> (a, a)
             -- ^  Der zweite Punkt
             -> (a, a)
             -- ^ Der dritte Punkt
             -> Bool             
kollinear (x1, y1) (x2, y2) (x3, y3)
  | (x1, y1) == (x2, y2) = True
  | x1 == x2 = x3 == x1
  | y1 == y2 = y3 == y1
  | otherwise = y1 + ((y2 - y1) / (x2 - x1)) * (x3 - x1) == y3

-- | Berechnet den Winkel zwischen Strecken (x1, y1) - (x2, y2) und (x1, y1) - (x3, y3).
winkel :: (Num a, Floating a)
          => (a, a)
          -- ^ Scheitelpunkt (x1, y1)
          -> (a, a)
          -- ^ Punkt (x2, y2) auf Basisschenkel
          -> (a, a)
          -- ^ Punkt (x3, y3) auf freiem Schenkel
          -> a
winkel (x1, y1) (x2, y2) (x3, y3) =
  acos $ (x2 - x1) * (x3 - x1) + (y2 - y1) * (y3 - y1)


-- | Orientiert ein Dreieck positiv und verwendet den Punkt 
-- mit geringstem Ursprungsabstand als ersten.
eindeutig :: (Ord a) 
             => ((a, a), (a, a), (a, a))
             -- ^ Punkte im zu orientierenden Dreieck
             -> ((a, a), (a, a), (a, a))
eindeutig (a, b, c)
  | a <= b && b <= c = (a, b, c)
  | a <= c && c <= b = (a, c, b)
  | b <= a && b <= c = (b, a, c)
  | b <= c && c <= a = (b, c, a)
  | c <= a && a <= b = (c, a, b)
  | otherwise   = (c, b, a)


-- | Bestimmt, ob die übergebenen Dreiecke ähnlich sind.
ähnlich :: 
  (Num a, Floating a, Ord a)
  => ((a, a), (a, a), (a, a))
  -- ^ Das erste Dreieck
  -> ((a, a), (a, a), (a, a))
  -- ^ Das zweite Dreieck
  -> Bool
ähnlich dreieck1 dreieck2 =
  let
    (a, b, c) = eindeutig dreieck1
    (d, e, f) = eindeutig dreieck2
    (alpha, beta, gamma) = (winkel a b c, winkel b a c, winkel c a b)
    (delta, epsilon, phi) = (winkel d e f, winkel e d f, winkel f d e)
  in
    (alpha, beta, gamma) == (delta, epsilon, phi)

-- | Bestimmt, ob die übergebenen Dreiecke kongruent sind.
kongruent ::
  (Num a, Floating a, Ord a)
  => ((a, a), (a, a), (a, a))
  -- ^ Das erste Dreieck
  -> ((a, a), (a, a), (a, a))
  -- ^ Das zweite Dreieck
  -> Bool
kongruent dreieck1 dreieck2 =
  let
    (a, b, c) = eindeutig dreieck1
    (d, e, f) = eindeutig dreieck2
  in
    (ähnlich (a, b, c) (d, e, f)) 
    && (fst a - fst b, snd a - snd b) == (fst d - fst e, snd d - snd e) 
    && (fst a - fst c, snd a - snd c) == (fst d - fst f, snd d - snd f)


