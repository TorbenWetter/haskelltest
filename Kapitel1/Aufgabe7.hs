module Kapitel1.Aufgabe7 
  ( (#)
  , (%)
  , (##)
  , (%%)
  , real
  , imag
  , betrag
  , zuPolar
  , zuKartesisch
  ) where


(#) :: Num a => (a, a) -> (a, a) -> (a, a)
(a, b) # (c, d) = (a + c, b + d)

infixl 6 #

(##) :: Num a => (a, a) -> (a, a) -> (a, a)
(a, b) ## (c, d) = (a * c - b * d, a * d + b * c)

infixl 7 ##

(%) :: Num a => (a, a) -> (a, a) -> (a, a)
(a, b) % (c, d) = (a - c, b - d)

infixl 6 %

(%%) :: (Num a, Fractional a) => (a, a) -> (a, a) -> (a, a)
(a, b) %% (c, d) = ((a*c + b*d) / (c^2 + d^2), (b*c - a*d) / (c^2 + d^2))

infixl 7 %%

real :: Num a => (a, a) -> a
real = fst

imag :: Num a => (a, a) -> a
imag = snd

betrag :: (Num a, Floating a) => (a, a) -> a
betrag (a, b) = sqrt(a^2 + b^2)

phase :: (Num a, Floating a, Ord a) => (a, a) -> a
phase (a, b)
  | b >= 0 = acos (a / betrag (a, b))
  | otherwise = 2 * pi - acos (a / betrag (a, b))

zuPolar :: (Num a, Floating a, Ord a) => (a, a) -> (a, a)
zuPolar (a, b) = (betrag (a, b), phase (a, b))

zuKartesisch :: (Num a, Floating a, Ord a) => (a, a) -> (a, a)
zuKartesisch (r, phi) = (r * cos(phi), r * sin(phi))

