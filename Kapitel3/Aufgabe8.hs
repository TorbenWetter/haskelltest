module HaskellFuerDenRestVonUns.Kapitel3.Aufgabe8
  ( addiere
  , multipliziere
  , differenziere
  ) where


-- | Addiert zwei Polynome, welche in Form ihrer Koeffizienten angegeben sind
addiere :: (Num a) => [a] -> [a] -> [a]
addiere [] [] = []
addiere [] xs = xs
addiere xs [] = xs
addiere (x:xs) (y:ys) = (x+y):(addiere xs ys)

-- | Multipliziert zwei Polynome in Koeffizientendarstellung
multipliziere :: (Num a) => [a] -> [a] -> [a]
multipliziere xs ys =
  let
    m = length xs - 1
    n = length ys - 1
  in
    map (\ j -> sum $ zipWith (*) (take (j + 1) $ drop (j-m) ys) (drop (m - j) $ reverse xs)) [0 .. (m + n)]


-- | Differenziert ein Polynom in Koeffizientendarstellung
differenziere :: (Num a, Enum a) => [a] -> [a]
differenziere xs = zipWith (*) (tail xs) [1..]