module Main where

import qualified HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe4 as K3A4
import qualified HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe5 as K3A5
import qualified HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe6 as K3A6
import qualified HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe7 as K3A7
import qualified HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe8 as K3A8
import qualified HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe9 as K3A9

main = do
  K3A4.runTests
  K3A5.runTests
  K3A6.runTests
  K3A7.runTests
  K3A8.runTests
  K3A9.runTests