{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe4 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import qualified HaskellFuerDenRestVonUns.Kapitel3.Aufgabe4 as A4

data Klammern = Rund Klammern
              | Eckig Klammern
              | Geschweift Klammern
              | Defekt Char
              | Leer deriving (Eq)

instance Arbitrary Klammern where
  arbitrary = 
    oneof [ Rund <$> arbitrary'
          , Eckig <$> arbitrary'
          , Geschweift <$> arbitrary'
          , pure Leer
          ]
    where
      arbitrary' = oneof [arbitrary, Defekt <$> elements "([{}])"]

instance Show Klammern where
  show (Rund k) = '(' : ((show k) ++ ")")
  show (Eckig k) = '[' : ((show k) ++ "]")
  show (Geschweift k) = '{' : ((show k) ++ "}")
  show (Defekt c) = c:[]
  show (Leer) = ""

-- | Prüft einen generierten Baum aus Klammern auf korrektheit
baumKlammerung :: Klammern -> Bool
baumKlammerung (Rund k) = baumKlammerung k
baumKlammerung (Eckig k) = baumKlammerung k
baumKlammerung (Geschweift k) = baumKlammerung k
baumKlammerung (Leer) = True
baumKlammerung _ = False

-- | Testet die Funktion 'klammerung' gegen eine Referenzimplementierung auf einem generierten Wald
prop_klammerung :: [Klammern] -> Bool
prop_klammerung ks =
  (A4.klammerung (concat $ map show ks)) == (and $ map baumKlammerung ks)

runTests = $quickCheckAll