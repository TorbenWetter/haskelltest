{-# LANGUAGE TemplateHaskell #-}
module HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe7
  ( runTests 
  ) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified HaskellFuerDenRestVonUns.Kapitel3.Aufgabe5 as A5
import qualified HaskellFuerDenRestVonUns.Kapitel3.Aufgabe6 as A6
import qualified HaskellFuerDenRestVonUns.Kapitel3.Aufgabe7 as A7
import HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe5 (Matrix (..), dimensionen, kompatibel)
import HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe6 (quadratisch)

-- | Verwandelt eine dünnbesetzte Matrix in eine vollbesetzte mxn-Matrix
vollbesetzt :: (Num a) => Int -> Int -> [(a, Int, Int)] -> [[a]]
vollbesetzt m n xs = 
  [[ head $ [ x | (x, k, l) <- xs, k == z, l == s ] ++ [0] | s <- [0..n-1]] | z <- [0..m-1]]

-- | Verwandelt eine vollbesetzte Matrix in eine dünnbesetzte
dünnbesetzt :: (Eq a, Num a) => [[a]] -> [(a, Int, Int)]
dünnbesetzt xss = 
  filter (\ (x, _, _) -> x /= 0) . concat $ zipWith (\ xs z -> zipWith (\ x s -> (x, z, s)) xs [0..]) xss [0..]

-- | Entfernt alle leeren einträge aus einer vollbesetzen Matrix
normalisiere :: [[a]] -> [[a]]
normalisiere xss = filter (not . null) xss

-- | Vergleicht das Transponieren einer dünnbesetzten Matrix 
-- mit dem Transponieren einer vollbesetzten (Augabe 3.5)
prop_transponiere :: Matrix -> Bool
prop_transponiere mat@(Matrix m) = 
  let
    Just (z, s) = dimensionen mat
  in
    (A5.transponiere m) == (vollbesetzt s z . A7.transponiere $ dünnbesetzt m)

-- | Vergleicht das Multiplizieren von dünnbesetzten Matrizen 
-- mit dem Multiplizieren von vollbesetzten (Aufgabe 3.5)
prop_multipliziere :: Matrix -> Matrix -> Bool
prop_multipliziere mat1 mat2 = 
  let
    (mat1'@(Matrix m1), mat2'@(Matrix m2)) = kompatibel mat1 mat2
    Just (z, _) = dimensionen mat1'
    Just (_, s) = dimensionen mat2'
  in
    (normalisiere $ m1 A5.~* m2) == (vollbesetzt z s $ (dünnbesetzt m1) A7.~* (dünnbesetzt m2))

-- | Vergleicht das Berechnen von Determinanten von dünnbesetzten Matrizen
-- mit dem Berechnen von Determinanten vollbesetzter Matrizen (Aufgabe 3.6)
prop_det :: Matrix -> Bool
prop_det mat =
  let
    (Matrix m) = quadratisch mat
  in
    (A6.det m) == (A7.det $ dünnbesetzt m)

runTests = $quickCheckAll