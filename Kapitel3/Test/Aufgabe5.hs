{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe5 
  ( runTests
  , Matrix (..)
  , dimensionen
  , kompatibel) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import Control.Applicative
import Data.List (group)

import qualified HaskellFuerDenRestVonUns.Kapitel3.Aufgabe5 as A5

data Matrix = Matrix [[Int]] deriving (Eq, Show)

-- | Berechnet die Anzahl von Zeilen und Spalten in einer Matrix.
-- Gibt Nothing zurück, wenn die Matrix unpassend dimensioniert ist.
dimensionen :: Matrix -> Maybe (Int, Int)
dimensionen (Matrix xss) = 
  case (group $ map length xss) of
    [] -> Just (0, 0)
    [(0:_)] -> Just (0, 0)
    [(spalten:_)] -> Just (length xss, spalten)
    _ -> Nothing

-- | Macht zwei Matrizen kompatibel für die Produktbildung durch verwerfen von Zeilen und Spalten.
kompatibel :: Matrix -> Matrix -> (Matrix, Matrix)
kompatibel a@(Matrix as) b@(Matrix bs) =
  let
    Just (zm, sm) = dimensionen a
    Just (zn, sn) = dimensionen b
    zs = min sm zn
  in
    (Matrix $ map (take zs) as, Matrix $ take zs bs)

instance Arbitrary Matrix where
  arbitrary = do
    (zeilen, spalten) <- arbitrary
    Matrix <$> vectorOf (zeilen `mod` 5) (vectorOf (spalten `mod` 5) arbitrary)

-- | Prüft, ob die Funktion transponiere eine richtig dimensionierte Matrix zurückgibt.
prop_dimensionen :: Matrix -> Bool
prop_dimensionen m@(Matrix xss) = 
  let
    Just (zeilen, spalten) = dimensionen m
  in
    Just (spalten, zeilen) == (dimensionen $ Matrix (A5.transponiere xss))

-- | Prüft, ob für alle j, k: (m!!j)!!k == (transponiere m !! k)!!j
prop_transponiere :: Matrix -> Bool
prop_transponiere m@(Matrix xss) =
  let
    Just (zeilen, spalten) = dimensionen m
    xss' = A5.transponiere xss
  in
    and [(xss!!j)!!k == (xss'!!k)!!j | j <- [0..zeilen-1], k <- [0..spalten-1]]

-- | Prüft, ob die Funktion multipliziere eine richtig dimensionierte Matrix zurückgibt.
prop_dimensionenMult :: Matrix -> Matrix -> Bool
prop_dimensionenMult m@(Matrix xss) n@(Matrix yss) = 
  let
    (m'@(Matrix xss'), n'@(Matrix yss')) = kompatibel m n
    Just (zm, _) = dimensionen m'
    Just (zs, sn) = dimensionen n'
  in
    if zs == 0
    then Just (0, 0) == (dimensionen . Matrix $ xss' A5.~* yss')
    else Just (zm, sn) == (dimensionen . Matrix $ xss' A5.~* yss')

-- | Testet, ob die Funktion (~*) das Skalarprodukt von Zeilen und Spalten liefert
prop_mult :: Matrix -> Matrix -> Bool
prop_mult m@(Matrix xss) n@(Matrix yss) =
  let
    (Matrix m', Matrix n') = kompatibel m n
    Just (zm, _) = dimensionen (Matrix m')
    Just (zs, sn) = dimensionen (Matrix n')
  in
    and [(((m' A5.~* n') !! z) !! s) == (sum $ zipWith (*) (m' !! z) (map (!! s) n')) | z <- [0 .. zm-1], s <- [0 .. sn-1]]

runTests = $quickCheckAll