{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe6 ( runTests, quadratisch ) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import Control.Applicative

import qualified HaskellFuerDenRestVonUns.Kapitel3.Aufgabe6 as A6
import qualified HaskellFuerDenRestVonUns.Kapitel3.Aufgabe5 as A5
import HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe5 (Matrix (..), dimensionen, kompatibel)

-- | Macht eine Matrix quadratisch durch verwerfen von Zeilen oder Spalten
quadratisch :: Matrix -> Matrix
quadratisch a@(Matrix xss) = 
  let
    Just n = uncurry min <$> dimensionen a
  in
    Matrix $ take n (map (take n) xss)

-- | Testet, ob die Determinante einer 2x2 Matrix richtig ausgerechnet wird
prop_2x2 :: ((Int, Int), (Int, Int)) -> Bool
prop_2x2 ((a, b), (c, d)) = A6.det [[a, b], [c, d]] == a * d - b * c

-- | Testet, ob das Produkt zweier Determinanten der Determinante des Produkts ihrer Matrizen ist.
prop_prod :: Matrix -> Matrix -> Bool
prop_prod a@(Matrix xss) b@(Matrix yss) = 
  let
    Just (za, sa) = dimensionen a
    Just (zb, sb) = dimensionen b
    n = minimum [za, sa, zb, sb]
    (a', b') = (take n $ map (take n) xss, take n $ map (take n) yss)
  in
    A6.det a' * A6.det b' == A6.det (a' A5.~* b')

-- | Testet, ob die Multiplikation mit einem Skalar richtig erfasst wird (det (c * A) = c^n * det A)
prop_scalarProd :: Int -> Matrix -> Bool
prop_scalarProd c a = 
  let
    Matrix a' = quadratisch a
  in
    A6.det (map (map (*c)) a') == c ^ length a' * A6.det a'


-- | Testet, ob die Determinante der Transponierten einer Matrix gleich der Determinanten der Matrix ist
prop_transponiert :: Matrix -> Bool
prop_transponiert a = 
  let
    (Matrix a') = quadratisch a
  in
    A6.det a' == A6.det (A5.transponiere a')

runTests = $quickCheckAll