{-# LANGUAGE TemplateHaskell #-}
module HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe9 ( runTests ) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified HaskellFuerDenRestVonUns.Kapitel3.Aufgabe9 as A9

-- | Testet, ob durch Verschlüsseln jeder Buchstabe 3 weiter geschaltet wird
prop_verschl :: Bool
prop_verschl = 
  let
    tabelleKlein = (['d' .. 'z'] ++ ['a' .. 'c'])
    tabelleGroß = (['D'..'Z'] ++ ['A' .. 'C'])
  in 
    A9.verschl ['a' .. 'z'] == tabelleKlein &&
    A9.verschl ['A' .. 'Z'] == tabelleGroß

-- | Testet, ob entschlüsseln invers zu verschlüsseln ist
prop_invers :: [Char] -> Bool
prop_invers xs = xs == (A9.entschl $ A9.verschl xs)

runTests = $quickCheckAll