{-# LANGUAGE TemplateHaskell #-}
module HaskellFuerDenRestVonUns.Kapitel3.Test.Aufgabe8 ( runTests ) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified HaskellFuerDenRestVonUns.Kapitel3.Aufgabe8 as A8

-- | Streicht überflüssige Nullen aus Ergebnissen von Polynomberechnungen
normalisiere :: (Eq a, Num a) => [a] -> [a]
normalisiere = reverse . dropWhile (== 0) . reverse

-- | Testet, ob die übergebene Operation auf Polynomen kommutativ ist
kommutativ :: (Num a, Eq a) => ([a] -> [a] -> [a]) -> [a] -> [a] -> Bool
kommutativ f xs ys = normalisiere (xs `f` ys) == normalisiere (ys `f` xs)

-- | Testet, ob die übergebene Operation auf Polynomen assoziativ ist
assoziativ :: (Num a, Eq a) => ([a] -> [a] -> [a]) -> [a] -> [a] -> [a] -> Bool
assoziativ f xs ys zs = normalisiere ((xs `f` ys) `f` zs) == normalisiere (xs `f` (ys `f` zs))

-- | Testet, ob die übergebene Operation g rechtsdistributiv bezüglich f ist
distributiv :: (Num a, Eq a) => ([a] -> [a] -> [a]) -> ([a] -> [a] -> [a]) -> [a] -> [a] -> [a] -> Bool
distributiv f g xs ys zs = normalisiere ((xs `f` ys) `g` zs) == normalisiere ((xs `g` zs) `f` (ys `g` zs))

-- | Testet, ob Verknüpfung mit der Inversen auf das neutrale Element abbildet
inverse :: (Eq a, Num a) => ([a] -> [a] -> [a]) -> ([a] -> [a]) -> [a] -> [a] -> Bool
inverse f inv n xs = normalisiere (xs `f` (inv xs)) == normalisiere n

-- | Testet, ob Verknüpfung mit dem neutralen Element, die Identität ergibt
neutral :: (Eq a, Num a) => ([a] -> [a] -> [a]) -> [a] -> [a] -> Bool
neutral f n xs = normalisiere (xs `f` n) == normalisiere xs

-- Instanzen für addition und multiplikation von Polynomen
prop_kommutativ_add :: (Num a, Eq a) => [a] -> [a] -> Bool
prop_kommutativ_add = kommutativ A8.addiere
prop_kommutativ_mult :: (Num a, Eq a) => [a] -> [a] -> Bool
prop_kommutativ_mult = kommutativ A8.multipliziere
prop_assoziativ_add :: (Num a, Eq a) => [a] -> [a] -> [a] -> Bool
prop_assoziativ_add = assoziativ A8.addiere
prop_assoziativ_mult :: (Num a, Eq a) => [a] -> [a] -> [a] -> Bool
prop_assoziativ_mult = assoziativ A8.multipliziere
prop_distributiv_add_mult :: (Num a, Eq a) => [a] -> [a] -> [a] -> Bool
prop_distributiv_add_mult = distributiv A8.addiere A8.multipliziere
prop_inverse_add :: (Num a, Eq a) => [a] -> Bool
prop_inverse_add xs = inverse A8.addiere (map ((-1) *)) (map (const 0) xs) xs
prop_neutral_add :: (Num a, Eq a) => [a] -> Bool
prop_neutral_add xs = neutral A8.addiere (map (const 0) xs) xs
prop_neutral_mult :: (Num a, Eq a) => [a] -> Bool
prop_neutral_mult xs = neutral A8.multipliziere [1] xs

-- | Testet, ob die Ableitung von Polynomen ihrer mathematischen Definition entspricht
prop_differenziere :: (Num a, Eq a, Enum a) => [a] -> Bool
prop_differenziere xs = 
  and $ [(A8.differenziere xs) !! (i - 1) == (fromInteger (toInteger i)) * (xs !! i) | i <- [1 .. length xs - 1]]

runTests = $quickCheckAll