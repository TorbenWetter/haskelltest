module HaskellFuerDenRestVonUns.Kapitel3.Aufgabe4 ( klammerung ) where

-- | Prüft, ob der übergebene String korrekt geklammert ist.
klammerung :: String -> Bool
klammerung zs = 
  [] == foldl klammerung' [] zs
  where
    klammerung' erwartet '(' = ')':erwartet
    klammerung' erwartet '[' = ']':erwartet
    klammerung' erwartet '{' = '}':erwartet
    klammerung' (')':erwartet) ')' = erwartet
    klammerung' (']':erwartet) ']' = erwartet
    klammerung' ('}':erwartet) '}' = erwartet
    klammerung' _ _ = "fehler"
