module HaskellFuerDenRestVonUns.Kapitel3.Aufgabe9
  ( verschl
  , entschl
  ) where

-- | Wiederholt eine Liste zyklisch
zyklisch :: [a] -> [a]
zyklisch xs = xs ++ (zyklisch xs)

-- | Gibt das Element zurück, das sich 3 Positionen weiter
-- in der zyklisch wiederholten Eingabeliste befindet.
dreiWeiter :: (Eq a) => [a] -> a -> a
dreiWeiter cs c = 
  head . drop 3 . dropWhile (/= c) $ zyklisch cs

-- | Verschlüsselt eine Zeichenkette mit dem Caesar Chiffre
verschl :: [Char] -> [Char]
verschl cs = map verschl' cs
  where
    verschl' c
       | c >= 'A' && c <= 'Z' = dreiWeiter ['A'..'Z'] c
       | c >= 'a' && c <= 'z' = dreiWeiter ['a'..'z'] c
       | otherwise = c

-- | Entschlüsselt eine Zeichenkette, die mit dem Caesar Chiffre verschlüsselt wurde.
entschl :: [Char] -> [Char]
entschl cs = map entschl' cs
  where
    entschl' c
      | c >= 'A' && c <= 'Z' = dreiWeiter (reverse ['A' .. 'Z']) c
      | c >= 'a' && c <= 'z' = dreiWeiter (reverse ['a'..'z']) c
      | otherwise = c
