module HaskellFuerDenRestVonUns.Kapitel3.Aufgabe6 ( det ) where

-- | Liefert die Matrix a nach Streichen der k-ten Zeile und i-ten Spalte.
streiche :: [[a]]
         -- ^ Eingabematrix
         -> Int
         -- ^ Zu streichende Zeile
         -> Int
         -- ^ Zu streichende Spalte
         -> [[a]]
streiche a k l = streiche' k $ map (streiche' l) a
  where
    streiche' j xs = (take j xs) ++ (drop (j+1) xs)

-- | Berechnet die Determinante der übergebenen Matrix
det :: (Num a) => [[a]] -> a
det [] = 0
det [[]] = 0
det [[a]] = a
det a = 
  let 
    l = 0 
  in
  sum $ map (\ k -> (-1)^(k+l) * ((a!!k)!!l) * det (streiche a k l)) [0 .. (length a) - 1]
