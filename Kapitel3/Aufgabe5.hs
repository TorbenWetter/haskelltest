module HaskellFuerDenRestVonUns.Kapitel3.Aufgabe5 ( transponiere, (~*) ) where

-- | Transponiert die übergebene Matrix.
-- Undefiniert für Matrizen, die nicht wohlkonditioniert sind.
transponiere :: [[a]] -> [[a]]
transponiere [] = []
transponiere ([]:leer) = []
transponiere xss = 
  (\ (ys, yss) -> ys:(transponiere yss)) . unzip $ map (\ xs -> (head xs, tail xs)) xss


-- | Multipliziert die übergebenen Matrizen.
-- Undefiniert für Matrizen, deren Dimensionen nicht übereinstimmen.
(~*) :: (Num a) => [[a]] -> [[a]] -> [[a]]
xss ~* yss = map (\ xs -> map (sum . zipWith (*) xs) $ transponiere yss) xss