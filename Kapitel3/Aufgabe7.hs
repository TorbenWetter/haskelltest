module HaskellFuerDenRestVonUns.Kapitel3.Aufgabe7 
  ( transponiere
  , (~*)
  , det
  ) where

-- | Transponiert eine dünnbesetzte Matrix.
transponiere :: [(a, Int, Int)] -> [(a, Int, Int)]
transponiere xs = map (\ (x, i, j) -> (x, j, i)) xs

-- | Entfernt alle Duplikate aus einer Liste
nub :: (Eq a) => [a] -> [a]
nub xs = foldl (\ xs' x -> if x `elem` xs' then xs' else x:xs') [] xs


-- | Gibt eine Liste mit allen besetzen Zeilennummern einer dünnbesetzten Matrix zurück.
zeilen :: [(a, Int, Int)] -> [Int]
zeilen xs = nub $ map (\ (_, z, _) -> z) xs

-- | Gibt eine Liste mit allen besetzen Spaltennummern einer dünnbesetzten Matrix zurück.
spalten :: [(a, Int, Int)] -> [Int]
spalten xs = nub $ map (\ (_, _, s) -> s) xs


-- | Multipliziert zwei dünnbesetzte Matrizen.
(~*) :: (Num a, Eq a) => [(a, Int, Int)] -> [(a, Int, Int)] -> [(a, Int, Int)]
xs ~* ys = [ (e, z, s)
             | z <- zeilen xs
             , s <- spalten ys
             , let e = sum [ x * y | (x, i, j) <- xs, (y, m, n) <- ys, i == z, n == s, j == m ]
             , e /= 0
           ]

-- | Streicht die k-te Zeile und die l-te Spalte aus einer dünnbesetzten Matrix
streiche :: [(a, Int, Int)] -> Int -> Int -> [(a, Int, Int)]
streiche xs k l = 
  foldl (streiche' k l) [] xs
  where
    streiche' k l xs (x, m, n) =
      case (compare m k, compare n l) of
        (LT, LT) -> (x, m, n):xs
        (LT, GT) -> (x, m, n-1):xs
        (GT, LT) -> (x, m-1, n):xs
        (GT, GT) -> (x, m-1, n-1):xs
        otherwise -> xs

-- | Berechnet die Determinante einer dünnbesetzten Matrix
det :: (Num a) => [(a, Int, Int)] -> a
det [] = 0
det [(x, _, _)] = x
det xs = foldl (summiereDet xs) 0 $ zeilen xs
  where
    summiereDet xs summe z = 
      case filter (\ (_, k, l) -> k == z && l == 0) xs of
        [] -> summe
        [(akl, k, l)] -> summe + (-1)^(k + l) * akl * det (streiche xs k l)
