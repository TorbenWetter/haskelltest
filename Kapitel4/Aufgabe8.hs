module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe8 ( ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5

instance (Eq a) => Eq (Baum a) where
  Leer == Leer = True
  (Knoten x l r) == (Knoten x' l' r') = 
    x == x' && l == l' && r == r'
  _ == _ = False

