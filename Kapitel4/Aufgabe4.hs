module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe4 ( Warteschlange (..), PRIO (..) ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe3 (Warteschlange (..))

class PRIO w where
  erzeugen :: (Ord a) => w a
  einfügen :: (Ord a) => a -> w a -> w a
  entfernen :: (Ord a) => w a -> Maybe (a, w a)
  istLeer :: (Ord a) => w a -> Bool

instance PRIO Warteschlange where
  erzeugen = Leer
  einfügen x Leer = x :>: Leer
  einfügen x (y :>: zs)
    | x <= y = x :>: (y :>: zs)
    | otherwise = y :>: (einfügen x zs)
  entfernen Leer = Nothing
  entfernen (x :>: w) = Just (x, w)
  istLeer Leer = True
  istLeer _ = False


