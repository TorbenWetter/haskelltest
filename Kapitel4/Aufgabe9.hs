module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe9 ( entferne ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5

-- | Entfernt einen Knoten aus einem Binären Suchbaum
entferne :: (Ord a) => a -> Baum a -> Baum a
entferne x (Leer) = Leer
entferne x (Knoten y l r)
           | x > y = Knoten y l (entferne x r)
           | x < y = Knoten y (entferne x l) r
           | x == y = 
             case (l, r) of
               (Leer, Leer) -> Leer
               (Knoten _ _ _, Leer) -> l
               (Leer, Knoten _ _ _) -> r
               _ -> Knoten (minimal r) l (entferne (minimal r) r)
  where
    minimal (Knoten x Leer _) = x
    minimal (Knoten x l _) = minimal l