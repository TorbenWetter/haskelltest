module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe6 ( baumSumme ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5 (Baum (..))

baumSumme :: (Num a) => Baum a -> a
baumSumme (Leer) = 0
baumSumme (Knoten x l r) = x + (baumSumme l) + (baumSumme r)

