module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe7 ( ) where

data Baum a = Knoten a (Baum a) (Baum a) | Leer

instance (Show a) => Show (Baum a) where
  show baum = show' baum ""
    where
      show' (Leer) blanks = '\n' : (blanks ++ "Leer")
      show' (Knoten x l r) blanks =
        '\n' : (blanks ++ show x ++ show' l ("  " ++ blanks) ++ show' r ("  " ++ blanks))

