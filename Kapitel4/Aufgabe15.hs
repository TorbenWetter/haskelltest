module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe15 ( topSort ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11

-----------------------------------------------------------------------------
-- | Topologische sortierung
-- 1. Per Induktion in der Größe des Graphen:
-- Induktionsanfang: Graph mit einem Knoten v
-- a) v hat keine Kante auf sich selbst
-- => topSort(G) = v:topSort(({}, {})) = v:[] = [v]
-- b) v hat eine Kante auf sich selbst
-- => v ist Endpunkt einer Kante, es existiert kein anderer
-- Knoten
-- => topSort(G) = []
--
-- Induktionsschritt: Graph mit n+1 Knoten v_1 ... v_n+1
-- a) v_(n+1) erzeugt keinen Kreis
-- => topSort(G)
--    = topSort(G_vorgänger(v_n+1)) ++ topSort(G \ G_vorgänger(v_n+1))
-- G_vorgänger(v_n+1) hat <= n Knoten und
-- G \ G_vorgänger(v_n+1) hat <= (n + 1) - 1 = n Knoten (v ist enthalten)
-- => IH: topSort arbeitet korrekt
-- b) v_(n+1) erzeugt einen Kreis
-- => ex. m >= 0: ex. v_1, v_2 ... v_m: 
--      (v_j, v_(j+1)) in Kanten(G) und
--      (v_m, v_n+1) in Kanten(G) und
--      (v_n+1, v_j) in Kanten(G)
-- => Um v_n+1 zu löschen, muss der Knoten v_m gelöscht werden,
--    um v_m zu löschen, muss der Knoten v_m-1 gelöscht werden, ...
--    um v_1 zu löschen, muss der Knoten v_n+1 gelöscht werden
-- => v_n+1 kann nicht gelöscht werden.
-----------------------------------------------------------------------------

topSort :: (Eq a) => GGraph a -> [a]
topSort (GGraph ns es) =
  case ohneZielKnoten es ns of
    (x:<:xs) -> x:(topSort (GGraph (delete x ns) (ohneKnoten x es)))    
    Leer -> []
  where
    ohneZielKnoten :: (Eq a) => MeineMenge (GKante a) -> MeineMenge a -> MeineMenge a
    ohneZielKnoten Leer ys = ys
    ohneZielKnoten ((_:->:x):<:xs) ys = ohneZielKnoten xs (delete x ys)
    ohneKnoten :: (Eq a) => a -> MeineMenge (GKante a) -> MeineMenge (GKante a)
    ohneKnoten n (k@(x :->: y):<:xs)
      | x == n || y == n = ohneKnoten n xs
      | otherwise = k :<: (ohneKnoten n xs)
    ohneKnoten n Leer = Leer