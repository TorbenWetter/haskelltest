module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe14 ( erreichbar ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11
import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe13

erreichbar :: (Eq a) => GGraph a -> a -> a -> Bool
erreichbar g von nach =
  case dijkstra g (kosten g) von of
    Just f -> f nach < maxBound
    Nothing -> False
  where
    kosten :: (Eq a) => GGraph a -> GKante a -> Int
    kosten (GGraph ns es) kante@(s :->: t)
            | s == t = 1
            | kante `element` es = 1
            | otherwise = maxBound