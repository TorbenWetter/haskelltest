module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5 ( Baum (..), zuGroßbaum  ) where

data Baum a
  = Knoten a (Baum a) (Baum a)
  | Leer
  deriving (Show, Read)
  
-- | Bäume sind auch Funktoren:
-- fmap id Leer = Leer
-- fmap id (Knoten x l r) 
-- = Knoten (id x) (fmap id l) (fmap id r)
-- = Knoten x l r
-- fmap f (fmap g Leer) 
-- = fmap f Leer 
-- = Leer 
-- = fmap (f . g) Leer
-- fmap f (fmap g (Knoten x l r)) 
-- = fmap f (Knoten (g x) (fmap g l) (fmap g r))
-- = Knoten (f (g x)) (fmap f (fmap g l)) (fmap f (fmap g r))
-- = Knoten (f . g $ x) (fmap (f . g) l) (fmap (f . g) r)
-- = fmap (f . g) (Knoten x l r)
instance Functor Baum where
  fmap f (Knoten x l r) = Knoten (f x) (fmap f l) (fmap f r)
  fmap f (Leer) = Leer

-- | Zuordnung von Klein- und Großbuchstaben.
buchstabenTabelle :: Char -> Char
buchstabenTabelle c = 
  foldl (\ res (x, y) -> if x == c then y else res) c $ zip ['a' .. 'z'] ['A' .. 'Z']

-- | Verwandelt die Buchstaben in einem Baum in Großbuchstaben
-- Suchbäume bleiben nicht erhalten:
-- "Aa" < "aa", aber "AA" == "AA", sodass Duplikate entstehen können.
zuGroßbaum :: Baum String -> Baum String
zuGroßbaum baum = fmap (map buchstabenTabelle) baum

