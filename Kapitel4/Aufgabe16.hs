module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe16 ( Graph (..) ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11

newtype Graph a = Graph ([a], a -> [a])

instance (Show a) => Show (Graph a) where
  show (Graph (as, f)) = 
    "Graph " ++ show as ++ (show $ map (\ a -> "a -> " ++ show (f a)) as)

instance (Eq a) => Eq (Graph a) where
  (Graph (as, f)) == (Graph (bs, g)) =
    (toMenge as == toMenge bs) &&
    all (\ a -> toMenge (f a) == toMenge (g a)) as