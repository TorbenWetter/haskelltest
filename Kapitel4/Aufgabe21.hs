module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe21
  ( Testable (..)
  , Deletable (..)
  , Wochentag (..)
  , Tag (..)
  , Monat (..)
  , Jahr (..)
  , Datum (..)
  , Stunde (..)
  , Minute (..)
  , Uhrzeit (..)
  , Dauer (..)
  , Beschreibung (..)
  , Termin (..)
  , TerminArt (..)
  , MonatsArt (..)
  , KlassifizierterTermin (..)
  , terminArt
  , monatsArt
  , klassifiziereTermin
  ) where

-- | Objekte, die man auf Gültigkeit prüfen kann
class Testable a where
  teste :: a -> Bool

-- | Objekte, die man löschen kann
class Deletable a where
  lösche :: a -> a

data Wochentag
  = Montag 
  | Dienstag 
  | Mittwoch 
  | Donnerstag 
  | Freitag 
  | Samstag
  | Sonntag
  deriving (Show, Ord, Eq, Enum, Bounded)

newtype Tag = 
  Tag Int
  deriving (Show, Ord, Eq)

data Monat
  = Januar
  | Februar
  | März
  | April
  | Mai
  | Juni
  | Juli
  | August
  | September
  | Oktober
  | November
  | Dezember
  deriving (Show, Ord, Eq, Enum, Bounded)

newtype Jahr = 
  Jahr Int
  deriving (Show, Ord, Eq)

data Datum = 
  Datum Wochentag Tag Monat Jahr | KeinDatum
  deriving (Show, Ord, Eq)

-- | Testet, ob das übergebene Datum gültig ist.
-- Hierzu werden Tage und Monate auf Zugehörigkeit zu einem Bereich
-- zwischen 1 und 31 bzw 1 und 12 geprüft. Die restlichen
-- Eigenschaften sind statisch per Typ sichergestellt.
instance Testable Datum where
  teste (Datum _ (Tag tag) _ _) = tag > 0 && tag <= 31
  teste (KeinDatum) = False

-- | Löscht ein Datum
instance Deletable Datum where
  lösche _ = KeinDatum

newtype Stunde = 
  Stunde Int
  deriving (Show, Ord, Eq)

newtype Minute = 
  Minute Int
  deriving (Show, Ord, Eq)

data Uhrzeit = 
  Uhrzeit Stunde Minute | KeineUhrzeit
  deriving (Show, Ord, Eq)

-- | Testet eine übergebene Uhrzeit auf gültigkeit.
-- Minuten und Stunden werden auf die Zugehörigkeit zu einem Intervall
-- zwischen 0 und 59 bzw. zwischen 0 und 23 geprüft.
instance Testable Uhrzeit where
  teste (Uhrzeit (Stunde stunde) (Minute minute)) = 
    (stunde >= 0 && stunde < 24) && (minute >= 0 && minute < 60)
  teste (KeineUhrzeit) = False

-- | Löscht eine Uhrzeit
instance Deletable Uhrzeit where
  lösche _ = KeineUhrzeit

data Dauer = 
  Dauer Int | KeineDauer
  deriving (Show, Ord, Eq)

-- | Testet eine Dauer darauf, ob sie positiv ist.
instance Testable Dauer where
  teste (Dauer d) = d > 0
  teste (KeineDauer) = False

-- | Löscht eine Dauer
instance Deletable Dauer where
  lösche _ = KeineDauer

data Beschreibung = 
  Beschreibung String | KeineBeschreibung
  deriving (Show, Ord, Eq)

-- | Testet, dass eine Beschreibung nicht leer ist
instance Testable Beschreibung where
  teste (Beschreibung xs) = not $ null xs
  teste (KeineBeschreibung) = False

-- | Löscht eine Beschreibung
instance Deletable Beschreibung where
  lösche _ = KeineBeschreibung

data Termin = 
  Termin Datum Uhrzeit Dauer Beschreibung | KeinTermin
  deriving (Show, Ord, Eq)

-- | Testet einen Termin durch Testen seiner Bestandteile
instance Testable Termin where
  teste (Termin datum uhrzeit dauer beschreibung) =
    teste datum && 
    teste uhrzeit && 
    teste dauer && 
    teste beschreibung
  teste (KeinTermin) = False

-- | Löscht einen Termin
instance Deletable Termin where
  lösche _ = KeinTermin

data TerminArt = 
  Freizeittermin | Arbeitstermin
  deriving (Show, Ord, Eq)

data MonatsArt =
  Kurz | Mittel | Lang
  deriving (Show, Ord, Eq)

data KlassifizierterTermin = 
  KlassifizierterTermin TerminArt MonatsArt Termin | KeinKlassifizierterTermin
  deriving (Show, Ord, Eq)

-- | Ordnet den Tag einer TerminArt zu 
terminArt :: Wochentag -> TerminArt
terminArt t
  | t `elem` [Montag .. Freitag] = Arbeitstermin
  | otherwise = Freizeittermin

-- | Ordnet den Monat einer Monatsart zu
monatsArt :: Monat -> MonatsArt
monatsArt m
  | m `elem` [Januar, März, Mai, Juli, August, Oktober, Dezember] = Lang
  | m `elem` [April, Juni, September, November] = Mittel
  | otherwise = Kurz

-- | Klassifiziert einen Termin entsprechend des Monats und Wochentages
klassifiziereTermin :: Termin -> KlassifizierterTermin
klassifiziereTermin t@(Termin (Datum w _ m _) _ _ _) =
  KlassifizierterTermin (terminArt w) (monatsArt m) t
klassifiziereTermin _ = KeinKlassifizierterTermin

instance Testable KlassifizierterTermin where
  teste (KlassifizierterTermin tA mA t@(Termin (Datum w _ m _) _ _ _)) =
    (terminArt w == tA) && (monatsArt m == mA) && teste t
  teste _ = False

instance Deletable KlassifizierterTermin where
  lösche _ = KeinKlassifizierterTermin
                        
               
