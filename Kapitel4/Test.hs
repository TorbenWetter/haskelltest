module Main where

import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe2 as K4A2
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe3 as K4A3
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe4 as K4A4
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe5 as K4A5
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe6 as K4A6
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe7 as K4A7
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe8 as K4A8
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe9 as K4A9
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe10 as K4A10
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe12 as K4A12
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe13 as K4A13
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe14 as K4A14
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe15 as K4A15
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe16 as K4A16
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe18 as K4A18
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe19 as K4A19
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe20 as K4A20
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe21 as K4A21

main = do
  K4A2.runTests
  K4A3.runTests
  K4A4.runTests
  K4A5.runTests
  K4A6.runTests
  K4A7.runTests
  K4A8.runTests
  K4A9.runTests
  K4A10.runTests
  K4A12.runTests
  K4A13.runTests
  K4A14.runTests
  K4A15.runTests
  K4A16.runTests
  K4A18.runTests
  K4A19.runTests
  K4A20.runTests
  K4A21.runTests
