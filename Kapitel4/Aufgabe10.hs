module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe10 ( vater, pfade ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5

-- | Berechnet den Vater eines Knotens, falls dieser Existiert
vater :: (Eq a) => a -> Baum a -> Maybe a
vater x (Leer) = Nothing
vater x (Knoten v Leer Leer) = Nothing
vater x (Knoten v l@(Knoten y _ _) Leer)
  | x == y = Just v
  | otherwise = vater x l
vater x (Knoten v Leer r@(Knoten y _ _))
  | x == y = Just v
  | otherwise = vater x r
vater x (Knoten v l@(Knoten y _ _) r@(Knoten z _ _))
  | x == y || x == z = Just v
  | otherwise = 
    case vater x l of
      Just v' -> Just v'
      Nothing -> vater x r

-- | Berechnet die Pfade für jedes Blatt bis zur Wurzel
pfade :: Baum a -> [[a]]
pfade baum = pfade' [[]] baum
  where
    pfade' _  (Leer) = []
    pfade' (as:ass) (Knoten x l@(Knoten _ _ _) r@(Knoten _ _ _)) = pfade' ((x:as):ass) l ++ pfade' ((x:as):ass) r
    pfade' (as:ass) (Knoten x l@(Knoten _ _ _) Leer) = pfade' ((x:as):ass) l
    pfade' (as:ass) (Knoten x Leer r@(Knoten _ _ _)) = pfade' ((x:as):ass) r
    pfade' (as:ass) (Knoten x Leer Leer) = (x:as):ass