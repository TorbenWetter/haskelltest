module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe18 ( mconcatMap ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe17

mconcatMap :: (Eq b) => (a -> MeineMenge b) -> MeineMenge a -> MeineMenge b
mconcatMap f m = bigUnion (fmap f m)
