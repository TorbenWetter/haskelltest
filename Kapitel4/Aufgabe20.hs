module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe20
  ( Verkehrsmittel (..)
  , HBahn (..)
  , Bus (..)
  , SBahn (..)
  , linie440
  , linieS1
  , südNord
  ) where

type Liniennummer = Int
type Fahrer = String
type Haltestelle = String
data Verkehrsmittel = 
  Verkehrsmittel Liniennummer Fahrer Haltestelle Haltestelle

type Sitzplätze = Int
data Bus = Bus Verkehrsmittel Sitzplätze

type Wagenanzahl = Int
data SBahn = SBahn Verkehrsmittel Sitzplätze Wagenanzahl

data HBahn = HBahn Haltestelle Haltestelle

instance Show Verkehrsmittel where
  show (Verkehrsmittel nr f start stop) =
    "Liniennummer: " ++ (show nr) ++ " Fahrer: " ++ f ++
    " Von: " ++ start ++ " Nach: " ++ stop

instance Show Bus where
  show (Bus v sitze) = 
    (show v) ++ " Sitzplätze: " ++ (show sitze)

instance Show SBahn where
  show (SBahn v sitze wagen) = 
    (show v) ++ " Sitzplätze pro Wagon: " ++ (show sitze) ++ 
    " Wagons: " ++ (show wagen)

instance Show HBahn where
  show (HBahn start stop) =
    "HBahn von " ++ start ++ " nach " ++ stop

linie440 = 
  Bus (Verkehrsmittel 440 "August Agilis" "Dortmund Oespel" "Dortmund Flughafen") 60

linieS1 =
  SBahn (Verkehrsmittel 1 "Tadeus Tardus" "Dortmund Hauptbahnhof" "Solingen Hauptbahnhof") 4 50

südNord =
  HBahn "Campus Nord" "Campus Süd"

