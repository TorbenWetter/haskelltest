{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe2 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import Data.List (sort)

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe2 as A2

-- | Wandelt eine MeineListe in eine Haskell-Liste
toList :: A2.MeineListe a -> [a]
toList A2.Null = []
toList (x A2.:+: xs) = x:(toList xs)

-- | Wandelt eine Haskell-Liste in eine MeineListe
fromList :: [a] -> A2.MeineListe a
fromList = foldr (A2.:+:) A2.Null 

-- | Testet insertionSort gegen die Haskell-Referenzimplementierung
prop_insertionSort :: (Arbitrary a, Ord a) => [a] -> Bool
prop_insertionSort xs = (sort xs) == (toList $ A2.insertionSort (fromList xs))

-- | Testet quickSort gegen insertionSort
prop_quickSort :: (Arbitrary a, Ord a) => [a] -> Bool
prop_quickSort xs = (A2.quickSort (fromList xs)) == (A2.insertionSort (fromList xs))

runTests = $quickCheckAll