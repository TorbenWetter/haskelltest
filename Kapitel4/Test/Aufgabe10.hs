{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe10 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import qualified Data.Foldable as F

import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe6 as A6
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5 as A5
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe7 as A7
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe10 as A10

-- | Testet, ob zwei Knoten in Vater-Sohn beziehung stehen
istVater :: (Eq a) => a -> a -> A5.Baum a -> Bool
istVater vater sohn (A5.Knoten v l@(A5.Knoten s _ _) r@(A5.Knoten s' _ _)) =
  (v == vater && (s == sohn || s' == sohn)) || istVater vater sohn l || istVater vater sohn r
istVater vater sohn (A5.Knoten v l@(A5.Knoten s _ _) A5.Leer) = 
  (v == vater && s == sohn) || istVater vater sohn l
istVater vater sohn (A5.Knoten v A5.Leer r@(A5.Knoten s _ _)) = 
  (v == vater && s == sohn) || istVater vater sohn r
istVater _ _ _ = False

-- | Findet alle Blätter in einem Baum
blätter :: A5.Baum a -> [a]
blätter (A5.Leer) = []
blätter (A5.Knoten x A5.Leer A5.Leer) = [x]
blätter (A5.Knoten x l r) = blätter l ++ blätter r

-- | Prüft, ob ein Knoten in einem Baum existiert
existiert :: (Eq a) => a -> A5.Baum a -> Bool
existiert x baum = F.foldl (\ ex y -> ex || x == y) False baum

-- | Prüft, ob die Vater-Sohn-Beziehung für die Rückgabe der Funktion vater erfüllt ist
prop_vaterSohn :: (Arbitrary a, Eq a) => A5.Baum a -> a -> Bool
prop_vaterSohn baum@(A5.Leer) sohn = A10.vater sohn baum == Nothing
prop_vaterSohn baum@(A5.Knoten x _ _) sohn = 
  case A10.vater sohn baum of
    Just vater -> istVater vater sohn baum
    Nothing -> x == sohn || not (existiert sohn baum)

-- | Prüft, ob alle Wurzelpfade konsistente Vater-Sohn-Beziehungen darstellen.
prop_pfadKonsistenz :: (Arbitrary a, Eq a) => A5.Baum a -> Bool
prop_pfadKonsistenz baum = 
  all (konsistent baum) $ A10.pfade baum
  where
    konsistent baum (s:v:xs) = istVater v s baum && konsistent baum (v : xs)
    konsistent baum _ = True

-- | Prüft, ob Pfadanfänge den Blättern des Baumes entsprechen
prop_alleBlätter :: (Arbitrary a, Eq a) => A5.Baum a -> Bool
prop_alleBlätter baum =
  let
    pfadEnden = map head $ A10.pfade baum
    bs = blätter baum
  in
    all (`elem` bs) pfadEnden && all (`elem` pfadEnden) bs

-- | Prüft, ob alle Pfade bei der Wurzel Enden.
prop_alleWurzeln :: (Arbitrary a, Eq a) => A5.Baum a -> Bool
prop_alleWurzeln baum@(A5.Leer) = A10.pfade baum == []
prop_alleWurzeln baum@(A5.Knoten x l r) = all (== x) . map last $ A10.pfade baum

runTests = $quickCheckAll