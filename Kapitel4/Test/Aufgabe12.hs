{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe12 
  ( runTests
  , Graph (..)
  , toGGraph
  ) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import qualified Data.Map as M
import qualified Data.Set as S

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11 as A11
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe12 as A12

-- | Eine effiziente Graphdarstellung mit Haskell-Containern
data Graph a = Graph (M.Map a (S.Set a)) deriving (Eq)

-- | Fügt eine Kante zu einem Graphen hinzu
addEdge :: (Ord a) => Graph a -> a -> a -> Graph a
addEdge (Graph m) from to = 
  Graph . M.insertWith (S.union) from (S.singleton to) $ M.insertWith (S.union) to S.empty m

-- | Fügt eine Menge von Kanten zu einem Graphen hinzu
addEdges :: (Ord a) => Graph a -> [(a, a)] -> Graph a
addEdges g edges = 
  foldl (\ g (from, to) -> addEdge g from to) g edges

-- | Liefert Menge adjazenter Knoten
getAdjacent :: (Ord a) => Graph a -> a -> S.Set a
getAdjacent (Graph m) node = 
  M.findWithDefault S.empty node m

-- | Baut einen leeren Graph
newGraph :: Graph a
newGraph = Graph (M.empty)

-- | Zufällige Graphen
instance (Ord a, Arbitrary a) => Arbitrary (Graph a) where
  arbitrary = do
    n <- (`mod` 5) <$> arbitrary
    from <- vectorOf n arbitrary
    to <- vectorOf n arbitrary
    return $ addEdges newGraph (zip from to)

-- | Einen Graph anzeigen
instance (Show a) => Show (Graph a) where
  show (Graph m) = show m

-- | Verwandelt einen Graphen in die Darstellung aus Aufgabe 11
toGGraph :: (Eq a) => Graph a -> A11.GGraph a
toGGraph (Graph m) = 
  M.foldlWithKey 
     (\ g from adjacent -> S.foldr (\ to g -> A11.neueKante g from to) g adjacent) 
     (A11.GGraph A11.Leer A11.Leer) m

-- | Verwandelt in die effiziente Darstellung
fromGGraph :: (Ord a) => A11.GGraph a -> Graph a
fromGGraph (A11.GGraph nodes edges) = 
  addEdges newGraph $ map (\ (s A11.:->: t) -> (s, t)) (A11.toListe edges)

-- | Tiefensuche auf einem Graph
dfs :: (Ord a) => Graph a -> a -> [a]
dfs g@(Graph m) start
  | start `M.member` m = dfs' g [] start
  | otherwise = []
  where
    dfs' g@(Graph m) visited start
      | start `elem` visited = visited
      | otherwise = S.foldl (dfs' g) (visited ++ [start]) (m M.! start)

-- | Testet die Tiefensuche aus Aufgabe 12
-- gegen die Referenzimplementierung
prop_dfs :: (Ord a, Arbitrary a) => Graph a -> a -> Bool
prop_dfs graph start = 
  dfs graph start == A12.dfs (toGGraph graph) start

runTests = $quickCheckAll