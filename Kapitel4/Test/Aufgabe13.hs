{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe13 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative
import Data.Ix
import Data.Array.IArray


import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11 as A11
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe13 as A13

-- | Graph as adjacency matrix
newtype Graph a c = Graph (Array a (Array a c)) deriving (Show)

-- | Funktion auf einen Graphen anwenden
gmap :: (Ix a) => (a -> a -> c -> d) -> (Graph a c) -> Graph a d
gmap f (Graph g) =
  Graph . array (bounds g) . map (mapCol) $ assocs g
  where
    mapCol (col, rows) = 
      (col, array (bounds rows) . map (updateRow col) $ assocs rows)
    updateRow col (row, c) = (row, f col row c)

-- | Graph zusammenfalten
gfoldl :: (Ix b) => (a -> b -> b -> c -> a) -> a -> Graph b c -> a
gfoldl f s (Graph g) =
  foldl (foldCol f) s $ assocs g
  where
    foldCol f s (col, rows) = foldl (step f col) s $ assocs rows
    step f col s (row, c) = f s col row c

-- | Minimum des direkten wegs und des Umwegs via k
floydDist :: (Ix a, Ord c, Num c, Bounded c) => Graph a (c, [a]) -> a -> a -> a -> (c, [a])
floydDist (Graph g) k i j = 
  let
    cij = fst $ (g ! i) ! j
    cik = fst $ (g ! i) ! k
    ckj = fst $ (g ! k) ! j
  in
    if cik < cij - ckj
    then (cik + ckj, k:(snd $ (g ! i) ! j)) else (g ! i) ! j

-- | Floyd-Schritt via k
floydStep :: (Ix a, Ord c, Num c, Bounded c) => Graph a (c, [a]) -> a -> Graph a (c, [a])
floydStep g k = gmap (\ i j _ -> floydDist g k i j) g

-- | Floyd Algorithmus
floyd :: (Ix a, Ord c, Num c, Bounded c) => Graph a c -> Graph a (c, [a])
floyd g@(Graph rs) = foldl floydStep (attachEmptyPaths g) $ indices rs
  where
    attachEmptyPaths = gmap (\ _ _ c -> (c, []))

-- | Zufällige Graphen bis 5x5
instance (Arbitrary a, Ix a, Integral a, Arbitrary c, Num c, Bounded c) => Arbitrary (Graph a c) where
  arbitrary = 
    do
      n <- (`mod` 5) <$> arbitrary
      rows <- vectorOf (toInt n) (row n)
      return . zeroDiag . Graph . array (0, n-1) $ zip [0..n-1] rows
    where
      cost = oneof [abs <$> arbitrary, return maxBound]
      row n = array (0, n-1) . zip [0..n-1] <$> vectorOf (toInt n) cost
      toInt = fromInteger . toInteger
      zeroDiag g = gmap (\ i j c -> if i == j then 0 else c) g

toGGraphAndCosts :: (Ix a, Num c, Bounded c, Ord c) => Graph a c -> (A11.GGraph a, (A11.GKante a -> c))
toGGraphAndCosts g@(Graph rs)= 
  gfoldl build (A11.GGraph (A11.toMenge $ indices rs) A11.Leer, const maxBound) g
  where
    build (g, cf) from to c
      | c < maxBound = 
        (A11.neueKante g from to, adjustCosts cf (from A11.:->: to) c)
      | otherwise = (g, cf)
    adjustCosts cf e c e'
      | e == e' = c
      | otherwise = cf e'

-- | Testet dijkstra gegen floyd
prop_allPairs :: (Ix a) => Graph a Int -> Bool
prop_allPairs g = 
  gfoldl (\ res i j (c, _) -> res && (Just c == dijkstra g i j)) True $ floyd g
  where
    dijkstra g i j = 
      let (gg, cf) = toGGraphAndCosts g in ($ j) <$> A13.dijkstra gg cf i

runTests = $quickCheckAll