{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe5 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import Data.Char

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5 as A5
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe7 as A7

-- | Erzeugen zufälliger Bäume
instance Arbitrary a => Arbitrary (A5.Baum a) where
  arbitrary = oneof [ return A5.Leer
                    , A5.Knoten <$> arbitrary <*> (return A5.Leer) <*> (return A5.Leer)
                    , A5.Knoten <$> arbitrary <*> arbitrary <*> (return A5.Leer)
                    , A5.Knoten <$> arbitrary <*> (return A5.Leer) <*> arbitrary
                    , A5.Knoten <$> arbitrary <*> arbitrary <*> arbitrary
                    ]

-- | Testet die Methode zuGroßbaum gegen eine manuelle Traversierung.
-- Verwendet die Methode 'toUpper' aus Haskell und schränkt sich dabei auf 
-- Buchstaben von 'a' - 'z' ein.
prop_groß :: A5.Baum String -> Bool
prop_groß baum = prop_groß' (A5.zuGroßbaum baum) baum
  where
    großBuchstabe x
      | x `elem` ['a' .. 'z'] = toUpper x
      | otherwise = x
    prop_groß' (A5.Knoten x l r) (A5.Knoten x' l' r') = 
      x == (map großBuchstabe x') && prop_groß' l l' && prop_groß' r r'
    prop_groß' (A5.Leer) (A5.Leer) = True
    prop_groß' _ _ = False

runTests = $quickCheckAll