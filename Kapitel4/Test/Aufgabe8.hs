{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe8 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe8 as A8
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe7 as A7
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5 as A5
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe5

-- | Eine Kopie des Datentyps mit abgeleitetem Eq
data Baum a = Leer | Knoten a (Baum a) (Baum a) deriving (Eq)

toBaum :: A5.Baum a -> Baum a
toBaum A5.Leer = Leer
toBaum (A5.Knoten x l r) = Knoten x (toBaum l) (toBaum r)

-- | Testet die geschriebene Eq instanz gegen die von Haskell abgeleitete.
prop_eq :: (Arbitrary a, Eq a) => A5.Baum a -> A5.Baum a -> Bool
prop_eq b1 b2 = (b1 == b2) == ((toBaum b1) == (toBaum b2))

runTests = $quickCheckAll