{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe20 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe20 as A20

-- | Vergleicht den String der Linie 440
prop_bus :: Bool
prop_bus = 
  show A20.linie440 == "Liniennummer: 440 Fahrer: August Agilis Von: Dortmund Oespel Nach: Dortmund Flughafen Sitzplätze: 60"

-- | Vergleicht den String der S1
prop_sbahn :: Bool
prop_sbahn = 
  show A20.linieS1 == "Liniennummer: 1 Fahrer: Tadeus Tardus Von: Dortmund Hauptbahnhof Nach: Solingen Hauptbahnhof Sitzplätze pro Wagon: 4 Wagons: 50"

-- | Vergleicht den String der Süd-Nord-HBahn
prop_hbahn :: Bool
prop_hbahn = show A20.südNord == "HBahn von Campus Nord nach Campus Süd"

runTests = $quickCheckAll