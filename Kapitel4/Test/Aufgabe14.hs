{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe14 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe12 (Graph (..), toGGraph)

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11 as A11
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe12 as A12
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe14 as A14

-- | Testet die über dijkstra ermittelte Erreichbarkeit gegen
-- die zuvor implementierte Tiefensuche.
prop_erreichbarInDFS :: (Eq a) => Graph a -> Bool
prop_erreichbarInDFS g =
  let
    g'@(A11.GGraph ns es) = toGGraph g
  in
    and $ (\ s t -> (A14.erreichbar g' s t) == (t `elem` A12.dfs g' s)) <$> (A11.toListe ns) <*> (A11.toListe ns)

runTests = $quickCheckAll