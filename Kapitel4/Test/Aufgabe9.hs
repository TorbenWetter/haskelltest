{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe9 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import Data.List (delete)
import qualified Data.Foldable as F

import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe6 as A6
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5 as A5
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe7 as A7
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe8 as A8
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe9 as A9

-- | Fügt in einen binären Suchbaum ein.
einfügen :: (Ord a) => a -> A5.Baum a -> A5.Baum a
einfügen x A5.Leer = A5.Knoten x A5.Leer A5.Leer
einfügen x (A5.Knoten y l r)
  | x > y = A5.Knoten y l (einfügen x r)
  | x < y = A5.Knoten y (einfügen x l) r
  | otherwise = A5.Knoten x l r

newtype BinBaum a = BinBaum (A5.Baum a) deriving (Show)

-- | Erzeugen zufälliger binärer Suchbäume
instance (Ord a, Arbitrary a) => Arbitrary (BinBaum a) where
  arbitrary = BinBaum . foldr einfügen A5.Leer <$> listOf arbitrary

-- | Liste mit preorder Durchlauf des Baums
toList :: A5.Baum a -> [a]
toList baum = F.foldr (:) [] baum

-- | Prüft, ob der gegebene Baum ein Suchbaum ist
isBinTree :: (Ord a) => A5.Baum a -> Bool
isBinTree (A5.Leer) = True
isBinTree (A5.Knoten x l r) =
  case (l, r) of
    (A5.Leer, A5.Leer) -> True 
    (A5.Knoten y _ _, A5.Knoten z _ _) -> x > y && x < z && isBinTree l && isBinTree r
    (A5.Leer, A5.Knoten z _ _) -> x < z && isBinTree r
    (A5.Knoten y _ _, A5.Leer) -> x > y && isBinTree l

-- | Prüft, ob der Baum nach dem Löschen noch ein Suchbaum ist
prop_binTree :: (Arbitrary a, Ord a) => BinBaum a -> a -> Bool
prop_binTree (BinBaum baum) x = isBinTree (A9.entferne x baum)

-- | Prüft, ob alle Elemente bis auf das zu löschende nach dem Löschen vorhanden sind
prop_deleted :: (Arbitrary a, Ord a) => BinBaum a -> a -> Bool
prop_deleted (BinBaum baum) x = 
  let
    altOhneX = delete x $ toList baum
    neuOhneX = toList $ A9.entferne x baum
  in
    all (`elem` altOhneX) neuOhneX && all (`elem` neuOhneX) altOhneX

runTests = $quickCheckAll