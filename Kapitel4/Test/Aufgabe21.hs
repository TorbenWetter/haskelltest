{-# LANGUAGE TemplateHaskell #-}
module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe21 ( runTests ) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe21 as A21

-- | Zufallsinstanzen für die deklarierten Typen

instance Arbitrary A21.Wochentag where
  arbitrary = elements [ A21.Montag .. A21.Sonntag ]

instance Arbitrary A21.Tag where
  arbitrary = A21.Tag <$> arbitrary

instance Arbitrary A21.Monat where
  arbitrary = elements [ A21.Januar .. A21.Dezember ]

instance Arbitrary A21.Jahr where
  arbitrary = A21.Jahr <$> arbitrary

instance Arbitrary A21.Datum where
  arbitrary = 
    oneof [ A21.Datum <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
          , return A21.KeinDatum
          ]
instance Arbitrary A21.Stunde where
  arbitrary = A21.Stunde <$> arbitrary

instance Arbitrary A21.Minute where
  arbitrary = A21.Minute <$> arbitrary

instance Arbitrary A21.Uhrzeit where
  arbitrary = 
    oneof [ A21.Uhrzeit <$> arbitrary <*> arbitrary
          , return A21.KeineUhrzeit
          ]

instance Arbitrary A21.Dauer where
  arbitrary =
    oneof [ A21.Dauer <$> arbitrary
          , return A21.KeineDauer
          ]

instance Arbitrary A21.Beschreibung where
  arbitrary =
    oneof [ A21.Beschreibung <$> arbitrary
          , return A21.KeineBeschreibung
          ]

instance Arbitrary A21.Termin where
  arbitrary =
    oneof [ A21.Termin <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
          , return A21.KeinTermin
          ]

instance Arbitrary A21.TerminArt where
  arbitrary = elements [A21.Freizeittermin, A21.Arbeitstermin]

instance Arbitrary A21.MonatsArt where
  arbitrary = elements [A21.Kurz, A21.Mittel, A21.Lang]

instance Arbitrary A21.KlassifizierterTermin where
  arbitrary =
    oneof [ A21.KlassifizierterTermin <$> arbitrary <*> arbitrary <*> arbitrary
          , return A21.KeinKlassifizierterTermin
          ]

-- | Prüft, ob ein Objekt nach dem Löschen ungültig ist
ungültigNachLöschen :: (A21.Deletable a, A21.Testable a) => a -> Bool
ungültigNachLöschen o = not $ A21.teste (A21.lösche o) 

-- | Prüft den Gültigkeitstest für Datumsangaben
prop_testeDatum :: A21.Datum -> Bool
prop_testeDatum d@(A21.Datum _ (A21.Tag t) _ _) = 
  ((t > 0 && t <= 31) == A21.teste d)
  && ungültigNachLöschen d
prop_testeDatum d = not $ A21.teste d

-- | Prüft den Gültigkeitstest für Uhrzeiten
prop_testeUhrzeit :: A21.Uhrzeit -> Bool
prop_testeUhrzeit u@(A21.Uhrzeit (A21.Stunde s) (A21.Minute m)) =
  ((s >= 0 && s < 24 && m >= 0 && m < 60) == A21.teste u) &&
  ungültigNachLöschen u
prop_testeUhrzeit u = not $ A21.teste u

-- | Prüft den Gültigkeitstest für Dauern
prop_testeDauer :: A21.Dauer -> Bool
prop_testeDauer dauer@(A21.Dauer d) =
  ((d > 0) == A21.teste dauer) &&
  ungültigNachLöschen dauer
prop_testeDauer dauer = not $ A21.teste dauer

-- | Prüft den Gültigkeitstest für Beschreibungen
prop_testeBeschreibung :: A21.Beschreibung -> Bool
prop_testeBeschreibung beschreibung@(A21.Beschreibung b) =
  ((not $ null b) == A21.teste beschreibung) &&
  ungültigNachLöschen beschreibung
prop_testeBeschreibung beschreibung = not $ A21.teste beschreibung

-- | Prüft den Gültigkeitstest für Termine
prop_testeTermin :: A21.Termin -> Bool
prop_testeTermin termin@(A21.Termin datum dauer uhrzeit beschreibung) =
  ((A21.teste datum && 
    A21.teste dauer && 
    A21.teste uhrzeit && 
    A21.teste beschreibung) == A21.teste termin) &&
  ungültigNachLöschen termin
prop_testeTermin termin = not $ A21.teste termin

-- | Testet, ob termine richtig klassifiziert werden
prop_klassifiziere :: A21.Termin -> Bool
prop_klassifiziere t =
  A21.teste t == A21.teste (A21.klassifiziereTermin t)

-- | Prüft den Gültigkeitstest für klassifizierte Termine
prop_testeKlassifiziertenTermin :: A21.KlassifizierterTermin -> Bool
prop_testeKlassifiziertenTermin kt@(A21.KlassifizierterTermin tA mA t@(A21.Termin (A21.Datum w _ m _) _ _ _)) =
    ((A21.terminArt w == tA) && 
     (A21.monatsArt m == mA) && 
     (A21.teste t)) ==
    A21.teste kt
prop_testeKlassifiziertenTermin kt = not $ A21.teste kt

runTests = $quickCheckAll



