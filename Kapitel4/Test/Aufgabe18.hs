{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe18 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative
import Data.List (nub)

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe17 as A17
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe18 as A18

-- | Testet mconcatMap gegen concatMap + nub
prop_concat :: (Eq b, Eq a) => Fun a [b] -> [a] -> Bool
prop_concat (Fun _ f) xs = 
  A17.toMenge (concatMap f (nub xs)) == A18.mconcatMap (A17.toMenge . f) (A17.toMenge xs)

runTests = $quickCheckAll