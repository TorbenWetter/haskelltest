{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe4 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import Data.List (sort)

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe4 as A4

-- | Wandelt eine Warteschlange in eine Haskell-Liste
toList :: A4.Warteschlange a -> [a]
toList A4.Leer = []
toList (x A4.:>: xs) = x:(toList xs)

-- | Wandelt eine Haskell-Liste in eine Warteschlange
fromList :: (Ord a) => [a] -> A4.Warteschlange a
fromList = foldr (A4.:>:) A4.Leer . sort

-- | FIFO Instanz für eine Haskell-Liste
instance A4.PRIO [] where
  erzeugen = []
  einfügen x = sort . (x:)
  entfernen [] = Nothing
  entfernen xs = Just (head $ sort xs, tail $ sort xs)
  istLeer = null

-- | Testet erzeugen gegen die Haskell-Listen Instanz
prop_erzeugen :: Bool
prop_erzeugen = (A4.erzeugen :: [Int]) == (toList A4.erzeugen)

-- | Testet einfügen gegen die Haskell-Listen Instanz
prop_einfügen :: (Ord a, Arbitrary a) => a -> [a] -> Bool
prop_einfügen x xs = A4.einfügen x xs == toList (A4.einfügen x (fromList xs))

-- | Testet entfernen gegen die Haskell-Listen Instanz
prop_entfernen :: (Ord a, Arbitrary a) => [a] -> Bool
prop_entfernen xs = A4.entfernen xs == ((toList <$>) <$> A4.entfernen (fromList xs))

-- | Testet istLeer gegen die Haskell-Listen Instanz
prop_istLeer :: (Arbitrary a, Ord a) => [a] -> Bool
prop_istLeer xs = A4.istLeer xs == A4.istLeer (fromList xs)

runTests = $quickCheckAll