{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe7 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe7 as A7
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5 as A5
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe5

-- | Eine Liste der jeweilgen Knotenhöhen in preorder, wobei leere Knoten mit berücksichtigt werden
höhen :: A5.Baum a -> [Int]
höhen baum = höhen' baum 0
  where
    höhen' (A5.Leer) h = [h]
    höhen' (A5.Knoten x l r) h = h :((höhen' l (h+1)) ++ (höhen' r (h+1)))

-- | Zerlegt den show-String eines Baums anhand seiner Zeilen 
-- und vergleich die führenden Leerzeichen mit den Baumhöhen
prop_höhen :: (Show a, Arbitrary a) => A5.Baum a -> Bool
prop_höhen baum = 
  and . zipWith (==) (höhen baum) . map ((`div` 2) . length . takeWhile (== ' '))  . lines . tail $ show baum 

runTests = $quickCheckAll