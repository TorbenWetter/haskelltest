{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe6 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe6 as A6
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe5 as A5
import qualified HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe5

import qualified Data.Foldable as F

-- | Ableiten einer fold instanz für Bäume
instance F.Foldable A5.Baum where
  foldr _ z (A5.Leer) = z
  foldr f z (A5.Knoten x l r) = f x (F.foldr f (F.foldr f z r) l)

-- | Testen der Baumsumme gegen foldl (+) 0
prop_baumSumme :: (Eq a, Num a, Arbitrary a) => A5.Baum a -> Bool
prop_baumSumme baum = A6.baumSumme baum == F.foldl (+) 0 baum

runTests = $quickCheckAll