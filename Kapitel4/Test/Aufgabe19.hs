{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe19 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import Data.List (delete)

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe17 as A17
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe19 as A19

-- | Zufällige Multimengen
instance (Arbitrary a) => Arbitrary (A19.MultiMenge a) where
  arbitrary = A19.fromListe <$> arbitrary

-- | Testet, ob singletons richtig erstellt werden
prop_singleton :: (Eq a) => a -> Bool
prop_singleton x = 
  let
    m = A19.singleton x
  in
  (x `A19.element` m) && (A19.istLeer $ A19.deleteFirst x m)

-- | Testet insert
prop_insert :: (Eq a) => a -> A19.MultiMenge a -> Bool
prop_insert x m =
  let
    m' = A19.insert x m
  in
    x `A19.element` m' && A19.deleteFirst x m' == m

-- | Testet die Funktorinstanz gegen ihre mathematische Definition
prop_functor :: (Eq c) => Fun a b -> Fun b c -> A19.MultiMenge a -> Bool
prop_functor (Fun _ f) (Fun _ g) m =
  fmap (g . f) m == (fmap g . fmap f) m

-- | Testet mfoldr gegen foldr
prop_foldr :: (Eq a) => Fun (b, a) a -> a -> A19.MultiMenge b -> Bool
prop_foldr (Fun _ f) x m =
  A19.mfoldr (curry f) x m == foldr (curry f) x (A19.toListe m)

-- | Testet mfoldr gegen mfoldr1
prop_mfoldr1 :: (Eq a) => Fun (a, a) a -> a -> A19.MultiMenge a -> Bool
prop_mfoldr1 (Fun _ f) x m =
  A19.mfoldr (curry f) x m == 
     A19.mfoldr1 (curry f) (A19.fromListe $ (A19.toListe m) ++ [x])

-- | Testet mfoldl gegen foldl
prop_foldl :: (Eq a) => Fun (a, b) a -> a -> A19.MultiMenge b -> Bool
prop_foldl (Fun _ f) x m =
  A19.mfoldl (curry f) x m == foldl (curry f) x (A19.toListe m)

-- | Testet mfoldl1 gegen mfoldl
prop_mfoldl1 :: (Eq a) => Fun (a, a) a -> a -> A19.MultiMenge a -> Bool
prop_mfoldl1 (Fun _ f) x m =
  A19.mfoldl1 (curry f) (x A19.:<<: m) == A19.mfoldl (curry f) x m

-- | Testet element gegen elem auf Listen
prop_elem :: (Eq a) => a -> A19.MultiMenge a -> Bool
prop_elem x m = 
  (x `A19.element` m) == (x `elem` (A19.toListe m))

-- | Testet mfilter gegen filter auf Listen
prop_filter :: (Eq a) => Fun a Bool -> A19.MultiMenge a -> Bool
prop_filter (Fun _ f) m =
  A19.mfilter f m == A19.fromListe (filter f (A19.toListe m))

-- | Testet, ob elemente wirklich gelöscht werden
prop_delete :: (Eq a) => a -> A19.MultiMenge a -> Bool
prop_delete x m
  | x `A19.element` m = not $ x `A19.element` (A19.delete x m)
  | otherwise = m == (A19.delete x m)

-- | Testet deleteFirst gegen delete auf Listen
prop_deleteFirst :: (Eq a) => a -> A19.MultiMenge a -> Bool
prop_deleteFirst x m = 
   A19.toListe (A19.deleteFirst x m) == delete x (A19.toListe m)

-- | Testet neutrale Mengenoperationen 
prop_neutral :: (Eq a) => A19.MultiMenge a -> Bool
prop_neutral m = 
  (A19.Leer A19.# m == m) &&
  (A19.Leer A19.% m == A19.Leer) &&
  (m A19.% m == m)

-- | Testet Mengenoperationen auf kommutativität
prop_kommutativ :: (Eq a) => A19.MultiMenge a -> A19.MultiMenge a -> Bool
prop_kommutativ n m = 
  (m A19.% n == n A19.% m) && (m A19.# n == n A19.# m)

-- | Testet Mengenoperationen auf distributivität
prop_distributiv :: (Eq a) => A19.MultiMenge a -> A19.MultiMenge a -> A19.MultiMenge a -> Bool
prop_distributiv m n o =
  ((m A19.% n) A19.# o == (m A19.# o) A19.% (n A19.# o))

-- | Testet Mengenoperationen auf Assoziativität
prop_assoziativ :: (Eq a) => A19.MultiMenge a -> A19.MultiMenge a -> A19.MultiMenge a -> Bool
prop_assoziativ m n o =
  ((m A19.# n) A19.# o == m A19.# (n A19.# o)) &&
  ((m A19.% n) A19.% o == m A19.% (n A19.% o))

-- | Testet Mengensubtraktion auf ihre algebraischen Eigenschaften
prop_mminus :: (Eq a) => A19.MultiMenge a -> A19.MultiMenge a -> A19.MultiMenge a -> Bool
prop_mminus m n o =
  m `A19.mminus` m == A19.Leer &&
  m `A19.mminus` A19.Leer == m &&
  A19.Leer `A19.mminus` m == A19.Leer &&
  (m `A19.mminus` n) `A19.mminus` o == m `A19.mminus` (n A19.# o) &&
  (m A19.# n) `A19.mminus` n == m

-- | Testet das verwandeln in Listen und Mengen
prop_cast :: (Eq a) => A19.MultiMenge a -> Bool
prop_cast m =
  A19.fromListe (A19.toListe  m) == m &&
  A19.toMenge m == (A17.toMenge $ A19.toListe m)

runTests = $quickCheckAll