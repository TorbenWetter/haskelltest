{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe16 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative
import qualified Data.Map as M
import qualified Data.Set as S
import Data.List (isInfixOf)

import HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe12 (Graph (..))

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe16 as A16

toRelGraph :: (Ord a) => Graph a -> A16.Graph a
toRelGraph (Graph m) = 
  A16.Graph (M.keys m, (\ k -> S.toList $ M.findWithDefault S.empty k m))

-- | Testet die Eq Implementierung aus Aufgabe 16 gegen die abgeleitete
-- Implementierung aus dem Test von Aufgabe 12.
prop_eq :: (Ord a) => Graph a -> Graph a -> Bool
prop_eq g1 g2 = 
  (g1 == g2) == (toRelGraph g1 == toRelGraph g2)

-- | Testet, ob der Name aller Knoten im von show gelieferten 
-- String vorkommt
prop_allNodes :: (Ord a, Show a) => Graph a -> Bool
prop_allNodes g@(Graph m) =
  all (\ s -> (show s) `isInfixOf` (show $ toRelGraph g)) $ M.keys m

runTests = $quickCheckAll