{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe15 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative
import Data.List (elemIndex)

import HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe12 (Graph (..), toGGraph)

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11 as A11
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe14 as A14
import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe15 as A15


-- | Testet mit 'erreichbar' aus Aufgabe 14, ob die topSort Ausgabe 
-- zykelfrei ist und die Ordnung in der Liste der Erreichbarkeit entspricht.
prop_acyclicOrdered :: (Eq a) => Graph a -> Bool
prop_acyclicOrdered g = 
  let gg@(A11.GGraph ns es) = toGGraph g in
  and $ acyclicOrdered gg <$> (A11.toListe ns) <*> (A11.toListe ns)
  where
    acyclicOrdered g@(A11.GGraph ns es) x y
      | x == y = 
        not $ ((x A11.:->: x) `A11.element` es && x `elem` A15.topSort g)
      | A14.erreichbar g x y && A14.erreichbar g y x = 
        not (x `elem` A15.topSort g || y `elem` A15.topSort g)
      | A14.erreichbar g x y = 
        elemIndex x (A15.topSort g) <= elemIndex y (A15.topSort g)
      | A14.erreichbar g y x = acyclicOrdered g y x
      | otherwise = True

runTests = $quickCheckAll