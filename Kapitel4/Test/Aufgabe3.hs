{-# LANGUAGE TemplateHaskell #-}

module HaskellFuerDenRestVonUns.Kapitel4.Test.Aufgabe3 (runTests) where

import Test.QuickCheck
import Test.QuickCheck.All
import Test.QuickCheck.Function
import Control.Applicative

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe3 as A3

-- | Wandelt eine Warteschlange in eine Haskell-Liste
toList :: A3.Warteschlange a -> [a]
toList A3.Leer = []
toList (x A3.:>: xs) = x:(toList xs)

-- | Wandelt eine Haskell-Liste in eine Warteschlange
fromList :: [a] -> A3.Warteschlange a
fromList = foldr (A3.:>:) A3.Leer

-- | FIFO Instanz für eine Haskell-Liste
instance A3.FIFO [] where
  erzeugen = []
  einfügen = (:)
  entfernen [] = Nothing
  entfernen xs = Just (last xs, init xs)
  istLeer = null

-- | Testet erzeugen gegen die Haskell-Listen Instanz
prop_erzeugen :: Bool
prop_erzeugen = (A3.erzeugen :: [Int]) == (toList A3.erzeugen)

-- | Testet einfügen gegen die Haskell-Listen Instanz
prop_einfügen :: (Eq a, Arbitrary a) => a -> [a] -> Bool
prop_einfügen x xs = A3.einfügen x xs == toList (A3.einfügen x (fromList xs))

-- | Testet entfernen gegen die Haskell-Listen Instanz
prop_entfernen :: (Eq a, Arbitrary a) => [a] -> Bool
prop_entfernen xs = A3.entfernen xs == ((toList <$>) <$> A3.entfernen (fromList xs))

-- | Testet istLeer gegen die Haskell-Listen Instanz
prop_istLeer :: (Arbitrary a) => [a] -> Bool
prop_istLeer xs = A3.istLeer xs == A3.istLeer (fromList xs)

runTests = $quickCheckAll