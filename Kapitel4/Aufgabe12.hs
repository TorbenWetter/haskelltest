module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe12 ( dfs ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11

dfs :: (Eq a) => GGraph a -> a -> [a]
dfs graph@(GGraph ns es) node
  | node `element` ns = dfs' graph node []
  | otherwise = []
  where
    dfs' graph node visited
      | node `elem` visited = visited
      | otherwise = foldl (visit node) (visited ++ [node]) (toListe $ kanten graph)
    visit start visited (n :->: succ)
      | start /= n = visited
      | otherwise = dfs' graph succ visited