module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe13 ( dijkstra ) where

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11

dijkstra :: (Eq a, Num b, Ord b, Bounded b) => GGraph a -> (GKante a -> b) -> a -> Maybe (a -> b)
dijkstra (GGraph ns es) c start
  | start `element` ns = 
    Just . fst $ foldl (choose c) (\ t -> c (start :->: t), ([start], filter (/= start) (toListe ns))) [1 .. length (toListe ns) - 1]
  | otherwise = Nothing 
  where
    choose c (d, (s, ws)) _ = 
      let 
        w = minimumBy d ws
        s' = w:s
        ws' = filter (/= w) ws
      in
        (foldl (adjustD c w) d ws', (s', ws'))
    adjustD c w d v v'
      | v == v' && d w < d v - c (w :->: v) = 
        d w + c (w :->: v)
      | otherwise = d v'
    minimumBy :: (Ord b) => (a -> b) -> [a] -> a
    minimumBy f xs = foldl1 (\ m x -> if (f m) < (f x) then m else x) xs
    