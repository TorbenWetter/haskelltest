module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe19
  ( MultiMenge (..)
  , istLeer
  , singleton
  , insert
  , mfoldr
  , mfoldr1
  , mfoldl
  , mfoldl1
  , element
  , mfilter
  , delete
  , deleteFirst
  , teilMenge
  , (#)
  , (%)
  , mminus
  , toMenge
  , toListe
  , fromListe
  ) where

import qualified HaskellFuerDenRestVonUns.Kapitel4.Aufgabe17 as A17

infixr 5 :<<:
data MultiMenge a = a :<<: MultiMenge a | Leer

istLeer :: MultiMenge a -> Bool
istLeer Leer = True
istLeer _ = False

singleton :: a -> MultiMenge a
singleton x = x :<<: Leer

insert :: a -> MultiMenge a -> MultiMenge a
insert = (:<<:)

instance Functor MultiMenge where
  fmap f (x:<<:xs) = (f x :<<: fmap f xs)
  fmap f Leer = Leer

mfoldr :: (b -> a -> a) -> a -> MultiMenge b -> a
mfoldr f x (y:<<:ys) = f y (mfoldr f x ys)
mfoldr f x (Leer) = x

mfoldr1 :: (a -> a -> a) -> MultiMenge a -> a
mfoldr1 f (x :<<: Leer) = x
mfoldr1 f (x :<<: xs) = f x (mfoldr1 f xs)

mfoldl :: (a -> b -> a) -> a -> MultiMenge b -> a 
mfoldl f x (y :<<: ys) = mfoldl f (f x y) ys
mfoldl _ x Leer = x

mfoldl1 :: (a -> a -> a) -> MultiMenge a -> a
mfoldl1 f (x :<<: xs) = mfoldl f x xs

element :: (Eq a) => a -> MultiMenge a -> Bool
element x xs = mfoldl (\ res y -> res || y == x) False xs

mfilter :: (a -> Bool) -> MultiMenge a -> MultiMenge a
mfilter f xs = 
  mfoldr (\ x ys -> if f x then x :<<: ys else ys) Leer xs

delete :: (Eq a) => a -> MultiMenge a -> MultiMenge a
delete x xs = mfilter (/= x) xs

deleteFirst :: (Eq a) => a -> MultiMenge a -> MultiMenge a
deleteFirst x (y:<<:ys)
  | x == y = ys
  | otherwise = y :<<: (deleteFirst x ys)
deleteFirst _ Leer = Leer

teilMenge :: (Eq a) => MultiMenge a -> MultiMenge a -> Bool
teilMenge xs ys = 
  fst $ mfoldl (\ (res, ys') x -> (res && (x `element` ys'), deleteFirst x ys')) (True, ys) xs

infixr 5 #
(#) :: (Eq a) => MultiMenge a -> MultiMenge a -> MultiMenge a
xs # ys = mfoldl (flip insert) xs ys

infixr 5 %
(%) :: (Eq a) => MultiMenge a -> MultiMenge a -> MultiMenge a
ms % ns = 
  fst $ mfoldl (\ (res, ns') m -> if m `element` ns' 
                                  then (m :<<: res, deleteFirst m ns') 
                                  else (res, ns')) (Leer, ns) ms

mminus :: (Eq a) => MultiMenge a -> MultiMenge a -> MultiMenge a
ms `mminus` ns = mfoldl (\ ms' n -> deleteFirst n ms') ms ns

instance (Eq a) => Eq (MultiMenge a) where
  ms == ns = teilMenge ms ns && teilMenge ns ms

instance (Show a) => Show (MultiMenge a) where
  show Leer = "{{ }}"
  show (m:<<:ms) = 
    "{{ " ++ mfoldl (\ s x -> s ++ ", " ++ show x) (show m) ms ++ " }}"

toMenge :: (Eq a) => MultiMenge a -> A17.MeineMenge a
toMenge ms = mfoldr (A17.insert) A17.Leer ms

toListe :: MultiMenge a -> [a]
toListe xs = mfoldr (:) [] xs

fromListe :: [a] -> MultiMenge a
fromListe xs = foldr (:<<:) Leer xs
