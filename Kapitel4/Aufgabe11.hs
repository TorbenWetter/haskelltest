module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11 
  ( MeineMenge (..)
  , GKante (..)
  , GGraph (..)
  , istLeer
  , element
  , teilMenge
  , insert
  , singleton
  , toListe
  , toMenge
  , delete
  , (#)
  , bigUnion
  , (%)
  , dieTeilmengen
  , knoten
  , kanten
  , neueKante
  ) where

infixr 5 :<:
data MeineMenge a = Leer | a :<: MeineMenge a

data GKante a = a :->: a deriving (Eq, Show)

data GGraph a = GGraph (MeineMenge a) (MeineMenge (GKante a))

istLeer :: MeineMenge a -> Bool
istleer Leer = True
istLeer _ = False

element :: (Eq a) => a -> MeineMenge a -> Bool
element x Leer = False
element x (y :<: xs) = if (x == y) then True else (element x xs)

teilMenge :: (Eq t) => MeineMenge t -> MeineMenge t -> Bool
teilMenge Leer ms = True
teilMenge (a :<: ns) ms = 
  if (a `element` ms) then teilMenge ns ms
  else False

instance (Eq a) => Eq (MeineMenge a) where
  ns == ms = (teilMenge ns ms) && (teilMenge ms ns)

insert :: (Eq a) => a -> MeineMenge a -> MeineMenge a
insert x xs
  | x `element` xs = xs
  |otherwise = x :<: xs

singleton :: a -> MeineMenge a
singleton x = x :<: Leer

toListe :: MeineMenge a -> [a]
toListe Leer = []
toListe (x :<: ms) = x:(toListe ms)

toMenge :: (Eq a) => [a] -> MeineMenge a
toMenge xs = foldr insert Leer xs

delete :: (Eq a) => a -> MeineMenge a -> MeineMenge a
delete x Leer = Leer
delete x (y :<: xs)
  | x == y = delete x xs
  | otherwise = y :<: (delete x xs)

instance (Show a) => Show (MeineMenge a) where
  show Leer = "{ }"
  show (a :<: ms) = "{" ++ (show a) ++ (concat showRest) ++ "}"
    where
      showRest
        | istLeer ms = [""]
        | otherwise = map (", " ++) (map show (toListe ms))

infixr 5 #
(#) :: (Eq a) => MeineMenge a -> MeineMenge a -> MeineMenge a
ms # ns = foldr insert ms (toListe ns)

bigUnion :: (Eq a) => MeineMenge (MeineMenge a) -> MeineMenge a
bigUnion nss = foldr (#) Leer (toListe nss)

infixr 5 %
(%) :: (Eq a) => MeineMenge a -> MeineMenge a -> MeineMenge a
ms % ns = toMenge (filter (\y -> y `element` ns) (toListe ms))

instance Functor MeineMenge where
  fmap f Leer = Leer
  fmap f (a :<: ms) = (f a) :<: (fmap f ms)

teilM :: (Eq a) => MeineMenge a -> [a] -> MeineMenge (MeineMenge a) -> MeineMenge (MeineMenge a)
teilM ms b alle
  | b == [] = insert ms alle
  | otherwise = foldr (#) (insert ms alle) hilf
  where
    t = length b - 1
    alleT = [drop k b | k <- [0 .. t]]
    hilf = [teilM (insert r ms) rs alle | (r:rs) <- alleT]

dieTeilmengen :: (Eq a) => MeineMenge a -> MeineMenge (MeineMenge a)
dieTeilmengen ms = teilM Leer (toListe ms) Leer

knoten :: GGraph a -> MeineMenge a
knoten (GGraph ns _) = ns

kanten :: GGraph a -> MeineMenge (GKante a)
kanten (GGraph _ es) = es

-- | Fügt eine Kante zu einem Graphen hinzu
neueKante :: (Eq a) => GGraph a -> a -> a -> GGraph a
neueKante (GGraph ns es) s t = 
  GGraph (insert s (insert t ns)) (insert (s :->: t) es)

