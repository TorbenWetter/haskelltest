module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe17
  ( MeineMenge (..)
  , istLeer
  , element
  , teilMenge
  , insert
  , singleton
  , toListe
  , toMenge
  , delete
  , (#)
  , bigUnion
  , (%)
  , dieTeilmengen
  ) where

-- | Die Mengenfunktionen wurden bereits vorher für die Graphen
-- verwendet. Die Methoden 'potenzMenge' und 'alleTeilmengen' wurden
-- zu gunsten der bewiesenen und schnelleren Implementierung
-- 'dieTeilmengen' weggelassen.

import HaskellFuerDenRestVonUns.Kapitel4.Aufgabe11

