module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe3 ( Warteschlange (..), FIFO (..) ) where

class FIFO w where
  erzeugen :: w a
  einfügen :: a -> w a -> w a
  entfernen :: w a -> Maybe (a, w a)
  istLeer :: w a -> Bool

infixl 5 :>:
data Warteschlange a = Leer | a :>: Warteschlange a

instance FIFO Warteschlange where
  erzeugen = Leer
  einfügen x w = x :>: w
  entfernen Leer = Nothing
  entfernen (x :>: Leer) = Just (x, Leer)
  entfernen (x :>: w) = 
    let Just (y, w') = entfernen w in Just (y, x :>: w')
  istLeer Leer = True
  istLeer _ = False
