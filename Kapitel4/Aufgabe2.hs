module HaskellFuerDenRestVonUns.Kapitel4.Aufgabe2 ( MeineListe (..), insertionSort, quickSort ) where

infixr 5 :+:
data MeineListe a = Null | a :+: (MeineListe a) deriving (Eq)

infixr 5 #
Null # ys = ys
(x :+: xs) # ys = x :+: (xs # ys)

-- | Fügt sortiert in eine Liste ein
insert :: (Ord a) => a -> MeineListe a -> MeineListe a
insert x Null = x :+: Null
insert x (y :+: zs)
  | x <= y = x :+: (y :+: zs)
  | otherwise = y :+: (insert x zs)

-- | Soriert eine Liste durch Einfügen
insertionSort :: (Ord a) => MeineListe a -> MeineListe a
insertionSort xs = sortIns xs Null
  where
    sortIns Null ys = ys
    sortIns (x:+:xs) ys = sortIns xs (insert x ys)

-- | Filtert eine Liste entsprechend der übergebenen Funktion
filter' :: (a -> Bool) -> MeineListe a -> MeineListe a
filter' f (x:+:xs)
  | f x = x :+: (filter' f xs)
  | otherwise = filter' f xs
filter' _ Null = Null

-- | Sortiert eine Liste mit quicksort
quickSort :: (Ord a) => MeineListe a -> MeineListe a
quickSort Null = Null
quickSort (x:+:xs) = (quickSort $ filter' (<= x) xs) # (x :+: (quickSort $ filter' (> x) xs))

